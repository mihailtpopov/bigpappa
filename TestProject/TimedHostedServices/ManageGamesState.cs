using Consts;
using Data;
using Data.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Services;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TimedHostedServices
{
    public class ManageGamesState : IHostedService, IDisposable
    {
        private int executionCount = 0;
        private readonly ILogger<ManageGamesState> _logger;
        private readonly IServiceProvider serviceProvider;
        private Timer timer;
        private Timer secondTimer;
        private TimeSpan timerDueTime = TimeSpan.Zero;

        public ManageGamesState(ILogger<ManageGamesState> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer = new Timer(DoWork, null, timerDueTime, TimeSpan.FromMinutes(1));
            secondTimer = new Timer(AddOutOfDateMatchesToArchive, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));

            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            var count = Interlocked.Increment(ref executionCount);

            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();

                await this.ManageInPlayMatchesState(unitOfWork);
                await this.ManageScheduledMatchesState(unitOfWork);
            };
        }

        private async void AddOutOfDateMatchesToArchive(object state)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var unitOfWork = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                await unitOfWork.MatchesService.AddOutOfDateMatchesToArchive();
            }
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            // should write in logger when stops "gracefully"
            Console.WriteLine("Timed Hosted Service is stopping.");

            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }

        private async Task ManageInPlayMatchesState(IUnitOfWork unitOfWork)
        {
            var leaguesInPlayMatches = await unitOfWork.MatchesService
                    .GetByMatchStatusId(SportIdsConsts.FOOTBALL_ID, MatchStatusIdsConsts.IN_PLAY, DateTime.Today);

            foreach (var matches in leaguesInPlayMatches.Values)
            {
                foreach (var match in matches)
                {
                    var minutesPlayed = (int)Math.Ceiling(DateTime.Now.Subtract(match.StartingTime).TotalMinutes);

                    if (minutesPlayed <= 45)
                    {
                        match.FirstHalf.CurrMinute = minutesPlayed;
                    }
                    else if (minutesPlayed >= 60)
                    {
                        if (minutesPlayed == 90)
                        {
                            match.MatchStatusId = MatchStatusIdsConsts.FINISHED;
                            match.FirstHalf.CurrMinute = null;
                            match.SecondHalf.CurrMinute = null;
                        }
                        else
                        {
                            match.SecondHalf.CurrMinute = minutesPlayed - 45;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                await unitOfWork.SaveChangesAsync();
            }
        }

        private async Task ManageScheduledMatchesState(IUnitOfWork unitOfWork)
        {
            var leagueScheduledMatches = await unitOfWork.MatchesService
                    .GetByMatchStatusId(SportIdsConsts.FOOTBALL_ID, MatchStatusIdsConsts.SHEDULED, DateTime.Today);

            foreach (var matches in leagueScheduledMatches.Values)
            {
                foreach (var match in matches)
                {
                    var remainingMinutes = Math.Ceiling(DateTime.Now.Subtract(match.StartingTime).TotalMinutes * (-1.0));

                    if (remainingMinutes == 0)
                    {
                        match.MatchStatusId = MatchStatusIdsConsts.IN_PLAY;
                        match.FirstHalf.CurrMinute = 1;
                        await unitOfWork.SaveChangesAsync();
                    }
                }
            }
        }
    }
}
