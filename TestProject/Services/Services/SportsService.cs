﻿using Data;
using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class SportsService
    {
        private readonly FastScoreContext context;

        public SportsService(FastScoreContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<Sport>> GetAll()
        {
            return await this.context.Sports
                .Include(s => s.Countries)
                    .ThenInclude(c => c.Leagues).ToListAsync();
        }
    }
}
