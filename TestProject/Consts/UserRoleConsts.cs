﻿using System;

namespace Consts
{
    public static class UserRoleConsts
    {
        public const string ADMIN = "Admin";
        public const string USER = "User";
    }
}
