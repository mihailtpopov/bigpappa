﻿using Data.Models;
using System;
using TestProject.ViewModels;

namespace Mapping
{
    public class InputMapper
    {
        public User Map(UserRegisterViewModel userRegisterVM)
        {
            return new User()
            {
                UserName = userRegisterVM.UserName
            };
        }
    }
}
