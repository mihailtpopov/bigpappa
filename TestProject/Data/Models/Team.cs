﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models
{
    public class Team
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(30, ErrorMessage = "Team name can not exceed {0} characters")]
        public string Name { get; set; }

        public string FlagPhotoUrl { get; set; }

        [ForeignKey("League")]
        public int LeagueId { get; set; }
        public League League { get; set; }

        public IList<Player> Players { get; set; }

        public IList<Match> HomeMatches { get; set; }

        public IList<Match> AwayMatches { get; set; }

        public IList<MatchArchive> ArchiveHomeMatches { get; set; }

        public IList<MatchArchive> ArchiveAwayMatches { get; set; }

        public IList<Match> Wins { get; set; }

        public IList<Match> Losses { get; set; }

        public IList<MatchArchive> ArchiveWins { get; set; }

        public IList<MatchArchive> ArchiveLosses { get; set; }

        public IList<Match> Draws { get; set; }

        public IList<User> Favoritiees { get; set; }
    }
}
