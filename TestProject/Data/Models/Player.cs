﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models
{
    public class Player
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int? TeamId { get; set; }

        public Team Team { get; set; }

        [ForeignKey("Country")]
        public int CountryOfBirthId { get; set; }

        public Country Country { get; set; }
    }
}
