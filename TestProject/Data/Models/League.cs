﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data
{
    public class League
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Round { get; set; }

        [ForeignKey("Sport")]
        public int SportId { get; set; }
        public Sport Sport { get; set; }

        [ForeignKey("Country")]
        public int? CountryId { get; set; }
        public Country Country { get; set; }

        public IList<Match> Matches { get; set; }

        public IList<Team> Teams { get; set; }

        public List<User> Users { get; set; }
    }
}
