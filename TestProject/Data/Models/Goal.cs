﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class Goal
    {
        public int Id { get; set; }

        public FirstHalf FirstHalfHomeGoals { get; set; }

        public int? FirstHalfIdHomeGoals { get; set; }

        public FirstHalf FirstHalfAwayGoals{ get; set; }

        public int? FirstHalfIdAwayGoals { get; set; }

        public SecondHalf SecondHalfHomeGoals { get; set; }

        public int? SecondHalfIdHomeGoals { get; set; }

        public SecondHalf SecondHalfAwayGoals { get; set; }

        public int? SecondHalfIdAwayGoals { get; set; }

        public int ScorerId { get; set; }

        public Player Scorer { get; set; }

        public int? AssisstantId { get; set; }

        public Player Assisstant { get; set; }

        public int GoalMinute { get; set; }
    }
}
