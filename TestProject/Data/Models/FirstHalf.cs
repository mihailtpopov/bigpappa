﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models
{
    public class FirstHalf
    {
        public int Id { get; set; }

        public int MatchId { get; set; }

        public Match Match { get; set; }

        public int? CurrMinute { get; set; }

        public int Corners { get; set; }

        public IList<Card> Cards { get; set; }

        public IList<Goal> AwayGoals { get; set; }

        public IList<Goal> HomeGoals { get; set; }
    }
}
