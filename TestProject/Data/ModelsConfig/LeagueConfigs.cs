﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ModelsConfig
{
    class LeagueConfigs : IEntityTypeConfiguration<League>
    {
        public void Configure(EntityTypeBuilder<League> builder)
        {
            builder.HasOne(m => m.Sport).WithMany(s => s.Leagues).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
