﻿using Data.Models;
using Data.ModelsConfig;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Data
{
    public class FastScoreContext : IdentityDbContext<User>
    {
        public FastScoreContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new FirstHalfConfigs());
            builder.ApplyConfiguration(new SecondHalfConfigs());
            builder.ApplyConfiguration(new TeamConfigs());
            builder.ApplyConfiguration(new LeagueConfigs());

            this.Seed(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.EnableSensitiveDataLogging();
        }

        private void Seed(ModelBuilder builder)
        {
            var sports = new List<Sport>()
            {
                new Sport() { Id = 1, Name = "Football", IsFavoriteSport = true, PhotoUrl = "/imges/sports/football.png" },
                new Sport() { Id = 2, Name = "Tennis", IsFavoriteSport = true, PhotoUrl = "/imges/sports/tennis.png" },
                new Sport() { Id = 3, Name = "Basketball", IsFavoriteSport = true, PhotoUrl = "/imges/sports/basketball.png" }
            };
            builder.Entity<Sport>().HasData(sports);

            var countries = new List<Country>()
            {
                new Country() { Id = 1, Name = "United Kingdom", FlagPhotoUrl = "/imges/country_flags/uk.png" },
                new Country() { Id = 2, Name = "Spain", FlagPhotoUrl = "/imges/country_flags/spain_flag.png"},
                new Country() { Id = 3, Name = "Germany", FlagPhotoUrl = "/imges/country_flags/germany_flag.png"},
                new Country() { Id = 4, Name = "Russia" },
                new Country() {Id= 5, Name = "Canada"}
            };
            builder.Entity<Country>().HasData(countries);

            var leagues = new List<League>()
            {
                new League() { Id = 1, CountryId = 1, Name = "Premier League", SportId = 1, },
                new League() { Id = 2, CountryId = 2, Name = "La Liga", SportId = 1},
                new League() { Id = 3, CountryId = 3, Name = "Bundesliga", SportId = 1},
                new League() { Id = 4, Name="ATP-SINGLES: Laver Cup(World)", SportId = 2 }
            };
            builder.Entity<League>().HasData(leagues);

            var teams = new List<Team>()
            {
                new Team() { Id = 1, Name = "Manchester United", LeagueId = 1, FlagPhotoUrl = "/imges/football_logos/manchester_united_logo.png"},
                new Team() { Id = 2, Name = "Manchester City", LeagueId = 1, FlagPhotoUrl = "/imges/football_logos/manchester_city_logo.png"},
                new Team() { Id=3, Name="Barcelona", LeagueId = 2, FlagPhotoUrl = "/imges/football_logos/barcelona_logo.png"},
                new Team() { Id= 4, Name="Real Madrid", LeagueId = 3, FlagPhotoUrl = "/imges/football_logos/real_madrid_logo.png"}
            };
            builder.Entity<Team>().HasData(teams);

            var matchStatuses = new List<MatchStatus>()
            {
                new MatchStatus() {Id = 1, Name = "Scheduled" },
                new MatchStatus() {Id = 2, Name = "In play" },
                new MatchStatus() {Id= 3, Name = "Finished"}
            };
            builder.Entity<MatchStatus>().HasData(matchStatuses);

            var matches = new List<Match>()
            {
                new Match() { Id=1, LeagueId = 1, HomeTeamId = 1, AwayTeamId = 2, MatchStatusId = 3, RefereeName = "Some Referee", StartingTime = DateTime.Now.AddDays(-9), HasPreview = true },
                new Match() { Id=2, LeagueId = 2, HomeTeamId = 4, AwayTeamId = 3, MatchStatusId = 2, RefereeName = "Some Referee", StartingTime = DateTime.Now.AddMinutes(-25)}
            };
            builder.Entity<Match>().HasData(matches);

            var firstHalfs = new List<FirstHalf>()
            {
                new FirstHalf() { Id=1, MatchId = 2, Corners = 3, CurrMinute = 25 }
            };
            builder.Entity<FirstHalf>().HasData(firstHalfs);

            var goals = new List<Goal>()
            {
                new Goal() { Id = 1, FirstHalfIdHomeGoals = 1, GoalMinute = 10, ScorerId = 3}
            };
            builder.Entity<Goal>().HasData(goals);

            var players = new List<Player>()
            {
                new Player(){ Id = 1, Name = "Medvedev D.", CountryOfBirthId = 4},
                new Player() { Id = 2, Name = "Shapovalov D.", CountryOfBirthId = 5},
                new Player() { Id= 3, Name = "Sergio Ramos", CountryOfBirthId = 2, TeamId = 4}
            };
            builder.Entity<Player>().HasData(players);

            var secondHalfs = new List<SecondHalf>()
            {
                new SecondHalf() { Id = 2, Corners = 6, MatchId = 2 }
            };
        }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<League> Leagues { get; set; }

        public DbSet<Match> Matches { get; set; }

        public DbSet<MatchArchive> MatchesArchive { get; set; }

        public DbSet<MatchStatus> MatchStatuses { get; set; }

        public DbSet<Odds> Odds { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<Sport> Sports { get; set; }

        public DbSet<Team> Teams { get; set; }
    }
}
