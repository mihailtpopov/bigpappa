﻿
(() => {
    var timeout;
    var popupContainer = $('.outerContainer');
    var messageElement = $('.content');

    ShowFriendlyCoordinationMessage(popupContainer, messageElement);
    ManageUserFavoritesMatches();
    SetOnLoadJS();

    function ShowElementNextToCursor(popupContainer, messageElement, message, event) {

        clearTimeout(timeout);

        timeout = setTimeout(() => {
            popupContainer.css({ 'display': 'table', 'left': (event.clientX + 5) + 'px', 'top': (event.clientY + 14) + 'px' });
            messageElement.text(message);
        }, 1000);

        popupContainer.css('display', 'none');
    }
    function ShowFriendlyCoordinationMessage(popupContainer, messageElement) {
        $('.match-div').mousemove((event) => {
            let message = 'Click for match detail!';
            ShowElementNextToCursor(popupContainer, messageElement, message, event);
        });

        $('.match-star').mousemove((event) => {
            let message = 'Click to add to Favorites!';
            ShowElementNextToCursor(popupContainer, messageElement, message, event);
        });

        $('.special-container-match').on('mouseleave', () => {
            clearTimeout(timeout)
        });
    }
    function ManageUserFavoritesMatches() {
        $('.match-star').click(function () {
            let star = $('.match-star-img');
            let matchId = $(this).attr('id');

            if (star.attr('src') == '/imges/empty_star.png') {
                $.ajax({
                    method: "POST",
                    url: "/Account/AddMatchToFavorites",
                    data: { matchId: matchId },
                    success: () => {
                        star.attr('src', '/imges/full_star.png');
                        $('.outer-match-added-div').css('display', 'table');
                        $('.inner-match-added-div #content').text('Added to Favorites.');
                        let favoriteMatchesCount = $('#favoritesDot').text();
                        $('#favoritesDot').text(+favoriteMatchesCount + 1);
                    },
                    error: () => {
                        window.location.href = "/Account/Login";
                    }
                });
            }
            else {
                $.ajax({
                    method: "POST",
                    url: "/Account/RemoveMatchFromFavorites",
                    data: { matchId: matchId },
                    success: () => {
                        star.attr('src', '/imges/empty_star.png');
                        $('.outer-match-added-div').css('display', 'table');
                        $('.inner-match-added-div #content').text('Removed from Favorites.');
                        let favoriteMatchesCount = $('#favoritesDot').text();
                        $('#favoritesDot').text(+favoriteMatchesCount - 1);
                    }
                });
            }
        });
    }
    function SetOnLoadJS() {
        if ($('.live-bet[matchStatusId=2]').length > 0) {

            $('.live-bet[matchStatusId=2]').css({ 'background-color': 'red', 'color': 'white' });

            setInterval(() => {
                $('.tick-sign').toggle();
                $('.live-bet[matchStatusId=2]').toggle();
            }, 1000);

            let halfTimeInterval = setInterval(() => {

                $('.curr-minute').each(function (index, element) {

                    let currMin = +$(element).text();
                    $(element).text(currMin + 1);

                    if ($(element).text() === "23") {

                        $(element).text('Half-time');
                        clearInterval(halfTimeInterval);

                        let halfTimeBreak = setTimeout(() => {
                            $('.curr-minute').text(45);

                            halfTimeInterval = setInterval(() => {
                                $('.curr-minute').each(function (index, element) {
                                    let currMin = +$(element).text();
                                    $(element).text(currMin + 1);

                                    if ($(element).text() == '90') {

                                        clearTimeout(halfTimeBreak);

                                        $(element).text('Finished');
                                        $(element).css('color', 'black');

                                        let matchCurrResult = $(`.match-current-result[matchId="${$(element).attr('matchId')}"`);

                                        let homeTeamGoals = +matchCurrResult.children().first().text();
                                        let awayTeamGoals = +matchCurrResult.children().last().text();

                                        if (homeTeamGoals > awayTeamGoals) {
                                            let matchId = $(element).attr('matchId');
                                            let homeTeamName = $(`.home-team-name[matchId="${matchId}"]`);
                                            homeTeamName.css('font-weight', 'bold');
                                        }
                                        else if (awayTeamGoals > homeTeamGoals) {
                                            $(`.away-team-name[matchId="${$(element).attr('matchId')}"]`).css('font-weight', 'bold');
                                        }

                                        matchCurrResult.css({ 'color': 'black', 'font-weight': 'bold' });
                                    }
                                });
                            }, 60000);

                        }, 900000);
                    }
                });

            }, 60000)
        }
    }
})();
