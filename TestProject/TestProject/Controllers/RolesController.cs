﻿using Consts;
using Data;
using Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Mapping;
using TestProject.ViewModels.RoleVMs;

namespace TestProject.Controllers
{
    [Authorize(Roles = UserRoleConsts.ADMIN)]
    public class RolesController : Controller
    {
        private readonly RoleManager<Role> roleManager;
        private readonly FastScoreContext context;

        public RolesController(RoleManager<Role> roleManager, FastScoreContext context)
        {
            this.roleManager = roleManager;
            this.context = context;
        }

        public async Task<IActionResult> Get(string roleId)
        {
            var role = await context.Roles.FirstOrDefaultAsync(r => r.Id == roleId);

            if (role == null)
            {
                throw new ArgumentNullException();
            }

            ViewBag.roleId = roleId;

            var userRoleIds = context.UserRoles.Where(ur => ur.RoleId == roleId).Select(ur => ur.UserId);
            var users = context.Users.Where(u => userRoleIds.Contains(u.Id)).ToList();

            var roleInfoVM = new RoleInfoViewModel()
            {
                RoleId = roleId,
                RoleName = role.Name,
                UsersInRole = users
            };

            return View(roleInfoVM);
        }

        public async Task<IActionResult> GetAll()
        {
            var roles = await this.context.Roles.ToListAsync();
            return View(roles);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateRoleViewModel createRoleVM)
        {
            if (ModelState.IsValid)
            {
                var role = InputMapper.Map(createRoleVM);

                var result = await roleManager.CreateAsync(role);

                if (result.Succeeded)
                {
                    await context.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(createRoleVM);
        }

        public async Task<IActionResult> Edit(string roleId)
        {
            var role = await context.Roles.FirstOrDefaultAsync(r => r.Id == roleId);

            if (role == null)
            {
                throw new ArgumentNullException();
            }

            ViewBag.roleId = roleId;

            var userRoleIds = context.UserRoles.Where(ur => ur.RoleId == roleId).Select(ur => ur.UserId);
            var users = context.Users.Where(u => userRoleIds.Contains(u.Id)).ToList();

            var roleInfoVM = new RoleInfoViewModel()
            {
                RoleId = roleId,
                RoleName = role.Name,
                UsersInRole = users
            };

            return View(roleInfoVM);
        }

        [HttpPost]
        public IActionResult Edit(RoleInfoViewModel roleInfoVM)
        {
            return View();
        }
    }
}
