﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.ViewModels
{
    public class LeagueViewModel
    {
        public int LeagueId { get; set; }

        public int LeagueSportId { get; set; }

        public string LeagueName { get; set; }

        public string LeagueCountryName { get; set; }

        public string LeagueCountryFlagPhotoUrl { get; set; }
    }
}
