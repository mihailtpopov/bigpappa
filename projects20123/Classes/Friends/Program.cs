﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Humann;
using Pet;
namespace Friends
{
    class Program
    {
        static void Main(string[] args)
        {
            Kid michael = new Kid();
            Dog michaelsDog = new Dog("Saba");
   
            michael.CallTheDog(michaelsDog);
            michael.WagTheDog(michaelsDog);
        }
    }
}
