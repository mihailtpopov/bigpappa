﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{
    class example
    {
        public static void Swap<K>(ref K number1,ref K number2)
        {
            K passingThrough = number1;
            number1 = number2;
            number2 = passingThrough;
        }
    }
}
