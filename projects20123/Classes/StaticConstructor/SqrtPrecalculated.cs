﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticConstructor
{
    static class SqrtPrecalculated
    {
        private const int MaxValue = 1000;
        private static int[] sqrtValues;

        static public SqrtPrecalculated()
        {
            sqrtValues = new int[MaxValue + 1];

            for(int i = 0;i<sqrtValues.Length;i++)
            {
                sqrtValues[i] = (int)Math.Sqrt(i);
            }
        }
        public static int GetSqrt(int value)
        {
            if(value<0 || value>1000)
            {
                throw new ArgumentException(String.Format("The value should be in the range [0..{0}].", MaxValue));
            }
            return sqrtValues[value];
        }
    }
}
