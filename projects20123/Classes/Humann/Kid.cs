﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pet;
namespace Humann
{
    public class Kid
    {
        public void CallTheDog(Dog dog)
        {
            Console.WriteLine("Come on, " + dog.Name);
        }
        public void WagTheDog(Dog dog)
        {
            dog.Bark();
        }
    }
}
