﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Const
{
    class Color
    {
        public static readonly Color Black = new Color(0, 0, 0);
        public static readonly Color White = new Color(255, 255, 255);
        private int red;
        private int blue;
        private int green;
        public Color(int red, int blue, int green)
        {
            this.red = red;
            this.blue = blue;
            this.green = green;
        }
    }
}
