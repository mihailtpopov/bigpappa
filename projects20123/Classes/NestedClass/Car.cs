﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedClass
{
    
    class Car
    {
        Door frontLeftDoor;
        Door frontRightDoor;
        Door backLeftDoor;
        Door backRightDoor;
        public Engine engine;
        public Engine Engine
        {
            get { return this.engine; }
            set { this.engine = value; }
        }
        public Car()
        {
            engine = new Engine();
            engine.horsePower = 2000;
        }
        public class Engine
        {
          public int horsePower;
        }
    }
}
