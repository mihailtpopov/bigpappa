﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedClass
{
    public class OuterClass
    {
        private string name;
        public OuterClass(string name)
        {
            this.name = name;
        }
        public class NestedClass 
        {
            private string name;
            private OuterClass parent;
            public NestedClass(string name, OuterClass parent)
            {
                this.name = name;
                this.parent = parent;
            }
             public void PrintNames()
        {
            Console.WriteLine("Nested name: " + this.name);
            Console.WriteLine("Outer name: " + this.parent.name);
        }
        }
      
    }
}
