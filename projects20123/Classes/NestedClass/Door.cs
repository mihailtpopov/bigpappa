﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedClass
{
    class Door
    {
        private string color;
        private string material;
        public Door(string color,string material)
        {
            this.color = color;
            this.material = material;
        }
        public int MyProperty { get; set; }
        public string Material
        {
            get { return material; }
            set { this.material = value; }
        }
        public string Color
        {
            get { return color; }
            set { this.color = value; }
        }
    }
}
