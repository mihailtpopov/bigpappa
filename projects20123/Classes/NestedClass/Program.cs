﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestedClass
{
    class Program
    {
        static void Main(string[] args)
        {
            OuterClass outer = new OuterClass("outer");
            OuterClass.NestedClass nested = new OuterClass.NestedClass("nested",outer);

            //

            Car car = new Car();
            Console.WriteLine(car.Engine.horsePower);
            Car.Engine engine = new Car.Engine();
            Console.WriteLine(engine.horsePower);
        }
    }
}
