﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class CircleTest
    {
        static void Main()
        {
            Circle firstCircle = new Circle(3);
            firstCircle.PrintSurface();
            Circle secondCircle = new Circle(5);
            secondCircle.PrintSurface();
            
        }
    }
}
