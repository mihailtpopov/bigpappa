﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            ConstReadonlyModifiersTest t = new ConstReadonlyModifiersTest(5);
            Console.WriteLine(ConstReadonlyModifiersTest.PI);
            Console.WriteLine(t.size);
           // This will cause compile-time error Console.WriteLine(ConstReadonlyModifiersTest.size);
        }
    }
}
