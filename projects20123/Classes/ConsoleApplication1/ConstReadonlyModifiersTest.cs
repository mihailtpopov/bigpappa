﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ConstReadonlyModifiersTest
    {
        public const double PI = 3.14159265;
        public readonly double size;
        private string MyPropertyVariable = "";


        public ConstReadonlyModifiersTest()
        {

        }
        public ConstReadonlyModifiersTest(int size)
        {
            this.size = size;
        }
    }
}
