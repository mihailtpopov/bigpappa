﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pet
{
    public class Dog
    {
        public static string paws;
        private string name;
        private Collar collar;
        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        public Dog(string name)
        {
            this.name = name;
            this.collar = new Collar();
        }
        public void Bark()
        {
            Console.WriteLine("wow-wow");
        }
        public void DoSth()
        {
            this.Bark();
        }
    }
}
