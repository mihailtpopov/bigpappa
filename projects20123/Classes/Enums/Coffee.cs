﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    
   public class Coffee
    {
       public Coffee(CoffeeSize size)
       {
           this.size = size;
       }
       public enum CoffeeSize
       {
           Small = 100, Normal = 150, Double = 300
       }
      private CoffeeSize size;
      public CoffeeSize Size
      {
          get { return size; }
      }
       public double CalcPrice()
      {
           switch(Size)
           {
               case CoffeeSize.Small:
                   return 0.20;
               case CoffeeSize.Normal:
                   return 0.40;
               case CoffeeSize.Double:
                   return 0.60;
               default: throw new InvalidOperationException("Unsupported coffee size - " + (int)Size);
           }
      }
    }
}
