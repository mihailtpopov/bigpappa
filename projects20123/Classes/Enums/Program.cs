﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums
{
    class Program
    {
        static void Main(string[] args)
        {
            Coffee normalCoffee = new Coffee(Coffee.CoffeeSize.Normal);
            Coffee doubleCoffee = new Coffee(Coffee.CoffeeSize.Double);

            double normalCoffeePrice = normalCoffee.CalcPrice();
            Console.WriteLine("The {0} coffee is {1} ml. and the price is {2}",normalCoffee.Size,(int)normalCoffee.Size,normalCoffeePrice);
            Console.WriteLine("The {0} coffee is {1} ml.",doubleCoffee.Size,(int)doubleCoffee.Size);
        }
    }
}
