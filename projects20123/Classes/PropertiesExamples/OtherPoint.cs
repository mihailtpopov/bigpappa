﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertiesExamples
{
    class OtherPoint
    {
       private double[] coordinates;
        public OtherPoint (int xCoord, int yCoord)
	    {
            this.coordinates = new double[2];

            //Initializing x coordinates
            coordinates[0] = xCoord;
            //Initiliazing y coordinates
            coordinates[1] = yCoord;
	    }
      public double X
        {
            get { return coordinates[0]; }
            set { coordinates[0] = value; }
        }
        public double Y
      {
          get { return coordinates[1]; }
          set { coordinates[1] = value; }
      }
    }
}
