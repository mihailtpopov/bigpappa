﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class AnimalShelter<ТAnimal>
    {
        private readonly int shelterCapacity;
        private ТAnimal[] animalsList;
        private int usedPlaces;
        public AnimalShelter(int shelterCapacity)
        {
            this.shelterCapacity = shelterCapacity;
            animalsList=new ТAnimal[shelterCapacity];
            this.usedPlaces = 0;
        }

        public void Shelter(ТAnimal newAnimal)
        {
            if(this.usedPlaces>animalsList.Length)
            {
                throw new InvalidOperationException("Shelter is full!");
            }
            this.animalsList[this.usedPlaces] = newAnimal;
            this.usedPlaces++;
        }
        public ТAnimal Release(int index)
        {
            if(index<0 || index>this.usedPlaces-1)
            {
                throw new ArgumentException("Invalid cell index - " + index);
            }
            ТAnimal releasedAnimal = this.animalsList[index];
            for(int i = index;i<this.usedPlaces-1;i++)
            {
                this.animalsList[index] = this.animalsList[index + 1];
            }
            this.animalsList[usedPlaces - 1] = default(ТAnimal);

            return releasedAnimal;
        }
    }
}
