﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            AnimalShelter<Dog> dogsShelter = new AnimalShelter<Dog>(10);
            AnimalShelter<Cat> catsShelter = new AnimalShelter<Cat>(15);
            dogsShelter.Shelter(new Dog());
            dogsShelter.Shelter(new Dog());
            dogsShelter.Shelter(new Dog());
            Dog releasedDog = dogsShelter.Release(1);
            Console.WriteLine(releasedDog);
            releasedDog = dogsShelter.Release(0);
            Console.WriteLine(releasedDog);
            releasedDog = dogsShelter.Release(0);
            releasedDog = dogsShelter.Release(0);//Exception
        }
    }
}
