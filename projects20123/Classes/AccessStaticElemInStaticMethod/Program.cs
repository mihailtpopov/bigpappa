﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessStaticElemInStaticMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            double radius = 5;
            double perimeter  = Circle.CalculatePerimeter(radius);
            Console.WriteLine("Circle with raidius {0} has perimeter {1}",radius,perimeter);
        }
    }
}
