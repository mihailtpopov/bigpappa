﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccessStaticElemInStaticMethod
{
    class Circle
    {
        private const double PI  = 3.1415;
        private double r;
        public Circle(double r)
        {
            this.r = r;
        }
        public static double CalculatePerimeter(double radius)
        {
            return (2 * PI * radius);
        }
        public static void PrintRadius(Circle circle)
        {
            Console.WriteLine(r);
        }
    }
}
