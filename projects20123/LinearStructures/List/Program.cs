﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List
{
    class Program
    {
        public static void PrintList(List<int> listToPrint)
        {
            Console.Write("{ ");
            foreach (var item in listToPrint)
            {
                Console.Write(item + " ");
            }
            Console.Write("}");
            Console.WriteLine();
        }
        public static List<int> GetPrimes(int start, int end)
        {
            List<int> primeList = new List<int>();
            bool prime = true;
            for(int num = start;num<end;num++)
            {
                double sqrtValue = Math.Sqrt(num);
                for(int div = 2;div<=sqrtValue;div++)
                {
                    if(num%div==0)
                    {
                        prime = false;
                        break;
                    }
                    if(prime)
                    {
                        primeList.Add(num);
                    }
                }
            }
            return primeList;
        }
        static void Main(string[] args)
        {
            List<int> primeList = GetPrimes(200, 300);
            foreach(int prime in primeList)
            {
                Console.Write(prime + " ");
            }

            //
            List<int> firstList = new List<int>();
            firstList.Add(1);
            firstList.Add(2);
            firstList.Add(3);
            firstList.Add(4);
            List<int> secondList = new List<int>();
            secondList.Add(2);
            secondList.Add(5);
            secondList.Add(6);
            secondList.Add(4);
            List<int> intersect = new List<int>();
            intersect = IntersectAndUnion.GetIntersectList(firstList, secondList);
            List<int> union = new List<int>();
            union = IntersectAndUnion.GetUnioList(firstList, secondList);
            PrintList(firstList);
            PrintList(secondList);
            PrintList(intersect);
            PrintList(union);

            //
            int[] arr = new int[] {1,2,3,4};
            List<int> list = new List<int>(arr);
            int[] newArray = list.ToArray();
        }
    }
}
