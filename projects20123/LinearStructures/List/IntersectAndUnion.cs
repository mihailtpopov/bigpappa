﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace List
{
   public class IntersectAndUnion
    {
        public static List<int> GetIntersectList(List<int> firstList, List<int> secondList)
        {
            List<int> intersect = new List<int>();
            intersect.AddRange(firstList);
            foreach(var item in secondList)
            {
                if(intersect.Contains(item))
                {
                    continue;
                }
                else
                {
                    intersect.Add(item);
                }
            }
            return intersect;
        }

        public static List<int> GetUnioList(List<int> firstList, List<int> secondList)
        {
            List<int> union = new List<int>();
            
            foreach (var item in secondList)
            {
                if (firstList.Contains(item))
                {
                    union.Add(item);
                }
            }
            return union;
        }
    }
}
