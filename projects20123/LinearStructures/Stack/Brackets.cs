﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Brackets
    {
        public static void CorrectBrackets(string expression)
        {
            Stack<int> brackets = new Stack<int>();
            char openingBracket = '(';
            char closingBracket = ')';
            bool correctBrackets = true;
            for(int i = 0;i<expression.Length;i++)
            {
                if(expression[i].Equals(openingBracket))
                {
                    brackets.Push(i);
                }
                else if(expression[i].Equals(closingBracket))
                {
                    if(brackets.Count==0)
                    {
                        correctBrackets = false;
                        break;
                    }
                    brackets.Pop();
                }
            }
            if(brackets.Count!=0)
            {
                correctBrackets = false;
            }
            Console.WriteLine("Are the brackets correct? " + correctBrackets);
        }
    }
}
