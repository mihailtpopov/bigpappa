﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<string> stack = new Stack<string>();
            stack.Push("1. Ivan");
            stack.Push("2. Gabriel");
            stack.Push("3. Manganii");
            stack.Push("4. Margarita");
            string topName = stack.Peek();
            Console.WriteLine("Top= " + topName);
            while(stack.Count>0)
            {
                string personName = stack.Pop();
                Console.WriteLine(personName);
            }

            Brackets.CorrectBrackets("(1+((2*2)+2*(2+8)))");
        }
    }
}
