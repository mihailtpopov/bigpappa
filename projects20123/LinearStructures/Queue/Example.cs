﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    public class Example
    {
        public static void GetSequenceElement(int startingNumber, int elementValue)
        {
            Queue<int> queue = new Queue<int>();
            queue.Enqueue(startingNumber);
            int index = 0;
            Console.Write("S =");
            while(queue.Count>0)
            {
                index++;
                int current = queue.Dequeue();
                Console.Write(" " + current);
                if(current == elementValue)
                {
                    Console.WriteLine();
                    Console.WriteLine("Index = " + index);
                    return;
                }
                queue.Enqueue(current + 1);
                queue.Enqueue(2 * current);
            }
        }
    }
}
