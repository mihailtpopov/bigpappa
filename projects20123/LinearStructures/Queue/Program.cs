﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace QueueExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<string> queue= new Queue<string>();
            queue.Enqueue("Message One");
            queue.Enqueue("Message two");
            queue.Enqueue("Message three");
            queue.Enqueue("Message four");

            while(queue.Count>0)
            {
                string message = queue.Dequeue();
                Console.WriteLine(message);
            }

            int n = 3;
            int elementValue = 16;
            Queue.Example.GetSequenceElement(n, elementValue);

        }
    }
}
