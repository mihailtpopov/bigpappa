﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearStructures
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[4];
            int[] extendedArr = new int[4];
            Array.Copy(arr, extendedArr, 3);
            Array.Copy(arr, 2, extendedArr, 5, 2);
        }
    }
}
