﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindLargestSeqAscending
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int maxSeq = int.MinValue;
            List<int> seqList = new List<int>();
            List<int> keepSeq = new List<int>();
            for(int i = 0;i<array.Length;i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            for(int i = 0, next=1;i<array.Length-1;i++,next=i+1)
            {
                while(next<array.Length)
                {
                    int keepI = array[i];
                    for(int start = next;start<array.Length;start++)
                    {
                        
                        if(keepI<array[start])
                        {
                            if(keepI==array[i])
                            {
                                seqList.Add(keepI);
                            }
                            keepI = array[start];
                            seqList.Add(keepI);
                        }
                    }
                       if(seqList.Count>maxSeq)
                       {
                           maxSeq=seqList.Count;
                           keepSeq = seqList.ToList();
                           seqList.Clear();
                       }
                       seqList.Clear();
                    next++;
                 }
             }
            if(keepSeq.Count==0)
            {
                Console.WriteLine("There isn't ascending sequence! All the elements are the same or this is " +
                    "consecutive descending array");
                return;
            }
            Console.Write("{");
            for(int i =0;i<keepSeq.Count;i++)
            {
                if (i != keepSeq.Count - 1)
                {
                    Console.Write(keepSeq[i] + ", ");
                    continue;
                }
                Console.Write(keepSeq[i]);
            }
            Console.Write("}");
        }
    }
}
