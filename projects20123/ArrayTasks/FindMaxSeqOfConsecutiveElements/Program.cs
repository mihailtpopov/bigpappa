﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindMaxSeqOfConsecutiveElements
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the length of the array");
            int length = int.Parse(Console.ReadLine());
            int[] array = new int[length];
            int counter = 1;
            int maxSeq = 1;
            int value=default(int);
            int equalSeq=int.MinValue;
            Console.WriteLine("Enter the array's elements:");
            for(int index = 0;index<array.Length;index++)
            {
                
                array[index] = int.Parse(Console.ReadLine());
            }
            if (length == 1)
            {
                Console.WriteLine("The maximum sequence is: {"+array[0]+"}");
                Environment.Exit(array[0]);
            }
            for(int index=0;index<array.Length-1;index++)
            {
                    if(array[index]==array[index+1])
                    {
                        if (index < array.Length - 2)
                        {
                            counter++;
                            continue;
                        }
                        counter++;
                    }

                if(counter>=maxSeq)
                {
                    if(counter==maxSeq)
                    {
                        equalSeq++;
                        counter = 1;
                        continue;
                    }
                    equalSeq = 0;
                    maxSeq = counter;
                    counter = 1;
                    value = array[index];
                }
            }

            if (equalSeq==0)
            {
                Console.Write("The maximum sequence is: {");
                for (int i = 0; i < maxSeq; i++)
                {
                    if (i == maxSeq - 1)
                    {
                        Console.Write(value);
                        break;
                    }
                    Console.Write(value + ", ");
                }
                Console.WriteLine("}");
            }
            else
            {
                Console.WriteLine("The maximum consecutive sequence is with {0} elements!",maxSeq);
            }
        }
    }
}
