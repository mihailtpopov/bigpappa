﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintConsecutiveSequenceOfAscendingElements
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int start = 0;
            int len = 1;
            int bestLen = 1;
            int bestStart=int.MinValue;
            for(int i=0;i<array.Length;i++)
            {
                
                array[i] = int.Parse(Console.ReadLine());
            }
             
            for(int i = 0,j=i+1;i<array.Length-2;i++,j++,len=1)
            {
                if(array[i]<array[j])
                   {
                       start = i;
                       len++;
                       for (j = i+1; j < array.Length - 1; j++)
                       {
                           if (array[j] < array[j + 1])
                           {
                               len++;
                           }
                           else
                           {
                               j = array.Length - 1;
                           }
                       }
                       j = i + 1;
                   }
                
                if(len>bestLen)
                    {
                      bestLen=len;
                      bestStart=start;
                     
                    }
            }
            int length = bestLen + bestStart;
            Console.Write("{");
            for (int index = bestStart; index < length ;index++ )
            {
                if(index==length-1)
                {
                    Console.Write(array[index]);
                    break;
                }
                Console.Write(array[index] + ", ");
            }
            Console.Write("}");
                Console.ReadKey();
            }
        }
    }

