﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIfArraysAreEqual
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first array length");
            int lengthOne = int.Parse(Console.ReadLine());
            int[] arrayOne = new int[lengthOne];

            Console.WriteLine("Enter second array length");
            int lengthTwo = int.Parse(Console.ReadLine());
            int[] arrayTwo = new int[lengthTwo];

            bool equalLength = lengthOne == lengthTwo;
            bool areEqual = true;
            if(equalLength)
            {
                Console.WriteLine("Enter first array elements:");
                for(int index=0;index<lengthOne;index++)
                {
                    arrayOne[index] = int.Parse(Console.ReadLine());
                }

                Console.WriteLine("Enter second array elements:");
                for(int index=0;index<lengthTwo;index++)
                {
                    arrayTwo[index] = int.Parse(Console.ReadLine());
                }
                for(int index=0;index<arrayTwo.Length;index++)
                {
                    if(arrayOne[index]!=arrayTwo[index])
                    {
                        areEqual = false;
                        break;
                    }
                }
            }
            else
            {
                throw new ArgumentException("Arrays with different lengths can't be equal");
            }
            Console.WriteLine("Arrays equal? - "+areEqual);
        }
    }
}
