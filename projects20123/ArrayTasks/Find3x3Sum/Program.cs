﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Find3x3Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int[,] matrix =new int[n,m];
            List<int> matrixList = new List<int>();
            List<int> keepBestMatrix = new List<int>();
            int sum = 0;
            int end = 0;
            int maxSum = int.MinValue;
            for(int i = 0;i<matrix.GetLength(0);i++)
            {
                for(int j = 0;j<matrix.GetLength(1);j++)
                {
                    matrix[i,j] = int.Parse(Console.ReadLine());
                }
                Console.WriteLine();
            }
            
            for(int forward=0, down = 0;forward<n-2 || down<m-2;forward++,down++)
            {
                for(int start = forward ;start<=forward+2;start++)
                {
                    for(end = down;end<=down+2;end++)
                    {
                        sum+=matrix[start,end];
                        matrixList.Add(matrix[start,end]);
                    }
                    end=down;
                }
                if(sum>maxSum)
                {
                    maxSum=sum;
                    keepBestMatrix.Clear();
                    keepBestMatrix=matrixList.ToList();
                }
            }
            for(int i =0;i<keepBestMatrix.Count;i++)
            {
                Console.Write(i);
                if(i%3==0)
                {
                    Console.WriteLine();
                }
            }
        }
    }
}
