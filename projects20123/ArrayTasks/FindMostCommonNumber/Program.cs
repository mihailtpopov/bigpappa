﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindMostCommonNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            for(int i = 0;i<array.Length;i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            int length = array.Length;
            int count = 1;
            int mostCommon=0;
            int maxCount=int.MinValue;
            int countSame = 1;
            for(int i = 0;i<array.Length-1;i++,count=1)
            {
                for(int j = i+1;j<array.Length;j++)
                {
                    
                    if(array[i]==array[j])
                    {
                        count++;
                    }
                }
             
                if(count>=maxCount)
                {
                    if(count==maxCount)
                    {
                        countSame++;
                        continue;
                    }
                    maxCount = count;
                    mostCommon = array[i];
                    countSame = 1;
                }
            }

            if(countSame>1)
            {
                Console.WriteLine("There is {0} numbers with same number of ocurrencies",countSame);
                Environment.Exit(1);
            }

            Console.WriteLine("The most common number is {0} with {1} ocurrencies",mostCommon,maxCount);
        }
    }
}
