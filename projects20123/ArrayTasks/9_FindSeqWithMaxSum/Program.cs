﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_FindSeqWithMaxSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            List<int> seqMaxSumList = new List<int>();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }

            seqMaxSumList = ConsecutiveSequenceWithMaxSum.Find(array).ToList();
            ConsecutiveSequenceWithMaxSum.Print(seqMaxSumList);
        }
    }
}
