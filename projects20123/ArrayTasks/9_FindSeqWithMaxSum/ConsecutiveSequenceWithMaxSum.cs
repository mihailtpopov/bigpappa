﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_FindSeqWithMaxSum
{
    class ConsecutiveSequenceWithMaxSum
    {
        public static void Print(List<int> keepSeq)
        {
            Console.Write("{");
            for (int i = 0; i < keepSeq.Count; i++)
            {
                if (i != keepSeq.Count - 1)
                {
                    Console.Write(keepSeq[i] + ", ");
                    continue;
                }
                Console.Write(keepSeq[i]);
            }
            Console.Write("}");
        }
        public static List<int> Find(int[] array)
        {
            int start, end, sum = 0, maxSum = 0;
            int counter = 1;
            List<int> seqList = new List<int>();
            List<int> keepSeq = new List<int>();

            for (int i = 0; i < array.Length - 1; i++, seqList.Clear(), sum = 0, counter = 1)
            {
                while (counter <= array.Length - i)
                {
                    for (int k = i, iteration = counter; iteration > 0; k++, iteration--)
                    {
                        sum += array[k];
                    }
                    if (sum > maxSum)
                    {
                        maxSum = sum;
                        for (start = i, end = counter; end > 0; start++, end--)
                        {
                            seqList.Add(array[start]);
                        }
                        if (seqList.Sum() > keepSeq.Sum())
                        {
                            keepSeq.Clear();
                            keepSeq = seqList.ToList();
                        }
                        seqList.Clear();
                    }
                    sum = 0;
                    counter++;
                }
            }
            return keepSeq;
        }
    }
}
