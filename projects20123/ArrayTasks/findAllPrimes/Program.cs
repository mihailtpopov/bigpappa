﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace findAllPrimes
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isPrime = true;
            for(int i = 1;i<=10000;i++, isPrime=true)
            {
                for( int j = 2;j<=Math.Sqrt(i);j++)
                {
                    if(i%j==0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
