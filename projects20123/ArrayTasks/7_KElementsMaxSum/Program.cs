﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_KElementsMaxSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = int.Parse(Console.ReadLine());
            int K = int.Parse(Console.ReadLine());
            int sum = 0;
            int maxSum = int.MinValue;
            int start = 0;
            int bestStart = 0;
            if(K>=N)
            {
                throw new ArgumentException("N must be greater than K!");
            }
            int[] array = new int[N];
            for (int i = 0; i < array.Length;i++ )
            {
                array[i] = int.Parse(Console.ReadLine());
            }
                for (int i = 0,j = 0; i <= N - K;i++,j=i)
                {

                    for (int k = K;k>0 ; k--)
                    {
                        sum += array[j];
                        j++;
                    }
                    if (sum > maxSum)
                    {
                        start = i;
                        maxSum = sum;
                    }
                    sum = 0;
                }
            Console.Write("{");
            for (int i = start; i < K + start;i++)
            {
                if (i != K + start - 1)
                {
                    Console.Write(array[i] + ", ");
                    continue;
                }
                Console.Write(array[i]);
            }
            Console.Write("}");
        }
    }
}
