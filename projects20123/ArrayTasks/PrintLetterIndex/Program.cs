﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLetterIndex
{
    class Program
    {
        static void Main(string[] args)
        {
            char[] latinLetters = new char[]{'a','b','c','d','e','f','g','h','i','k','l','m','n','o','p','q','r','s','t','v','x','y','z'};
            string word = Console.ReadLine();
            for(int symbol = 0;symbol<word.Length;symbol++)
            {
                for(int index = 0;index<latinLetters.Length;index++)
                {
                    if(word[symbol]==latinLetters[index])
                    {
                        Console.WriteLine(index);
                        break;
                    }
                }
            }
        }
    }
}
