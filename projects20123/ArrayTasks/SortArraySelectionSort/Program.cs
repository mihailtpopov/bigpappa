﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortArraySelectionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int smallest = int.MinValue;
            int temp;
            int[] array = new int[n];
            for(int i = 0; i <array.Length;i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            for(int i = 0;i<array.Length-1;i++)
            {
                smallest=i;
                for(int k=i+1;k<array.Length;k++)
                {
                    if(array[smallest]>array[k])
                    {
                        smallest = k;
                    }
                }
                temp = array[smallest];
                array[smallest] = array[i];
                array[i] = temp;
            }
            for(int i =0;i<array.Length;i++)
            {
                Console.Write(array[i]);
            }
        }
    }
}
