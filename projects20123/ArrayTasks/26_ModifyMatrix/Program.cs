﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _26_ModifyMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int  n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int[,] matrix = new int[n,m];
            int sum = 0;
            int maxSum = int.MinValue;
            List<int> keepPlatform = new List<int>();
            int col  = 0;
            int bestRow = 0;
            int bestCol = 0;
            for (int i = 0; i < matrix.GetLength(0);i++)
            {
                for(int j = 0;j<matrix.GetLength(1);j++)
                {
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
                Console.WriteLine();
            }
            
                for (int row = 0;row < matrix.GetLength(0) - 2; row++, col = 0)
                {
                    while (col < matrix.GetLength(1) - 2)
                    {
                        sum += matrix[row, col] + matrix[row, col + 1] + matrix[row,col + 2] + matrix[row + 1,col]
                            + matrix[row + 1, col + 1] + matrix[row + 1, col + 2];
                        if (sum > maxSum)
                        {
                            maxSum = sum;
                            keepPlatform.Clear();
                            bestRow = row;
                            bestCol = col;
                        }
                        sum = 0;
                        col++;
                    }
                }
                Console.WriteLine("{0} {1} {2}",matrix[bestRow,bestCol],matrix[bestRow,bestCol+1],matrix[bestRow,bestCol+2]);
                Console.WriteLine("{0} {1} {2}", matrix[bestRow+1,bestCol],matrix[bestRow+1,bestCol+1],matrix[bestRow+1,bestCol+2]);
                Console.WriteLine("{0} {1} {2}", matrix[bestRow+2,bestCol],matrix[bestRow+2,bestCol+1],matrix[bestRow+2,bestCol+2]);
        }
    }
}
