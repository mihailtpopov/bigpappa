﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_PrintMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = int.Parse(Console.ReadLine());
            Print.FirstKindMatrix(length);
            Console.WriteLine();
            Print.SecondKindMatrix(length);
            Console.WriteLine();
            Print.ThirdKindMatrix(length);
       }
    }
}
