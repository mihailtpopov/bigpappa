﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_PrintMatrix
{
    class Print
    {
        public static void FirstKindMatrix(int length)
        {
            int[,] matrix = new int[length, length];
            int next = 1;
            for (int i = 0; i < length; i++, next += 1)
            {

                for (int k = 0; k < length; k++, next += 4)
                {
                    matrix[i, k] = next;
                }
                next -= length * 4;
            }
            for (int i = 0; i < length; i++)
            {
                for (int k = 0; k < length; k++)
                {
                    Console.Write(matrix[i, k] + " ");
                }
                Console.WriteLine();
            }
        }
        public static void SecondKindMatrix(int length)
        {
            int[,] matrix = new int[length, length];
            int next = 1;
            int line = 0;
            int countLines = 1;
            int countRows = 0;
            for (int count=0; count < length;count++)
            {
                for (int k = 0; k < length; k++, next += 1)
                {
                    if (countLines % 2 == 0)
                    {
                        matrix[countRows, count] = next;
                        countRows--;
                        continue;
                    }
                    matrix[countRows, count] = next;
                    countRows++;
                }
                countLines++;
                if(countLines%2==0)
                {
                    countRows = length - 1;
                }
                else
                {
                     countRows = 0;
                }
            }
            for (int i = 0; i < length; i++)
            {
                for (int k = 0; k < length; k++)
                {
                    Console.Write(matrix[i, k] + " ");
                }
                Console.WriteLine();
            }
        }
        public static void ThirdKindMatrix(int length)
        {
            int[,] matrix = new int[length, length];
            int next = 1;
            int last = length * length;
            int plus1=1;
            int plus2=1;
            for (int i = length - 1, c = length - 1,m=0; i >= 0; i--,c--,m=0)
            {
                for (int k = 0,add=length-plus1; add < length && add >= 0; k++, next++, add++)
                {
                    matrix[c+m, m] = next;
                    if(c<length-1)
                    {
                        m++;
                    }
                }
                plus1++;
            }
            for (int i = 0, p = 0; i < length - 1; i++, p++)
            {
                for (int j = length-1,take=length-plus2; take < length && take >= 0; take++,last--, j--)
                {
                    matrix[p, j] = last;
                    if (p > 0)
                    {
                        p--;
                    }
                }
                p += i;
                plus2++;
            }
            for (int i = 0; i < length; i++)
            {
                for (int k = 0; k < length; k++)
                {
                    Console.Write(matrix[i, k] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
