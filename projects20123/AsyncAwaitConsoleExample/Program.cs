﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitConsoleExample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            Task<string> asyncMethodOneTask = AsynchronousMethodOne();
            Task asyncMethodTwoTask = AsynchronousMethodTwo();

            SynchronousWork();

            var result = await asyncMethodOneTask;
            Console.WriteLine(result);
            await asyncMethodTwoTask;
            //analog 
            //await Task.WhenAll(new List<Task>() { asyncMethodOneTask, asyncMethodTwoTask });

            Console.WriteLine("Miliseconds elapsed: {stopwatch.ElapsedMilliseconds}");
            Console.WriteLine();

            stopwatch = new Stopwatch();
            stopwatch.Start();

            SynchronousMethodOne();
            SynchronousMethodTwo();

            SynchronousWork();

            Console.WriteLine(@"Miliseconds elapsed: {stopwatch.ElapsedMilliseconds}");

            Console.ReadKey();
        }
        private static void SynchronousWork()
        {
            //mimics 1 seconds of work
            Console.WriteLine("Start synchronous work");
            Thread.Sleep(1000);
            Console.WriteLine("Finish synchronous work");
        }


        private static async Task<string> AsynchronousMethodOne()
        {
            //mimics 2 seconds work
            Console.WriteLine("AsynchronousMethodOne starts execution");

            await Task.Delay(2000);
            Console.WriteLine("AsynchronousMethodOne finished execution");

            return await Task.FromResult("sdsds");
        }

        private static void SynchronousMethodOne()
        {
            //mimics 2 seconds work
            Console.WriteLine("SynchronousMethodOne starts execution");
            Thread.Sleep(2000);
            Console.WriteLine("SynchronousMethodOne finished execution");
        }

        private static async Task AsynchronousMethodTwo()
        {
            //mimics 3 seconds work
            Console.WriteLine("AsynchronousMethodTwo starts execution");
            await Task.Delay(3000);
            Console.WriteLine("AsynchronousMethodTwo finished execution");
        }

        private static void SynchronousMethodTwo()
        {
            //mimics 3 seconds work
            Console.WriteLine("SynchronousMethodTwo starts execution");
            Thread.Sleep(3000);
            Console.WriteLine("SynchronousMethodTwo finished execution");
        }
    }
}
