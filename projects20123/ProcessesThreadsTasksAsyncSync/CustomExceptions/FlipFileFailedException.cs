﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptions
{
    public class FlipFileFailedException : Exception
    {
        public FlipFileFailedException(string failedFilepath, Exception reason)
            : base("Failed to process" + failedFilepath  + "See InnerException - " + reason.InnerException)
        {
            this.FailedFilePath = failedFilepath;
        }

        public string FailedFilePath { get; set; }
    }
}
