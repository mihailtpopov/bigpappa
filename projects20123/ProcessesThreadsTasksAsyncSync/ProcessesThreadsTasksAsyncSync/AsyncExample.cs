﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;

namespace ProcessesThreadsTasksAsyncSync
{
    class AsyncExample
    {
        public async static void ExecuteSync()
        {
            var watch = Stopwatch.StartNew();
            await RunDownloadASync();
            watch.Stop();
                Console.WriteLine("Execution time in milliseconds " + watch.ElapsedMilliseconds);
        }
        private List<string> PrepData()
        {
            List<string> output = new List<string>();

            output.Add("https://www.yahoo.com");
            output.Add("https://www.google.com");
            output.Add("https://www.microsoft.com");
            output.Add("https://www.cnn.com");
            output.Add("https://www.codeproject.com");
            output.Add("https://www.stackoverflow.com");

            return output;

        }
        private async Task<string> RunDownloadASync()
        {
            List<string> websites = PrepData();
            foreach (string site in websites)
            {
                WebsiteDataModel result = await Task.Run(()=>DownloadWebsite(site));
                ReportWebsiteInfo(result);
            }
            return "yahhoo";
        }
        private WebsiteDataModel DownloadWebsite(string websiteUrl)
        {
            WebsiteDataModel output = new WebsiteDataModel();
            WebClient client = new WebClient();
            output.WebsiteUrl = websiteUrl;
            output.WebsiteDataLength = client.DownloadString(websiteUrl);

            return output;
        }
        private void ReportWebsiteInfo(WebsiteDataModel site)
        {
            Console.WriteLine("website {0} downloaded: {1} characters long",site.WebsiteUrl,site.WebsiteDataLength);
        }
    }
    public class WebsiteDataModel
    {
        string websiteUrl;
        string websiteDataLength;
        public string WebsiteUrl { get { return websiteUrl; } set { this.websiteUrl = value; } }
        public string WebsiteDataLength
        { 
            get{return websiteDataLength;} 
            set{this.websiteDataLength=value;}
        }
        public WebsiteDataModel()
        {
            this.websiteUrl = "";
            this.websiteDataLength = "";
        }
    }
}
