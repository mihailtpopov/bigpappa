﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE = CustomExceptions;
using System.Collections.Concurrent;
namespace ProcessesThreadsTasksAsyncSync
{
    class HandlingExceptionsInForeachFlipingImages
    {
        private static void FlipImagesWithOptionalRetry(List<string> imagesFilepaths, string targetedDirectory)
        {
           while(imagesFilepaths.Count>0)
            {
                try
                {
                    FlipImages(imagesFilepaths, targetedDirectory);
                    imagesFilepaths.Clear();
                }
                catch (AggregateException еxp)
                {
                    imagesFilepaths.Clear();
                    List<string> failedFilepaths = new List<string>();
                    foreach (var exception in exp.InnerException)
                    {
                       CE.FlipFileFailedException flipFailed = exception as CE.FlipFileFailedException;
                        if(flipFailed!=null)
                        {
                            failedFilepaths.Add(flipFailed.FailedFilePath);
                        }
                    }
                }
                Console.WriteLine("The following files could not be flipped due to errors:");
                Console.WriteLine(String.Join(System.Environment.NewLine,failedFilePaths);
                Console.WriteLine("Would you like to reprocess them? (y/n)");
                if(Console.ReadLine()=="y")
                {
                    imagesFilepaths.AddRange(failedFilePaths);
                }
            }
        }

        public static void FlipImages(List<string> filePathsToFlip, string targetedDirectoryPath)
        {
            ConcurrentQueue<CE.FlipFileFailedException> exceptions = new ConcurrentQueue<CE.FlipFileFailedException>();
            Parallel.ForEach(filePathsToFlip,
                (filePath) =>
                {
                    try
                    {
                        //FlipImage(filePath, targetedDirectoryPath);
                    }
                    catch (Exception e)
                    {
                        exceptions.Enqueue(new CE.FlipFileFailedException(filePath, e));
                    }
                }
                );
            if(!exceptions.IsEmpty)
            {
                //and in upper level we will catch that exception in method FlipImagesWithOptionalRetry
                //and working on it
                throw new AggregateException("One or more files failed to flip", exceptions);
            }
        }
    }
}
