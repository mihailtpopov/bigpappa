﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessesThreadsTasksAsyncSync
{
    class ThreadExample
    {
        public static void SlowMethod()
        {
            string str = "kurec";
            for(int i = 0;i<20;i++)
            {
                str += str;
            }
            Console.WriteLine(str);
        }
        public static void FastMethod()
        {
            string str = "dva kureca";
            for(int i=0;i<20;i++)
            {
                str += str;
            }
            Console.WriteLine(str);
        }
    }
}
