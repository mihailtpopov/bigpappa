﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ProcessesThreadsTasksAsyncSync
{
    class ProcessClass
    {
        public static void SearchGoogle(string searchTitle)
        {
            Process.Start("http://google.com/search?q=" + searchTitle);
        }
    }
}
