﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.IO;
namespace ProcessesThreadsTasksAsyncSync
{
    class Program
    {
        static void Main(string[] args)
        {
          //  ProcessClass.SearchGoogle("http://www.c-sharpcorner.com/");
            //Stopwatch watch = new Stopwatch();
            //watch.Start();
           // Console.WriteLine(watch.ToString());
            //Thread backgroundThread = new Thread(new ThreadStart(ThreadExample.SlowMethod));
            
           // backgroundThread.Start();

           // AsyncExample.ExecuteSync();

            //FlippingImages.cs
            Console.WriteLine("Enter directory with images to flip: ");
            var directoryName = Console.ReadLine();
            var imagesFilepaths = Directory.GetFiles(directoryName).ToList();
            Console.WriteLine("Found the following files: ");
            Console.WriteLine(String.Join(System.Environment.NewLine,imagesFilepaths));
            Console.WriteLine("Would you like to flip them? (y/n)");
            if(Console.ReadLine().ToLower() == "y")
            {

            }

        }
    }
}
