﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryToHexAndDec
{
    class Program
    {
        static void Main(string[] args)
        {
           string binary = "1111010110011110";
           Console.WriteLine("{0:X2}",Convert.ToInt32(binary,2));
           int num = Convert.ToInt32(binary, 2);
           Console.WriteLine(num);
         
        }
    }
}
