﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animal;
namespace animal
{
   internal class Dog
    {
        string name;
        string kind;
        internal string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        internal string Kind
        {
            get { return kind; }
            set { this.kind = value; }
        } 
    }
}
