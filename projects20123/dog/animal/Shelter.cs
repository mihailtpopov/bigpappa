﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Shelter
    {
        internal string name;
        internal string[] animals;
        public string Name
        {
            get { return name;}
            set { this.name = value; }
        }
        public string[] Dogs
        {
            get { return animals; }
            set { this.animals = value; }
        }
        private Shelter(string[] animals)
        {
            this.animals = animals;
        }
    }
}
