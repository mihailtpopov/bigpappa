﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_CorrectBrackets
{
    class Program
    {
        static bool CorrectBrackets(string expression)
        {
            if(NumberOfBothKindBrackets(expression))
            {
               
            }

        }
        static bool NumberOfBothKindBrackets(string expression)
        {
            int bracketIndex = expression.IndexOf("(");
            int bracketCounter = 0;
            int opositeBracketIndex = expression.IndexOf(")");
            int opositeBracketCounter = 0;
            while (bracketIndex != -1)
            {
                bracketCounter++;
                bracketIndex = expression.IndexOf("(", bracketIndex + 1);
            }
            while (opositeBracketIndex != -1)
            {
                opositeBracketCounter++;
                opositeBracketIndex = expression.IndexOf(")", opositeBracketIndex + 1);
            }
            if(bracketCounter==opositeBracketCounter)
            {
                return true;
            }
            return false;
        }
        static void Main(string[] args)
        {
        }
    }
}
