﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractSentences
{
    class Program
    {
        static List<string> GetAllSentences(string text)
        {
            List<string> allSentences = new List<string>();
            allSentences = text.Split('.').ToList();
            return allSentences;
        }
        static List<string> GetExtractedSentences(string value, List<string> allSentences)
        {
            List<string> extractedSentences = new List<string>();
            List<string> sentencesWords = new List<string>();
            for(int i = 0;i<allSentences.Count;i++)
            {
               sentencesWords = allSentences[i].Split(' ').ToList();
               for(int index = 0;index<sentencesWords.Count;index++)
               {
                   if(sentencesWords[index].ToLower()==value)
                   {
                       extractedSentences.Add(allSentences[i]+".");
                   }
               }
                sentencesWords.Clear();
            }
           return extractedSentences;
        }
        static void PrintExtractedSentences(List<string> extractedSentences)
        {
            foreach(string sentence in extractedSentences)
            {
                Console.WriteLine(sentence);
            }
        }
        static void Main(string[] args)
        {
            string text = "We are living in a yellow submarine. We don't have anything else." +
            "Inside the submarine is very tight. So we are drinking all the day. We will move out of it in 5 days."; 
            PrintExtractedSentences(GetExtractedSentences("in", GetAllSentences(text)));
        }
    }
}
