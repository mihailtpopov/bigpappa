﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FillLeftWithStars
{
    class Program
    {
        static StringBuilder GetResult(string text)
        {
            if(text.Length>20)
            {
                throw new ArgumentException("This task requires text with length 20 or less characters");
            }
            StringBuilder modifiedText = new StringBuilder(20);
            modifiedText.Append(text);
            for(int i = modifiedText.Length;i<modifiedText.Capacity;i++)
            {
                modifiedText.Append("*");
            }
            return modifiedText;
        }
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            StringBuilder modifiedWithStars = GetResult(text);
            Console.WriteLine(modifiedWithStars.ToString());
        }
    }
}
