﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractPalindromes
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "My name is ABBA. I like lamal. This fail is exe.";
            List<string> palindromesList = OperateText.ExtractPalindromes(text);
            OperateText.PrintPalindromes(palindromesList);
        }
    }
}
