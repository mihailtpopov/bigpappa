﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractPalindromes
{
    class OperateText
    {
       public static List<string> SplitTextWords(string text)
        {
            char[] separators = new char[] { ' ', ',', '.' };
            List<string> separatedWords = text.Split(separators, StringSplitOptions.RemoveEmptyEntries).ToList();
            return separatedWords;
        }

      public static List<string> ExtractPalindromes(string text)
       {
           StringBuilder reversedText = new StringBuilder();
           List<string> separatedWords = SplitTextWords(text);
           List<string> palindromes = new List<string>();
           for (int nextWord = 0; nextWord < separatedWords.Count; nextWord++)
           {
               if (separatedWords[nextWord].Length == 1)
                   continue;
               reversedText.Append(separatedWords[nextWord].Reverse());
               if (separatedWords[nextWord] == reversedText.ToString())
               {
                   palindromes.Add(separatedWords[nextWord]);
                   reversedText.Clear();
               }
               reversedText.Clear();
           }
           return palindromes;
       }

      public static void PrintPalindromes(List<string> palindromesList)
       {
           foreach (string palindrome in palindromesList)
           {
               Console.WriteLine(palindrome);
           }
       }

    }
}
