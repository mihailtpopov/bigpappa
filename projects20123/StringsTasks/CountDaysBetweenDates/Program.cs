﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
namespace CountDaysBetweenDates
{
    class Program
    {
        static double GetDaysBetweenDates(DateTime startDate, DateTime endDate)
        {
            if(endDate.Date<startDate.Date)
            {
                throw new ArgumentException("The end date must be higher than the start one!");
            }
           double days = (endDate - startDate).TotalDays;
           return days;
        }
        static void Main(string[] args)
        {
            string format = "dd.MM.yyyy";
            Console.Write("Enter starting date: ");
            DateTime startingDate = DateTime.ParseExact(Console.ReadLine(), format, CultureInfo.InvariantCulture);
            Console.Write("Enter ending date: ");
            DateTime endingDate = DateTime.ParseExact(Console.ReadLine(), format, CultureInfo.InvariantCulture);
            double daysInBetween = GetDaysBetweenDates(startingDate, endingDate);
            Console.WriteLine("The days between the dates are " + daysInBetween);
        }
    }
}
