﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReversedText
{
    class Program
    {
        static StringBuilder ReverseText(string text)
        {
            StringBuilder reversedText = new StringBuilder();
            for(int i = text.Length-1;i>=0;i--)
            {
                reversedText.Append(text[i]);
            }
            return reversedText;
        }
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            StringBuilder reversedText = ReverseText(text);
            Console.WriteLine(reversedText);
        }
    }
}
