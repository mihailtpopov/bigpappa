﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractProtocolServerAndResource
{
    class Program
    {
        static string ExtractProtocol(string link)
        {
            int doubleDotsIndex = link.IndexOf(':');
            string protocol = link.Substring(0, doubleDotsIndex);
            return protocol;
        }
        static string ExtractServer(string link)
        {
            string doubleSlash = "//";
            string singleSlash = "/";
            int doubleSlashIndex = link.IndexOf(doubleSlash);
            int singleSlashIndex = link.IndexOf(singleSlash,doubleSlashIndex+doubleSlash.Length);
            int serverLength = singleSlashIndex - (doubleSlashIndex+doubleSlash.Length);

            string server = link.Substring(doubleSlashIndex + doubleSlash.Length , serverLength);
            return server;
        }
        static string ExtractResource(string link)
        {
            string doubleSlash = "//";
            string singleSlash = "/";
            int doubleSlashIndex = link.IndexOf(doubleSlash);
            int firstSingleSlashIndex = link.IndexOf(singleSlash, doubleSlashIndex + doubleSlash.Length);
            int nextSingleSlashIndex = link.IndexOf(singleSlash, firstSingleSlashIndex + 1);
            if (nextSingleSlashIndex != -1)
            {
                string innerResource = link.Substring(firstSingleSlashIndex);
                return innerResource;
            }
            string resource = link.Substring(firstSingleSlashIndex + singleSlash.Length);
            return resource;
        }
        static void Main(string[] args)
        {
            string link = "http://www.devbg.org/forum/index.php";
            string protocol = ExtractProtocol(link);
            string server = ExtractServer(link);
            string resource = ExtractResource(link);
            Console.WriteLine(protocol);
            Console.WriteLine(server);
            Console.WriteLine(resource);
        }
    }
}
