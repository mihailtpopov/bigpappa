﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberIn15SymbolField
{
    class Program
    {
        static void Main(string[] args)
        {
            string strNum = Console.ReadLine();
            strNum = strNum.PadLeft(15);
            Console.WriteLine(strNum);
            int intNum = Convert.ToInt32(String.Format("{0:X}",strNum));
            Console.WriteLine(strNum);
        }
    }
}
