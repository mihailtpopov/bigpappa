﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWordMeaning
{
    class Program
    {
        static string[] SplitDictionaryBySentences(string text)
        {
            string[] sentences = text.Split('\n');
            return sentences;
        }
        static string[] SplitDictionaryByKeyWords(string text)
        {
            string symbol = " -";
            string[] sentences = SplitDictionaryBySentences(text);
            string[] keywords = new string[sentences.Length];
            for (int i = 0; i < sentences.Length;i++)
            {
                int symbolIndex = sentences[i].IndexOf(symbol);
                keywords[i] = sentences[i].Substring(0, symbolIndex);
            }
            return keywords;
        }
        static string[] SplitDictionaryByKeyWordMeaning(string text)
        {
            string symbol = "- ";
            string[] sentences = SplitDictionaryBySentences(text);
            string[] keywordMeaning = new string[sentences.Length];
            for(int i = 0;i<sentences.Length;i++)
            {
                int symbolIndex = sentences[i].IndexOf(symbol);
                keywordMeaning[i] = sentences[i].Substring(symbolIndex + symbol.Length);
            }
        }
        static void PrintMeaning(IDictionary<string,string> dictionary, string keyword)
        {
           foreach(KeyValuePair<string,string> word in dictionary)
           {
               if(dictionary.Keys==keyword)
               {

               }
           }
        }
        static void Main(string[] args)
        {
            string text = ".NET – platform for applications from Microsoft\n" +
            "CLR – managed execution environment for .NET\n namespace – hierarchical organization of classes ";
            string[] sentences = SplitDictionaryBySentences(text);
            string[] keywords = SplitDictionaryByKeyWords(text);
            string[] keywordsMeaning = SplitDictionaryByKeyWordMeaning(text);

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            for(int i = 0;i<keywords.Length;i++)
            {
                dictionary[keywords[i]] = keywordsMeaning[i];
            }
        }
    }
}
