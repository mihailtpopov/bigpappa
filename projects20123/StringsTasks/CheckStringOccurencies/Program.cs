﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckStringOccurencies
{
    class Program
    {
        static int GetValueOccurencies(string text, string value)
        {
            text = text.ToLower();
            int index = text.IndexOf(value);
            int valueOccurencies = 0;
            while (index != -1)
            {
                valueOccurencies++;
                index = text.IndexOf(value, index + value.Length);
            }
            return valueOccurencies;
        }
        static void Main(string[] args)
        {
            string text = "We are living in a yellow submarine. We don't have anything else." +
            "Inside the submarine is very tight. So we are drinking all the day. We will move out of it in 5 days. ";
            string valueToBeFound = Console.ReadLine();
            int valueOccurencies = GetValueOccurencies(text, valueToBeFound);
            Console.WriteLine(valueOccurencies);
        }
    }
}
