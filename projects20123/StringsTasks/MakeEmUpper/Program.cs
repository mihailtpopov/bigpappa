﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeEmUpper
{
    class Program
    {
        static StringBuilder ModifyToUpper(string text, string startUp = "<upcase>", string endUp = "</upcase>")
        {
            int firstUpIndex = text.IndexOf(startUp);
            int firstUpLength = startUp.Length;
            int secondUpIndex = text.IndexOf(endUp);
            int secondUpLength = endUp.Length;
            StringBuilder modifiedText = new StringBuilder();
            modifiedText.Append(text);
            while(firstUpIndex!=-1 && secondUpIndex!=-1)
            {
                try
                {
                    int lastFirstUpIndex = firstUpIndex + firstUpLength;
                    int modifyValueLength = secondUpIndex - lastFirstUpIndex;
                    string modifyValue = modifiedText.ToString().Substring(lastFirstUpIndex, modifyValueLength);
                    modifiedText = modifiedText.Replace(modifyValue, modifyValue.ToUpper());

                    TrimUpcases(modifiedText, firstUpIndex, secondUpIndex);

                    firstUpIndex = modifiedText.ToString().IndexOf(startUp);
                    secondUpIndex = modifiedText.ToString().IndexOf(endUp);
                }
                catch(IndexOutOfRangeException)
                {
                    throw new ArgumentException("Some of the values went out of range");
                }
            }
            return modifiedText;
        }
        static void TrimUpcases(StringBuilder text, int startFirstUpIndex, int startSecondUpIndex)
        {
            string keyword = "<upcase>";
            string keyword2 = "</upcase>";
            string strText = text.ToString();
            try
            {
                text.Remove(startFirstUpIndex, keyword.Length);
                text.Remove(startSecondUpIndex-keyword.Length, keyword2.Length);
            }
            catch(IndexOutOfRangeException)
            {
                throw new ArgumentException("Some of the values went out of range!");
            }
        }
        static void Main(string[] args)
        {
            string text = "We are living in a <upcase>yellow submarine</upcase>." +
            "We don't have <upcase>anything</upcase> else.";
            StringBuilder modifiedText = ModifyToUpper(text);
            Console.WriteLine(modifiedText.ToString());
        }
    }
}
