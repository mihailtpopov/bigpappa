﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_PrintMinAndMax
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the length of the number sequence: ");
            int lengthSeq = int.Parse(Console.ReadLine());
            int min = int.MaxValue;
            int max = int.MinValue;
            for(int i = 0; i<lengthSeq;i++)
            {
                int num = int.Parse(Console.ReadLine());
                if(num<min)
                {
                    min = num;
                }
                else if(num>max)
                {
                    max = num;
                }
            }
            Console.WriteLine("Min: " + min + ", Max: " + max);
        }
    }
}
