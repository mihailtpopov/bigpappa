﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintAllCards
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> color = new List<string>();
            color.Add("clubs");
            color.Add("diamonds");
            color.Add("hearts");
            color.Add("spades");
            List<string> card = new List<string>();
            card.Add("Jack");
            card.Add("Queen");
            card.Add("King");
            card.Add("Ace");
            for(int cName = 0;cName<4;cName++)
            {
                for(int cardNum = 2;cardNum<=10;cardNum++)
                {
                    Console.WriteLine(cardNum + " of {0}",color[cName]);
                }
                for(int name = 0;name<4;name++)
                {
                    Console.WriteLine(card[name] + " of {0}",color[cName]);
                }
                Console.WriteLine();
            }
        }
    }
}
