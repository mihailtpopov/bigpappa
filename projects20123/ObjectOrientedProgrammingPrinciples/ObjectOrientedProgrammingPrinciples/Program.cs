﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgrammingPrinciples
{
    class Program
    {
        static void Main(string[] args)
        {
            AfricanLion lion = new AfricanLion(true, 80);
            object obj = lion;
            try
            {
                AfricanLion newLion = (AfricanLion)obj;
            }
            catch (InvalidCastException ice)
            {
                Console.WriteLine("obj cannot be downcasted to AfricanLion");
            }

            Console.WriteLine(new AfricanLion(true, 120));
            Console.WriteLine(new Lion(true, 90));
            {
                Lion lion2 = new Lion(true, 20);
                lion2.PrintCatchPray(null);
            }
            {
                AfricanLion africanLion = new AfricanLion(false, 30);
                africanLion.PrintCatchPray(null);
            }
            {
                Lion newAfricanLion = new AfricanLion(true, 50);
                newAfricanLion.PrintCatchPray(null);
            }
        }
    }
}
