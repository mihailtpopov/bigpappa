﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgrammingPrinciples
{
    class InheritanceFelidae
    {
        private bool male;

        public InheritanceFelidae()
            : this(true)
        { }

        public InheritanceFelidae(bool male)
        {
            this.male = male;
        }

        public bool Male
        {
            get { return this.male; }
            set { this.male = value; }
        }

        protected void Hide()
        {
            //some implementation
        }
        protected void Run()
        {
            //some implementation
        }

        public abstract bool CatchPray(object pray);
        public abstract void PrintCatchPray(object pray);
    }
}
