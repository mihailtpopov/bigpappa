﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedProgrammingPrinciples
{
    class Lion : InheritanceFelidae
    {
        private int weight;
        public Lion(bool male, int weight)
            : base(male)
        {
            //Compier error, cause male field is private
            //base.male = male;
            this.weight = weight;
        }
        public int Weight
        {
            get { return weight; }
            set { this.weight = value; }
        }
        protected void Ambush()
        {
            //some implementation
        }
        public override bool CatchPray(object pray)
        {
            base.Hide();
            this.Ambush();
            base.Run();
            return false;
        }

        public override void PrintCatchPray(object pray)
        {
            Console.WriteLine("Lion.CatchPray");
        }

    }
}
