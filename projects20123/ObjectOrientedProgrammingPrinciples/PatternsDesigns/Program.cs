﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternsDesigns.FactoryMethod;
using PatternsDesigns.Singleton;
namespace PatternsDesigns
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<Image> images = new List<Image>();
            images.Add(new JpegImage());
            images.Add(new GifImage());
            ImageCollection imageRepository = new ImageCollection(images);
            List<Thumbnail> thumbnails = imageRepository.CreateThumbnails();
            foreach (Thumbnail th in thumbnails)
            {
                Console.WriteLine(th.GetType().Name);
            }

            Console.WriteLine(Singleton.Singleton.Instance);
        }
    }
}
