﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsDesigns.Singleton
{
    class Singleton
    {
        private static Singleton instance;
        static Singleton()
        {
            instance = new Singleton();
        }
        public static Singleton Instance
        {
            get { return instance; }
        }
        private Singleton() { }
    }
}
