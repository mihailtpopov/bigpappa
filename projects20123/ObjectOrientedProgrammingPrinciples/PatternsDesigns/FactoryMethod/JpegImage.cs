﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsDesigns.FactoryMethod
{
    class JpegImage : Image
    {
        public Thumbnail CreateThumbnail()
        {
            Thumbnail jpegThumbnail = new Thumbnail();
            return jpegThumbnail;
        }
    }
}
