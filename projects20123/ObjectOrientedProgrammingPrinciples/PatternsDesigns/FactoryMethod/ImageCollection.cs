﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsDesigns.FactoryMethod
{
    class ImageCollection
    {
        private IList<Image> images;
        public ImageCollection(IList<Image> images)
        {
            this.images = images;
        }
        public List<Thumbnail> CreateThumbnails()
        {
            List<Thumbnail> thumbnails = new List<Thumbnail>(images.Count);
            foreach(Image th in images)
            {
                thumbnails.Add(th.CreateThumbnail());
            }
            return thumbnails;
        }
    }
}
