﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsDesigns.FactoryMethod
{
    class GifImage : Image
    {

        public Thumbnail CreateThumbnail()
        {
            Thumbnail gifThumbnail = new Thumbnail();
            //create thumbnail..
            return gifThumbnail; 
        }
    }
}
