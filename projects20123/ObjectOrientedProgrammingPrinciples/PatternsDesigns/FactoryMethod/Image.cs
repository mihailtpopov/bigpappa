﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatternsDesigns.FactoryMethod
{
    public interface Image
    {
       Thumbnail CreateThumbnail();
    }
}
