﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadAgeAfter10Years
{
    class Program
    {
        static void Main(string[] args)
        {
             DateTime currentDate = DateTime.Now;
             DateTime myAge = new DateTime(Int32.Parse(Console.ReadLine()),2,21);
             DateTime ageAfter10Years =  currentDate.AddYears(10);
             Console.WriteLine("I am born {0}.{1}.{2}",myAge.Day,myAge.Month,myAge.Year);
             Console.WriteLine("My age after 10 years will be: " + (ageAfter10Years.Year-myAge.Year));
            

        }
    }
}
