﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinusPlusSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int number = 2;
            while(i<100)
            {
                if(number%2==0)
                {
                    Console.WriteLine(number);
                }
                else
                {
                    Console.WriteLine("-" + number);
                }
                i++;
                number++;
            }
        }
    }
}
