﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAndDotNetDifferences
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("First of all C# is simple to understand object-oriented programming language, while .NET Framework is." +
                " the platfrom on which we use C#.\n.NET is a framework in which several similar programming languages" +
                "are combined and can be used on any hardware supporting the .NET platform.\nIn general .NET is a totality" +
                "of different libraries and  their functionalities, which can be used by few programming languagues, while" +
                "C# is just one of those languages");
        }
    }
}
