﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figure
{
    abstract class Shape
    {
        private double width;
        private double height;

        public double Width
        {
            get { return width; }
            set { this.width = value; }
        }
        public double Height
        {
            get { return height; }
            set { this.height = value; }
        }
        public virtual double calculateSurface()
        {
            return Width * Height;
        }
    }
}
