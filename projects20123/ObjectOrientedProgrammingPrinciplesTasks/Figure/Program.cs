﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figure
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shape> shapes = new List<Shape>();
            shapes.Add(new Circle(5));
            shapes.Add(new Triangle(6, 7));
            shapes.Add(new Rectangle(2, 9));
            List<double> shapesSurfaces = new List<double>();
            foreach(Shape shape in shapes)
            {
                double surface = shape.calculateSurface();
                shapesSurfaces.Add(surface);
            }
            shapesSurfaces.ForEach(Console.WriteLine);
        }
    }
}
