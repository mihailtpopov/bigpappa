﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figure
{
    class Triangle : Shape
    {
        private double width;
        private double height;

        public Triangle(double width, double height)
        {
            this.width = width;
            this.height = height;
        }

        public override double calculateSurface()
        {
            return (width * height) / 2;
           
        }
    }
}
