﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figure
{
    class Circle : Shape
    {
        private double radius;
        public Circle(double radius)
        {
            this.radius = radius;
            Height = Width;
        }
        public override double calculateSurface()
        {
            return 3.14 * Math.Pow(radius,2);
        }
    }
}
