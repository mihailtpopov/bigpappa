﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortByAscendingWithIComparable
{
    class Program
    {
        static void Main(string[] args)
        {
           Student student1 = new Student(4,"Ibrahim");
           Student student2 = new Student(4,"Akakii");
           Student student3 = new Student(2,"Avrelii");
           Student student4 = new Student(3,"Shibil");
           Student student5 = new Student(5.5,"Onq");
           Student student6 = new Student(6,"Kerim");
           Student student7 = new Student(3.5,"Penio");
           Student student8 = new Student(2.5,"Racho");
           Student student9 = new Student(4,"Maikal");
           Student student10 = new Student(4.5,"Nasakoto Yakata");

            Student.Students.Sort();//task 2

            Student.Students.Reverse(); //task 3 
           foreach(Student student in Student.Students)
           {
               Console.WriteLine(student.Grade);
           }
        }
    }
}
