﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortByAscendingWithIComparable
{
    class Student : IComparable<Student>
    {
        private double grade;
        private string name;
        private static List<Student> students = new List<Student>(10);
        public double Grade
        {
            get { return grade; }
            set { this.grade = value; }
        }
        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }
        public static List<Student> Students
        {
            get { return students; }
            set { students = value; }
        }
        public Student(double grade, string name)
        {
            this.grade = grade;
            this.name = name;
            students.Add(this);
        }
        public int CompareTo(Student other)
        {
           return this.Grade.CompareTo(other.Grade);
        }
        
    }
}
