﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School
{
    class Class
    {
        private string classIdentificator;
        private List<Student> students;
        private Teacher classTeacher;

        public Class(string classIdentificator, Teacher classTeacher, List<Student> students)
        {
            this.classIdentificator = classIdentificator;
            this.classTeacher = classTeacher;
            this.students = students;
        }
        public string ClassIdentificator
        {
            get { return this.classIdentificator; }
            set{ this.classIdentificator=value; }
        }
        public Teacher ClassTeacher
        {
            get { return this.classTeacher; }
            set { this.classTeacher = value; }
        }
        
        public void AddStudent(Student student)
        {
            this.students.Add(student);
        }
        public void RemoveStudent(Student student)
        {
            this.students.Remove(student);
        }
        public void AddClassTeacher(Teacher teacher)
        {
            classTeacher = teacher;
        }
        public void ChangeClassTeacher(Teacher teacher)
        {
            classTeacher = teacher;
        }


    }
}
