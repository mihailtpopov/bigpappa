﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class CreditAcc : Account
    {
        //ALL PERIODS ARE IN MONTHS
        //the period in which the client should return the loan
        private int repayPeriod;
        //the period in which the client will not pay interest
        private int noInterestPeriod;
        //
        private double monthlyInstallmentRelievedPeriod;
        //
        private double monthlyInstallmentRemainingPeriod;
        //
        private double principal;
        //
         private double creditAmountWithdrawn;
        //
        private double creditAmountToBeReturned;
        //
        public override void DepositMoney(double depositPrice,DateTime openingAccDate, DateTime depositDate)
        {
            if(depositDate.Date>openingAccDate.AddMonths(repayPeriod))
            {
                throw new ArgumentException("The depositing period already expired! You have to pay 'neustoika'");
            }
            if (openingAccDate.Month + noInterestPeriod < depositDate.Month)
                base.DepositMoney(depositPrice,depositDate);
            else
                base.DepositMoney(depositPrice * MonthlyInterestRate,depositDate);
        }
        public CreditAcc(Client client, double creditAmountWithdrawn, double monthlyInterestRate, int repayPeriod) 
            : base(client,creditAmountWithdrawn,monthlyInterestRate)
        {
           this.repayPeriod=repayPeriod;
           this.creditAmountWithdrawn=creditAmountWithdrawn;
           this.creditAmountToBeReturned = this.creditAmountWithdrawn* base.InterestForGivenPeriod(repayPeriod);
           this.principal = this.creditAmountWithdrawn / this.repayPeriod;

           if (client is Individual)
               this.noInterestPeriod = 3;
           else
               this.noInterestPeriod = 2;

           this.monthlyInstallmentRelievedPeriod = this.principal;
           this.monthlyInstallmentRemainingPeriod =
              (creditAmountToBeReturned - monthlyInstallmentRelievedPeriod * noInterestPeriod) / (repayPeriod - noInterestPeriod);
           IsActive = true;
        }


    }
}

