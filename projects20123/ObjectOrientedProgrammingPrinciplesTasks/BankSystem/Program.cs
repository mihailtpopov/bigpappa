﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Individual PetarKondev = new Individual("Petar Kondev", "BG912039891301");
            PetarKondev.OpenAccount(new DepositAcc(PetarKondev, 1001, 2,6));
            Console.WriteLine(PetarKondev.CheckIfActive(PetarKondev.DepositAccount));
            Console.WriteLine(PetarKondev.CheckIfActive(PetarKondev.CreditAccount));
            PetarKondev.DepositAccount.DepositMoney(1500,new DateTime(2020,1,5,12,54,23));
            //зашо не хвърля изключение
            PetarKondev.DepositAccount.Withdraw(1000, new DateTime(2019, 12, 15, 12, 43, 22));
        }
    }
}
