﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class MortgageAcc : Account, LoanAccounts
    {
        //ALL PERIODS ARE IN MONTHS;

        //въпрос- да декларирам ли полетата като константи и ако да, кои от тях?
        //relieved period for different account types(individual or company)
        private int relievedInterestPeriod = 0;
        //number of months in which the loan must be paid off
        private int repayPeriod = 0;
        //the amount of money of the loan
        private double mortgLoanAmount = 0.0d;
        //the amount of money that must be returned with the interest
        private double mortgLoanSizeToBeReturned = 0.0d;
        //monthly installment for relieved period
        private double monthlyInstallmentRelievedPeriod = 0.0d;
        //monthly installment which is obtained after the relieved period
        private double monthlyInstallmentRemainingPeriod = 0.0d;
        //monthly installment without interest 
        private double principal = 0.0d;
        //
        private bool isLoanPaidOff = false;
        public MortgageAcc(Client client, double mortgLoanAmount, double monthlyInterestRate, int repayPeriod)
            : base(client, mortgLoanAmount, monthlyInterestRate)
        {
            this.repayPeriod = repayPeriod;
            this.mortgLoanAmount = mortgLoanAmount;
            this.mortgLoanSizeToBeReturned = mortgLoanAmount + (mortgLoanAmount * base.InterestForGivenPeriod(repayPeriod));
            principal = mortgLoanAmount / repayPeriod;
            //necessary to calculate relieved period installment and remaining period (after relieved period) monthly installment 
            double interestAmountPerMonth = (this.mortgLoanSizeToBeReturned - this.mortgLoanAmount)/repayPeriod;
            //same
            double normalMonthlyInstallment = principal + interestAmountPerMonth;

            if (client is Individual)
            {
                relievedInterestPeriod = 6;
                monthlyInstallmentRelievedPeriod;
                monthlyInstallmentRemainingPeriod = 
                    (mortgLoanSizeToBeReturned-(relievedInterestPeriod*principal))/repayPeriod-relievedInterestPeriod;
            }
            else
            {
                relievedInterestPeriod = 12;
                monthlyInstallmentRelievedPeriod = normalMonthlyInstallment - interestAmountPerMonth/2;
                monthlyInstallmentRemainingPeriod = normalMonthlyInstallment + interestAmountPerMonth/2;
            }
            this.IsActive = true;
        }

        public override void DepositMoney(double depositPrice, DateTime openingAccDate, DateTime depositDate)
        {
            //the date for the last installment
            DateTime lastPaymentDate = openingAccDate.AddMonths(repayPeriod);

            if (depositDate.Date > lastPaymentDate.Date)
            {
                throw new ArgumentException("The depositing period already expired! You have to pay 'neustoika'");
            }
            if (openingAccDate.AddMonths(relievedInterestPeriod) < depositDate.Date)
            {
                if (relievedInterestPeriod == 12)
                    base.DepositMoney(depositPrice * MonthlyInterestRate / 2);
                else
                    base.DepositMoney(depositPrice);
            }
            else
                base.DepositMoney(depositPrice * MonthlyInterestRate);

            if (base.AccountBalance >= mortgLoanAmount && depositDate<lastPaymentDate)
            {
                isLoanPaidOff = true;
                throw new ArgumentException("The mortgage loan is paid off! You don't need to deposit more money!");
            }
        }

        public bool IsLoanPaidOff()
        {
            return isLoanPaidOff;
        }

        public double GetMortgLoanAmountForReturning()
        {
            double mortgLoanAmountForReturning = mortgLoanAmount + mortgLoanAmount * MonthlyInterestRate * repayPeriod;
            return mortgLoanAmountForReturning;
        }

        public double GetMonthlyInstallmentInRelievedPeriod()
        {
            double monthlyInstallmentInRelievedPeriod = 
        }

        public double GetMonthlyInstallmentInRemainingPeriod()
        {
            throw new NotImplementedException();
        }

        public double GetPrincipal()
        {
            double principal = mortgLoanAmount / repayPeriod;
            return principal;
        }
    }
}
