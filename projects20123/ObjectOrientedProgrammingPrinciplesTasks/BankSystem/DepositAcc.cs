﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class DepositAcc : Account
    {
        private DateTime depositDate;
        //shows on how many months you can make a withdraw, so you can receive the money with interest
        private uint withdrawingPeriodInMonths;
        //в това не съм сигурен! Замисъла е на период от равни интервали да можеш да теглиш парите, обложени на лихва. Ако изтеглиш
        //преди този период да не са обложени на лихва 
        private static DateTime previousWithdrawDate = default(DateTime);
        private static DateTime previousDepositDate = default(DateTime);
        public override void DepositMoney(double depositPrice, DateTime depositDate)
        {
            if(depositDate<previousDepositDate)
            {
                throw new ArgumentException("You can't travel in time!");
            }
            if(depositPrice<=0)
            {
                throw new ArgumentException("You can't deposit negative amount or 0");
            }
            base.DepositMoney(depositPrice, depositDate);

            if(AccountBalance < 1000 && AccountBalance > 0)
            {
                this.MonthlyInterestRate = 0;
            }
            previousDepositDate = depositDate;
            this.depositDate = depositDate;
        }
        public void Withdraw(double amount, DateTime withdrawDate)
        {
            if(withdrawDate<previousWithdrawDate)
            {
                throw new ArgumentException("You can't travel in time!");
            }
            TimeSpan interval = withdrawDate-previousWithdrawDate;
            if (AccountBalance > 1000 && interval.Days>=withdrawingPeriodInMonths*30)
            {
                if ((depositDate - withdrawDate).TotalDays > DateTime.DaysInMonth(depositDate.Year, depositDate.Month))
                {
                    int months = (int)((withdrawDate-depositDate).TotalDays)/ 30;
                    AccountBalance += AccountBalance*InterestForGivenPeriod(months);
                }
            }
            previousWithdrawDate = withdrawDate;
            this.AccountBalance -= amount;
        }
        public DepositAcc(Client client, double balance, double monthlyInterestRate,uint withdrawingPeriodInMonths) 
            : base(client, balance,monthlyInterestRate)
        {
            this.withdrawingPeriodInMonths = withdrawingPeriodInMonths;
            IsActive = true;
        }
    }
}
