﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class Individual : Client
    {
        public Individual(string name, string iban)
        {
            base.Name = name;
            base.IBAN = iban;
        }
    }
}
