﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    interface LoanAccounts
    {
        public double GetMortgLoanAmountForReturning();
        public double GetMonthlyInstallmentInRelievedPeriod();
        public double GetMonthlyInstallmentInRemainingPeriod();
        public double GetPrincipal();
    }
}
