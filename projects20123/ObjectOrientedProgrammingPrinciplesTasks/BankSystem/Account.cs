﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    abstract class Account
    {
        private Client client;
        private double monthlyInterestRate;
        private double accountBalance;
        private bool isActive=false;

        public Account(Client client, double balance, double monthlyInterest)
        {
            this.client = client;
            this.accountBalance = balance;
            this.monthlyInterestRate = monthlyInterest;
        }

        public double MonthlyInterestRate
        {
            get { return this.monthlyInterestRate; }
            set { this.monthlyInterestRate = value; }
        }
        public bool IsActive
        {
            get { return this.isActive; }
            set { this.isActive = value; }
        }
        public double AccountBalance
        {
            get { return accountBalance; }
            set { this.accountBalance = value; }
        }
        public double InterestForGivenPeriod(int months)
        {
            return months * (monthlyInterestRate/100);
        }

        public virtual void DepositMoney(double depositPrice)
        {
            accountBalance += depositPrice;
        }

        public virtual void DepositMoney(double depositPrice, DateTime depositDate)
        {
            accountBalance += depositPrice;
        }

        public virtual void DepositMoney(double depositPrice, DateTime openingDate, DateTime depositDate)
        {
            accountBalance += depositPrice;
        }
    }
}
