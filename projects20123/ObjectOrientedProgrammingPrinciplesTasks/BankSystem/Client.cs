﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    abstract class Client
    {
        private string name;
        private string Iban;
        private DepositAcc depositAcc;
        private MortgageAcc mortgageAcc;
        private CreditAcc creditAcc;

        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }

        public string IBAN
        {
            get { return Iban; }
            set { this.Iban = value; }
        }
        public DepositAcc DepositAccount
        {
            get { return this.depositAcc; }
            set { this.depositAcc = value; }
        }
        public MortgageAcc MortgageAccount
        {
            get { return this.mortgageAcc; }
            set { this.mortgageAcc = value; }
        }
        public CreditAcc CreditAccount
        {
            get { return this.creditAcc; }
            set { this.creditAcc = value; }
        }
        public string GetBIC()
        {
            return "DSKBIC";
        }
        public void OpenAccount(Account account)
        {
            if (account is DepositAcc)
            {
                this.depositAcc = (DepositAcc)account;
            }
            else if (account is CreditAcc)
            {
                this.creditAcc = (CreditAcc)account;
            }
            else
            {
                this.mortgageAcc = (MortgageAcc)account;
            }
        }
        public bool CheckIfActive(Account account)
        {
            bool isActive = true;
            if(account==null)
               isActive=false;
            return isActive;
        }
    }
}
