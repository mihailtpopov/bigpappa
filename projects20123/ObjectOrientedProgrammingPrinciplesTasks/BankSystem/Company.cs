﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class Company : Client
    {
        public Company(string name, string iban)
        {
            base.Name = name;
            base.IBAN = iban;
        }
    }
}
