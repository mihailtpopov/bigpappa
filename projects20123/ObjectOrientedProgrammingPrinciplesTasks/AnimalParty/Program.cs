﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalParty
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animalParty = new List<Animal>();
            Animal cat = new Cat("Chocho", 3, 'm');
            animalParty.Add(cat);
            Animal dog = new Dog("Asen", 5, 'm');
            animalParty.Add(dog);
            Animal kitten = new Kitten("Maca",1,'f');
            animalParty.Add(kitten);
            Animal frog = new Frog("Froggy", 2, 'm');
            animalParty.Add(frog);
            Animal tomcat = new Tomcat("Tom", 8, 'm');
            animalParty.Add(tomcat);

            foreach(Animal animal in animalParty)
            {
                animal.CommonFeatures();
            }
        }
    }
}
