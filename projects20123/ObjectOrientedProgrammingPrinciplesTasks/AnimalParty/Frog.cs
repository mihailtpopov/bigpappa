﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalParty
{
    class Frog : Animal
    {
        public Frog(string name, int age, char gender) : base(name,age,gender)
        {
           
        }

        public override void Sound()
        {
            Console.WriteLine("Kwa-kwa, Kwa-kwa");
        }
    }
}
