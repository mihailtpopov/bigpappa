﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalParty
{
   abstract class Animal
    {
       private string name;
       private int age;
       private char gender;
       private static List<Animal> animalParty;

       public string Name
       {
           get { return this.name; }
           set { this.name = value; }
       }
       public int Age
       {
           get { return this.age; }
           set { this.age = value; }
       }
       public char Gender
       {
           get { return this.gender; }
           set { this.gender = value; }
       }
       public static List<Animal> AnimalParty
       {
           get { return animalParty; }
           set { animalParty = value; }
       }
       public abstract void Sound();
       public void CommonFeatures()
       {
           Console.WriteLine("Animal: {3}\nName: {0}\nAge: {1}\nGender: {2}",this.Name,this.Age,this.Gender,this.GetType().Name);
           this.Sound();
           Console.WriteLine();
       }

       public Animal(string name, int age, char gender)
       {
           this.name = name;
           this.age = age;
           this.gender = gender;
       }
       
    }
}
