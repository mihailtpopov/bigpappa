﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanRoles
{
    class Worker : Human
    {
        private double wage;
        private double workedHours;

        public double Wage
        {
            get { return wage; }
            set { 
                if(wage<0)
                {
                    throw new ArgumentException("Wage can't be negative");
                }
                this.wage = value; 
            }
        }
        public double WorkedHours
        {
            get { return workedHours; }
            set { 
                    if(this.workedHours<0)
                  {
                      throw new ArgumentException("Worked hours can't be negative");
                  }
                this.workedHours = value; 
                }
        }
        public Worker(double wage, double workedHours)
        {
            this.wage = wage;
            this.workedHours = workedHours;
        }
        private double CalculateWageForOneHour(double wage,double workingHours)
        {
            return wage / workingHours;
        }
    }
}
