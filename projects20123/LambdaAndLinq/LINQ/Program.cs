﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Product
    {
        public string Name { get; set; }
        public int CategoryID { get; set; }
    }
    class Category
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var evenNumbers =
                from num in numbers
                where num % 2 == 0
                select num;
            foreach(var item in evenNumbers)
            {
                Console.Write(item + " ");
            }

            string[] words = { "cherry", "apple", "blueberry" };
            var wordsSortedByLength =
                from word in words
                orderby word.Length descending
                select word;
            foreach (var word in wordsSortedByLength)
            {
                Console.WriteLine(word);
            }

            //grouping results with LINQ
            int[] numbers2 = 
               { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0, 10, 11, 12, 13 };
            int divisor = 5;

            var numberGropedByAdvisor =
                from number in numbers2
                group number by number % divisor into gr0up
                select new { Remainder = gr0up.Key, Numbers = gr0up };
            foreach(var group in numberGropedByAdvisor)
            {
                Console.WriteLine("Numbers with remainder of {0} when divided by {1}",group.Remainder,divisor);
                foreach (var number in group.Numbers)
                {
                    Console.WriteLine(number);
                }
            }

            //connecting data with LINQ
            List<Category> categories = new List<Category>()
            {
                new Category() { Name = "Fruit", ID = 1},
                new Category() { Name = "Food", ID = 2},
                new Category() { Name = "Shoe", ID = 3},
                new Category() { Name = "Juice", ID = 4},
            };
            List<Product> products = new List<Product>()
            {
                new Product() { Name = "Banana", CategoryID = 1},
                new Product() { Name  = "Strawberry", CategoryID = 1},
                new Product() { Name = "Chicken meat", CategoryID = 2},
                new Product() { Name = "Apple Juice", CategoryID = 4},
                new Product() { Name = "Fish", CategoryID = 2},
                new Product() { Name = "Orange Juice", CategoryID = 4},
                new Product() { Name = "Sandal", CategoryID = 3},
            };

            var productsWithCategories =
                from product in products
                join category in categories
                on product.CategoryID equals category.ID
                select new { Name = product.Name, Category = category.Name };
            foreach(var item in productsWithCategories)
            {
                Console.WriteLine(item);
            }
        }
    }
}
