﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LambdaExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int> { 1, 2, 3, 4, 5, 6 };
            List<int> evenNumbers = list.FindAll(x => (x % 2) == 0);
            foreach(var num in evenNumbers)
            {
                Console.Write("{0}", num);
            }
            Console.WriteLine();

            //

            List<Dog> dogs = new List<Dog>()
            {
                new Dog {Name = "Rex", Age = 5},
                new Dog {Name = "Sharo", Age = 0},
                new Dog {Name = "Aleks", Age = 3}
            };

            var names = dogs.Select(x => x.Name);
            foreach(var name in names)
            {
                Console.WriteLine(name);
            }

            var newDogsList = dogs.Select(x => new { Age = x.Age, FirstLetter = x.Name[0] });
            foreach(var item in newDogsList)
            {
                Console.WriteLine(item);
            }

            //sorting with lambda expressions
            var sortedDogs = dogs.OrderByDescending(x => x.Age);
            foreach(var dog in sortedDogs)
            {
                Console.WriteLine("Dog {0} is {1} years old.",dog.Name,dog.Age);
            }

            //operators in lambda expressions
            List<int> numbers = new List<int>() { 20, 1, 4, 8, 9, 44 };
            var evenNumbersList = numbers.FindAll((i) =>
            {
                Console.WriteLine("The value of i is: {0}",i);
                return (i % 2) == 0;
            });
            foreach(var evenNumber in evenNumbersList)
            {
                Console.WriteLine(evenNumber);
            }

            //lambda expressions as delegates
            Func<bool> boolFunc = () => true;
            Func<int, bool> intFunc = (x) => x < 10;
            if (boolFunc() && intFunc(5))
            {
                Console.WriteLine("5<10");
            }
        }
    }
}
