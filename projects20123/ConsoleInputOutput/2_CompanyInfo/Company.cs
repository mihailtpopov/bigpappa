﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_CompanyInfo
{
    class Company
    {
        public static void Info(string name,string adress, string website, int phoneNumber, int faxNumber,string managerFirstName,
            string managerFamilyName, int managerPhoneNumber)
        {
            Console.WriteLine("Company full details:");
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Adress: " + adress);
            Console.WriteLine("Website: " + website);
            Console.WriteLine("Phone number: {0:(0##) ### ####}",phoneNumber);
            Console.WriteLine("Fax number: {0:(0#) ### ####}",faxNumber);
            Console.WriteLine("Manager first name: " + managerFirstName);
            Console.WriteLine("Manager second name: " + managerFamilyName);
            Console.WriteLine("Manager phone number: {0:(0##) ### ####",managerPhoneNumber);
        }
    }
}
