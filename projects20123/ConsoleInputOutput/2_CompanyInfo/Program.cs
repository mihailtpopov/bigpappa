﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_CompanyInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter company name: ");
            string name = Console.ReadLine();
            Console.Write("Enter company adress: ");
            string adress = Console.ReadLine();
            Console.Write("Enter company phone number: ");
            int phoneNumber = int.Parse(Console.ReadLine());
            Console.Write("Enter company fax number: ");
            int faxNumber = int.Parse(Console.ReadLine());
            Console.Write("Enter company website: ");
            string website = Console.ReadLine();
            Console.WriteLine("Enter manager first name: ");
            string managerFirstName = Console.ReadLine();
            Console.WriteLine("Enter manager second name: ");
            string managerSecondName = Console.ReadLine();
            Console.WriteLine("Enter manager phone number: ");
            int managerPhoneNumber = int.Parse(Console.ReadLine());

            Company.Info(name, adress, website, phoneNumber, faxNumber, managerFirstName, managerSecondName, managerPhoneNumber);
        }
    }
}
