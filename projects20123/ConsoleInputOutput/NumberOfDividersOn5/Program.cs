﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberOfDividersOn5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first number: ");
            int firstNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter second number: ");
            int secondNumber = int.Parse(Console.ReadLine());
            int addToBeDividable = firstNumber - (firstNumber % 5) + 5;
            int removeToBeDividable = secondNumber - (secondNumber%5);
            int subtractNumbers = removeToBeDividable - addToBeDividable;
            if(subtractNumbers==0)
            {
                Console.WriteLine("The number of numbers dividable by 5 in the diapazon {{0}-{1}} is: 1", firstNumber, secondNumber);
                return;
            }
            int dividableBy5 = (subtractNumbers / 5) + 1;
            Console.WriteLine("The number of numbers dividable by 5 in the diapazon {{0}-{1}} is: {2}", firstNumber,secondNumber,dividableBy5);

        }
    }
}
