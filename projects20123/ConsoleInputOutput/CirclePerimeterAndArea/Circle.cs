﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CirclePerimeterAndArea
{
    class Circle
    {
        public static double CalculateArea(double r)
        {
            double area = 3.14 * r * r;
            return area;
        }
        public static double CalculatePerimeter(double r)
        {
            double perimeter = 2 * 3.14 * r;
            return perimeter;
        }
        public static void PrintBothCalculations()
        {
            Console.WriteLine("This program is calculating the area and the perimeter of a circle");
            Console.WriteLine("Please, enter its radius: ");
            double r = double.Parse(Console.ReadLine());
            Console.WriteLine("The perimeter of the circle is: " + CalculatePerimeter(r));
            Console.WriteLine("The area of the circle is: " + CalculateArea(r));
        }
    }
}
