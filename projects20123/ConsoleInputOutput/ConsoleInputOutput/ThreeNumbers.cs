﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleInputOutput
{
    class _ThreeNumbers
    {
        public static double CalculateSum(int a, int b, int c)
        {
            int sum = a + b + c;
            return sum;
        }
        public static void PrintSum()
        {
            Console.WriteLine("Enter three integers: ");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());
            Console.WriteLine("The sum of the integers is: " + CalculateSum(a, b, c));
        }

    }
}
