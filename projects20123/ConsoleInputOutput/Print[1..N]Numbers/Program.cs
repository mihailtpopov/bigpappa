﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_1._.N_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("n=");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("The numbers from [1..{0}] are:",n);
            for(int i =1;i<=n;i++)
            {
                Console.WriteLine(i);
            }
            
        }
    }
}
