﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintFiveNumbersSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            Console.WriteLine("Enter five integers: ");
            for(int i = 1;i<=5;i++)
            {
                int number;
                string strNumber = Console.ReadLine();
                if(Int32.TryParse(strNumber, out number))
                {
                    sum += number;
                }
                else
                {
                    Console.WriteLine("You must enter a valid number! Try again!");
                    --i;
                }
            }
            Console.WriteLine("The sum of the integers is: {0}",sum);
        }
    }
}
