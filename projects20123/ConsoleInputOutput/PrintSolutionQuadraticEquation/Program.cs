﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintSolutionQuadraticEquation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program solves quadratic equation");
            Console.WriteLine("Enter a, b and c: ");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());
            char squareRoot = '\u221A';
            char divingLineSymbol = '\u2212';
            double D = Math.Sqrt(b - 4 * a * c); 
            if(D<0)
            {
                Console.WriteLine("The qudratic equation does not has roots");
                return;
            }
            Console.WriteLine("{0}x*x+{1}x+c=0",a,b,c);
            if(a!=0)
            {
                if (D > 0)
                {
                    Console.WriteLine("x1={0}", ((-b + D) / (2 * a)));
                    Console.WriteLine("x2={0}", ((-b - D) / (2 * a)));
                }
                else
                {
                    Console.WriteLine("x={0}",-b/(2*a));
                }
            }
            else
            {
                Console.WriteLine("\"a\" must be different from 0");
            }
            Console.WriteLine(squareRoot);
            Console.WriteLine(divingLineSymbol);

        }
    }
}
