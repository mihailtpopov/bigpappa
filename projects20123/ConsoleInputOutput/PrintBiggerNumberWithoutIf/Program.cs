﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintBiggerNumberWithoutIf
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int secondNumber = int.Parse(Console.ReadLine());
            if(number==secondNumber)
            {
                Console.WriteLine("The numbers are equal");
                return;
            }
            int biggerNumber = Math.Max(number, secondNumber);
            Console.WriteLine(biggerNumber);
            
        }
    }
}
