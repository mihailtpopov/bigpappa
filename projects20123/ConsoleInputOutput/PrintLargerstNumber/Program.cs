﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLargerstNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program returns the largest from 5 integers");
            int largestNumber=int.MinValue;
            for(int i=1;i<=5;i++)
            {
                int number = int.Parse(Console.ReadLine());
                if(number>largestNumber)
                {
                    largestNumber = number;
                }
            }
            Console.WriteLine("The largest number is:{0}",largestNumber);
        }
    }
}
