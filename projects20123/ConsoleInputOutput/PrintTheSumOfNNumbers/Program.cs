﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintTheSumOfNNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter the number of numbers you want to read: ");
            int n = int.Parse(Console.ReadLine());
            int sum = 0;
            for(int i =0;i<n;i++)
            {
                int number = int.Parse(Console.ReadLine());
                sum += number;
            }
            Console.WriteLine("The sum of the numbers is: {0}",sum);
        }
    }
}
