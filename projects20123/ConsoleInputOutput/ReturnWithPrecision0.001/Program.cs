﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReturnWithPrecision0._001
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            int one = 1;
            double counter=1;
            double oldSum = 0;
            double extractSums;
                while(sum-Math.Floor(sum)!=0.001)
                {
                    //1
                    oldSum = sum;//0,1
                    sum += (one / counter);//1,1,5
                    counter++;
                    extractSums = sum - oldSum;
                    if(extractSums<0.001)
                    {
                        Console.WriteLine(sum);
                        return;
                    }
                }
                
                
        }
    }
}
