﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ЕxceptionsExamples
{
    class TryCatchExample
    {
        static void ReadFile(string fileName)
        {
            try
            {
                TextReader reader = new StreamReader("WrongTextFile.txt");
                string line = reader.ReadLine();
                Console.WriteLine(line);
                reader.Close();
            }
            catch(FileNotFoundException fnfe)
            {
                //exception handler for FileNotFoundException
                //We just inform the user that there is no such file
                Console.WriteLine("The file {0} is not found.",fnfe.FileName);
            }
            catch(IOException ioe)
            {
                //exception handler for FileNotFoundException
                //We just print the stack trace on the console
                Console.WriteLine(ioe.StackTrace);
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
