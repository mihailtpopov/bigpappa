﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SurelyClosingFile_stream_
{
    class Program
    {
        static void ReadFile(string fileName)
        {
            TextReader reader = new StreamReader(fileName);
            try
            {
                string line = reader.ReadLine();
                Console.WriteLine(line);
            }
            finally
            {
               reader.Close() 
            }
            //dispose pattern - шаблон за освобождаване на ресурси
        }
        static void Main(string[] args)
        {
            
        }
    }
}
