﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ReleasingMoreThanOneResources
{
    class Program
    {
        static void ReadFile(string fileName, string secondFileName)
        {
            TextReader r1 = new StreamReader(fileName);
            try
            {
                TextReader r2 = new StreamReader(secondFileName);
                try
                {
                    string line = r1.ReadLine();
                    string line2 = r2.ReadLine();
                }
                finally
                {
                    r2.Close();
                }
            }
            finally
            {
                r1.Close();
            }

            //ooooooor
            TextReader r3 = new StreamReader(fileName);
            TextReader r4 = new StreamReader(secondFileName);
            try
            {
                //use r3 and r4
            }
            finally
            {
                r3.Close();
                r4.Close();
            }
        }


        static void Main(string[] args)
        {
        }
    }
}
