﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace HandlingAnExceptionsOnLevels
{
    class Program
    {
        static void ReadFile(string fileName)
        {
            try
            {
                TextReader reader = new StreamReader(fileName);
                string line = reader.ReadLine();
                Console.WriteLine(line);
                reader.Close();
            }
            catch(FileNotFoundException fnfe)
            {
                Console.WriteLine("File {0} does not exist.", fnfe.FileName);
            }
        }
        static void Main(string[] args)
        {
            try
            {
                string fileName = "WrongFileName.txt";
                ReadFile(fileName);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Bad thing happened", e);
            }

        }
    }
}
