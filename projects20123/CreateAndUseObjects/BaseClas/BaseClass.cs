﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseClas
{
    class BaseClass
    {
        public void Method1()
        {
            Console.WriteLine("Base class - Method 1");
        }
        public virtual void Method2()
        {
            Console.WriteLine("Derived class - Method 2");
        }
    }
}
