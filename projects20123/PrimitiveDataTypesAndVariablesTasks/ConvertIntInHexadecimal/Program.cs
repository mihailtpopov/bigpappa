﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertIntInHexadecimal
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 256;
            string hexValue = number.ToString("X");//converts the integer in a hexadecimal system in string variable
            string hexValueFormat = "0x" + hexValue;
            Console.WriteLine(hexValueFormat);

        }
    }
}
