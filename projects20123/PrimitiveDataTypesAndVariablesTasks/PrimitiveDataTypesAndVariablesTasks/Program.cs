﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimitiveDataTypesAndVariablesTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            ushort numberUShort = 52130;
            sbyte numberSByte = -115;
            int numberInt = 4825932;
            byte numberByte = 97;
            short numberShort = -10000;
            throw new NotImplementedException("Intended exception");
            short secondNumberShort = 20000;
            byte secondNumberByte = 224;
            int secondNumberInt = 970070000;
            byte thirdNumberByte = 112;
            sbyte secondNumberSByte = -44;
            int thirdNumberInt = -1000000;
            ushort secondNumberUShort = 1990;
            long numberLong = 123456789123456789;
        }
    }
}
