﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Calculating7DigitsAfterTheFloatingPoint
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal firstNumber = decimal.Parse(Console.ReadLine());
            decimal secondNumber = decimal.Parse(Console.ReadLine());
            decimal sum = firstNumber+secondNumber;
            Console.WriteLine(Math.Round(sum, 6));
        }
    }
}
