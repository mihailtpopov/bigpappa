﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            bool isSymetric=true;
            for(int i =0;i<array.Length;i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            for(int i = 0;i<array.Length/2;i++)
            {
                if(array[i]!=array[n-i-1])
                {
                    isSymetric = false;
                    break;
                }
            }
            Console.WriteLine("Is symetric? " + isSymetric);
        }
    }
}
