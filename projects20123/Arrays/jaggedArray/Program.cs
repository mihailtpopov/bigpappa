﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jaggedArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] jaggedArray = new int[2][];
            jaggedArray[0] = new int[5];
            jaggedArray[1] = new int[2];

            int[][] jaggedArray1 =
            {
                new int[]{2,5,7},
                new int[]{10,20,40},
                new int[]{3,25}
            };
            Console.WriteLine(jaggedArray1[0][1]);

            int[][,] myJaggedArray = new int[3][,];
            myJaggedArray[0] = new int[,] { { 2, 5, 7 }, { 10, 30, 42 } };
            myJaggedArray[1] = new int[,] { { 2, 3, 4, 5, 6 }, { 123, 23, 49, 58,5 } };

            Console.WriteLine("                        Opa");
        }
    }
}
