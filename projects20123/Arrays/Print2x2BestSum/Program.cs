﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print2x2BestSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int bestRow = 0;
            int bestCol = 0;
            int bestSum = int.MinValue;
            int[,] array = 
            {
                 { 0, 2, 4, 0, 9, 5 },
                 { 7, 1, 3, 3, 2, 1 },
                 { 1, 3, 9, 8, 5, 6 },
                 { 4, 6, 7, 9, 1, 0 }
            };
            for(int row=0;row<array.GetLength(0)-1;row++)
            {
                
                for(int col = 0; col<array.GetLength(1)-1;col++)
                {
                   int sum = array[row, col] + array[row, col + 1] + array[row + 1, col] + array[row + 1, col + 1];
                    if(sum>bestSum)
                    {
                        bestRow = row;
                        bestCol = col;
                        bestSum = sum;
                    }
                }
            }
            Console.WriteLine("Best matrix:");
            Console.WriteLine("{0} {1}",array[bestRow,bestCol],array[bestRow,bestCol+1]);
            Console.WriteLine("{0} {1}",array[bestRow+1,bestCol],array[bestRow+1,bestCol+1]);
            Console.WriteLine("Sum of the matrix is: " + bestSum);
        }
    }
}
