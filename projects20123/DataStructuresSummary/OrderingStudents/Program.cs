﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace OrderingStudents
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, List<Student>> courses = new Dictionary<string, List<Student>>();
            StreamReader reader = new StreamReader();
            using(reader)
            {
                while(true)
                {
                    string line = reader.ReadLine();
                    if(line==null)
                    {
                        break;
                    }
                    List<Student> students;
                    string[] entry = line.Split('|');
                    string firstName = entry[0].Trim();
                    string lastName = entry[1].Trim();
                    string course = entry[2].Trim();
                    if(!courses.TryGetValue(course, out students))
                    {
                        students = new List<Student>();
                        courses.Add(course, students);
                    }
                    Student student = new Student(firstName, lastName);
                    students.Add(student);
                }

                foreach(string course in courses.Keys)
                {
                    Console.WriteLine("Course " + course + ":");
                    List<Student> students = courses[course];
                    students.Sort();
                    foreach(Student student in students)
                    {
                        Console.WriteLine("\t{0}", student);
                    }
                }
            }
            
        }
    }
}
