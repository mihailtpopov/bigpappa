﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresSummary
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = { "море", "бира", "плаж", "кеф" };
            Queue<HashSet<string>> subsetQueue = new Queue<HashSet<string>>();
            HashSet<string> emptySet = new HashSet<string>();
            subsetQueue.Enqueue(emptySet);
            while(subsetQueue.Count>0)
            {
                HashSet<String> subset = subsetQueue.Dequeue();
                Console.Write("{");
                foreach(string word in subset)
                {
                    Console.Write("{0} ",word);
                }
                Console.WriteLine("}");

                //Generate and enqueue all possible child subsets
                foreach(string element in words)
                {
                    if(!subset.Contains(element))
                    {
                        HashSet<string> newSubset = new HashSet<string>();
                        newSubset.UnionWith(subset);
                        newSubset.Add(element);
                        subsetQueue.Enqueue(newSubset);
                    }
                }
            }
        }
    }
}
