﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outlook
{
    public class Hair
    {
        Color color;
        public Color Colorr
        {
            get { return color; }
            set { color = value; }
        }
        Style style;

        public Style Stylee
        {
            get { return style; }
            set { style = value; }
        }
        public enum Color 
        { 
            Black, Red, Blonde, Brown, White
        }
        public enum Style
        {
            Bold, Soldier, Faith, MediumLong, VeryLong, Long
        }
         
        public Hair(Color color, Style style)
        {
            this.color = color;
            this.style = style;
        }
    }
}
