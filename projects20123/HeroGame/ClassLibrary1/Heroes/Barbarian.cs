﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook;
namespace EventExample
{
    internal class Barbarian : Hero
    {
       public Barbarian(string name, char gender, Hair hair) : base(name,gender,hair)
        {
            this.Name = name;
            this.Gender = gender;
            this.Hair = hair;
            this.CurrentHealth = 140;
            this.Level = 1;
            this.CurrentExp = 0;
            //this.MaximumAttack=12;
            this.CritStrikePercent = 8;
        }

        public override void Jump()
        {
            throw new NotImplementedException();
        }

        public override void Laugh()
        {
            throw new NotImplementedException();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override void Talk()
        {
            throw new NotImplementedException();
        }
    }
}
