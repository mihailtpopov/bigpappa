﻿using EventExample;
using HeroesGame.Creatures;
using Outlook;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace HeroesGame.Heroes
{
    internal class Mage : Hero
    {
        int mana = 0;
        public Mage(string name, char gender, Hair hair)
            : base(name, gender, hair)
        {
            this.Name = name;
            this.Gender = gender;
            this.Hair = hair;
            this.mana = 30;
        }
        public enum HealingSpells
        {
           UltimateGoddessHeal = 150, HealOfAfrodita = 75, HealTheLamb = 35
        }
        public enum AttackingSpells
        {
            UltimateMadness = 60, RageOfZeus = 30, Lightning=15
        }
        public void Heal(Hero hero,HealingSpells magic)
        {
            switch(magic)
            {
                case HealingSpells.UltimateGoddessHeal:
                    hero.CurrentHealth += (int)HealingSpells.UltimateGoddessHeal * this.Level;
                    break;
                case HealingSpells.HealOfAfrodita:
                    hero.CurrentHealth += (int)HealingSpells.HealOfAfrodita * this.Level;
                    break;
                case HealingSpells.HealTheLamb:
                    hero.CurrentHealth += (int)HealingSpells.HealTheLamb * this.Level;
                    break;
            }
        }
        public void SpellAttack(Object enemy, AttackingSpells attackingSpell)
        {
            if(enemy is Creature)
            {
                switch(attackingSpell)
                {

                }    
            }
        }
        public override void Jump()
        {
            throw new NotImplementedException();
        }

        public override void Laugh()
        {
            throw new NotImplementedException();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override void Talk()
        {
            throw new NotImplementedException();
        }
    }
}
