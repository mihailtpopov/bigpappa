﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeroesGame.Heroes
{
    interface Actions
    {
        void Talk();
        void Laugh();
        void Jump();
        void Run();
    }
}
