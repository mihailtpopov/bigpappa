﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook;
namespace EventExample
{
    class Rogue : Hero
    { 
        public Rogue(string name, char gender, Hair hair) : base(name, gender,hair)
        {
            this.Name = name;
            this.Gender = gender;
            this.Hair = hair;
            this.CurrentExp = 0;
            this.CurrentHealth = 100;
            this.MaxAttack = 9;
            this.Level = 1;
            this.CritStrikePercent = 30;
        }

        public override void Jump()
        {
            throw new NotImplementedException();
        }

        public override void Laugh()
        {
            throw new NotImplementedException();
        }

        public override void Run()
        {
            throw new NotImplementedException();
        }

        public override void Talk()
        {
            throw new NotImplementedException();
        }
    }
}
