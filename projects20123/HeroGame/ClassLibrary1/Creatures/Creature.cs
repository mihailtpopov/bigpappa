﻿using System;
using System.Diagnostics;
using System.Reflection.Metadata.Ecma335;

namespace HeroesGame.Creatures
{
    abstract class Creature
    {
        int level=0;
        int currentHealth=0;
        private int defaultHealth = 0;
        int maxAttack=0;
        int currentAttack=0;
        bool isDead = false;
        public int Level { get => level;
            set
            {
                if (level <= 0 && level > 99)
                    throw new ArgumentException("You can't have a creature with that level!");
                level = value;
            }
        }

        public int CurrentHealth {  get => currentHealth;
            set
            {
                currentHealth = value;
                if (OnHealthChange != null)
                    OnHealthChange.Invoke(this, this.currentHealth);
            }
        }
        public int DefaultHealth { get => defaultHealth; set => defaultHealth = value; }
        public int MaxAttack { get => maxAttack; set => maxAttack = value;}
        
        public int CurrentAttack { get => currentAttack; 
            set
            {
                this.currentAttack = value;

                //it means if someone subscribed for the event
                if (OnAttackPower != null)
                {
                    this.OnAttackPower(this, this.currentAttack);
                }
            }
        }

        public bool IsDead { get => isDead; set => isDead = value; }

        public event EventHandler<int> OnAttackPower;
        public event EventHandler<int> OnHealthChange;

        public Creature(int level, int health)
        {
            this.Level = level;
            this.currentHealth = health;
            this.OnAttackPower += StrikePower;
            this.OnHealthChange += HealthChange;
        }

        private void  HealthChange(object sender, int health)
        {
            var creature = (Creature)sender;
            Console.WriteLine($"{creature.GetType().Name} health changed: {health}");
        }

        private void StrikePower(object sender, int attackPower)
        {
            var creature = (Creature)sender;
            Console.WriteLine($"{creature.GetType().Name} hits {attackPower}");
        }
    }
}
