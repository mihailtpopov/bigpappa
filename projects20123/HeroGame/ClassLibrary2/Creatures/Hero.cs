﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary2.HeroesOutlook;
using System.Threading;
using System.Runtime.CompilerServices;
using Actions = ClassLibrary2.Heroes;

namespace Creatures
{
     abstract class Hero : Actions
    {   
         //hero name
        private string name = null;
         //the health of the hero
        private int currentHealth = 0;
        private int defaultHealth = 0;
         //the level of the hero
        private int level = 0;
         //maximum possible attack without critical strike
        private int maxAttack = 0;
         //current attack power - most probably will be increased because of the enemy hero strength
        private int currentAttackPower = 0;
        //reaching the the needed experience(points) for the next level, the hero will increase its level
        private int currentExp = 0;
        //the chance in percentage the hero to make a critical strike
        private bool isDead;
        private double critStrikePercent = 0;
        //increase the damage of the hero if he succeed to make critical hit
        private int critStrikePower = 1;
        private bool critSuccessfull=false;
         //male or female gender
        private char gender = '\u0000';
         //choosing the hair color and style of the hero
        private Hair hair;
        public enum PotionSize
        {
            Big=200, Medium = 100, Small = 50
        }
        public Hair Hair { get => hair;  set => hair = value; }
        public double CritStrikePercent { get => critStrikePercent; set => critStrikePercent = value; }
        public char Gender { get => gender; set => gender = value;}    
        private int CurrentAttackPower
        {
            get { return this.currentAttackPower; }
            set
            {  
                    this.currentAttackPower = value;

                //it means if someone subscribed for the event
                if (OnAttackPower != null)
                {
                  if(!critSuccessfull)
                    this.OnAttackPower.Invoke(this, this.currentAttackPower);
                }
            }
        }
        public int MaxAttack { get => maxAttack;}
        private bool CritSuccesfull { get => critSuccessfull; 
            set
            {
               critSuccessfull = value;
                if (critSuccessfull)
                    if (OnCriticalStrike != null)
                        OnCriticalStrike.Invoke(this, this.CurrentAttackPower);
            }
              }
        public int Level
        {
            get { return this.level; }
            set {
                if (level <= 0)
                    throw new ArgumentException("Heroes level must be positive");
                this.level = value; 
                if(OnLevelUp!=null)
                { 
                    this.OnLevelUp.Invoke(this, this.Level);
                }
            }
        }
        public int CurrentExp { get => currentExp; set => currentExp = value;}
        public int ExpForNextLevel { get => 2000*this.Level; }
        public string Name {  get => name; set => name = value;}
        public int CurrentHealth
        {
            get { return this.currentHealth; }
            set
            {
                this.currentHealth = value;
                if (OnHealthChanged != null)
                {
                    this.OnHealthChanged.Invoke(this, this.currentHealth);
                }
            }
        }
        public int DefaultHealth { get => defaultHealth; }
        public bool IsDead1 { get => isDead; set => isDead = value; }


        public Hero(string name,char gender, Hair hair)
        {
            this.name = name;
            this.gender = gender;
            this.hair = hair;
            this.isDead = false;
            this.OnHealthChanged += HealthChanged;
            this.OnHealthChanged += IsDead;
            this.OnAttackPower += ShowAttackPower;
            this.OnCriticalStrike += CriticalStrike;
            this.OnLevelUp += LevelUp;
        }

        public event EventHandler<int> OnHealthChanged;
        public event EventHandler<int> OnAttackPower;
        public event EventHandler<int> OnCriticalStrike;
        public event EventHandler<int> OnLevelUp;

        private void LevelUp(object sender, int level)
        {
            var hero = (Hero)sender;
            Console.WriteLine("{0} increase level: {1}", hero.Name, level);
            hero.defaultHealth = (int)(1.3 * hero.defaultHealth);
            hero.maxAttack = (int)(1.3 * hero.maxAttack);
        }
        private void IsDead(object sender, int e)
        {
            var hero = (Hero)sender;
            if (hero.CurrentHealth <= 0)
            {
                Console.WriteLine("{0} is dead!", hero.Name);
            }
        }
        private void ShowAttackPower(object sender, int e)
        {
            var hero = (Hero)sender;
            //if crit is successfull CriticalStrike event method will be invoked
            Console.WriteLine("{0} hit {1} damage", hero.Name, hero.CurrentAttackPower);
        }

        private static void HealthChanged(object sender, int e)
        {
                var hero = (Hero)sender;
            if (!(hero.currentHealth > 0 && hero.currentHealth < 20))
             {
                //hero health divided by 7 is consider critical for his life-health potion should be considered
                if (hero.currentHealth>hero.currentHealth/7 && hero.currentHealth>0)
                    Console.WriteLine("{0} health changed: {1}", hero.Name, hero.CurrentHealth);
               else
                    Console.WriteLine($"{hero.Name} health is critical - {hero.CurrentHealth}! " +
                        $"Consider taking health potion");
             }
          
        }
        private static void CriticalStrike(object sender, int currentAttack)
        {
            var hero = (Hero)sender;
            Console.WriteLine("{0} made critical strike! {1}",hero.Name,currentAttack);
        }
         //CriticalStrikeBonus multiplies the currentAttack by 2
         private int CriticalStrikeBonus()
        {
            Random criticalChance = new Random();
            //checking the probability for critical strike, passing eqauls 0 to  the Next method
            if (critStrikePercent != 0 && criticalChance.Next(0, (int)(100 / critStrikePercent)) == 0)
            {
                critSuccessfull = true;
                return 2;
            }
            else
                return 1;
        }
         private void GainedExperience(Object killedCreature)
         {
            if (!(killedCreature is Hero || killedCreature is Creature))
                throw new InvalidCastException("The hero have to fight with someone from the game");
           
             int expForHero = 1000*this.level;
            int expForCreature = expForHero / 5;
             if(killedCreature is Hero)
             {
                 Hero hero = (Hero)killedCreature;
                 if(hero.Level<this.Level)
                 {
                     this.CurrentExp += expForHero / (this.Level - hero.Level);
                 }
                 else if(hero.Level==this.Level)
                 {
                     this.CurrentExp += expForHero;
                 }
                 else
                 {
                     this.CurrentExp += expForHero * (hero.Level - this.Level);
                 }
             }
             else
            {
                Creature creature = (Creature)killedCreature;
                if (creature.Level < this.Level)
                {
                    this.CurrentExp += expForCreature / (this.Level - creature.Level);
                }
                else if (creature.Level == this.Level)
                {
                    this.CurrentExp += expForCreature;
                }
                else
                {
                    this.CurrentExp += expForCreature * (creature.Level - this.Level);
                }
            }
             //2000*level necessary experience to increase level
             if(this.CurrentExp>=2000*level)
                this.Level++;
         }

        public void FightWithHero(Hero heroEnemy)
        {
            if (isDead)
                throw new ArgumentException("You can't fight with this hero! He is already death!");
            Random possibleAttackPower = new Random();
            int thisCurrentLevel = this.Level;
            int enemyCurrentLevel = heroEnemy.Level;
            //declaring two variables in case of leveling up - the hero health will went full and be increased and maximum attack
            //power will be increased
            while (this.CurrentHealth > 0 && heroEnemy.CurrentHealth > 0)
            {
                this.CurrentAttackPower = possibleAttackPower.Next(this.MaxAttack - (int)(MaxAttack / 1.5)
                    , MaxAttack) * this.CriticalStrikeBonus();
                heroEnemy.CurrentHealth -= this.CurrentAttackPower;

                if (heroEnemy.CurrentHealth <= 0)
                {
                    heroEnemy.isDead = true;
                    this.GainedExperience(heroEnemy);
                    if (heroEnemy.Level > enemyCurrentLevel)

                        return;
                }
                Console.WriteLine();
                //1.5 is an average odd for hitting less damage, because of enemy hero strength
                heroEnemy.CurrentAttackPower = possibleAttackPower.Next(heroEnemy.MaxAttack - (int)(heroEnemy.MaxAttack / 1.5)
                    , MaxAttack) * heroEnemy.CriticalStrikeBonus();
                this.CurrentHealth -= heroEnemy.CurrentAttackPower;
                if (this.CurrentHealth <= 0)
                {
                    heroEnemy.GainedExperience(this);
                    this.isDead = true;
                    return;
                }
                Console.WriteLine();
                Thread.Sleep(6000);
            }
        }
        private void FightWithCreature(Creature creature)
        {
            if (creature.CurrentHealth <= 0)
                throw new ArgumentException("You can't fight with this hero! He is already death!");
            Random possibleAttackPower = new Random();
            int thisCurrentLevel = this.Level;
            int enemyCurrentLevel = creature.Level;
            //declaring two variables in case of leveling up - the hero health will went full and be increased and maximum attack
            //power will be increased
            while (this.CurrentHealth > 0 && creature.CurrentHealth > 0)
            {
                this.CurrentAttackPower = possibleAttackPower.Next(this.MaxAttack - (int)(MaxAttack / 1.5)
                    , MaxAttack) * this.CriticalStrikeBonus();
                creature.CurrentHealth -= this.CurrentAttackPower;

                if (creature.CurrentHealth <= 0)
                {
                    this.GainedExperience(creature);
                    return;
                }
                Console.WriteLine();
                //1.5 is an average odd for hitting less damage, because of enemy hero strength
                creature.CurrentAttack = possibleAttackPower.Next(creature.MaxAttack - (int)(creature.MaxAttack / 1.5)
                     , MaxAttack);
                this.CurrentHealth -= creature.CurrentAttack;
                if (this.CurrentHealth <= 0)
                {
                    this.isDead = true;
                    return;
                }
                    
                Console.WriteLine();
                Thread.Sleep(6000);
            }
        }
        private void FightWithCreatureLair(CreatureLair<Creature> lair)
        {
            if (lair.Creatures.Count == 0)
                throw new ArgumentException("Hero can't fight alone! Creatures must live in this lair");
            Random possibleAttack = new Random();
            //ask slavko if this will change original creature lair
        
            while (this.currentHealth > 0 || lair.Creatures.Count > 0)
            {
                //doing it with for, not with foreach cause with foreach hero doesn't know when 
                //all of the creatures finished their attacks, so he can start attacking.
                for(int currCreat = 0;currCreat<lair.Creatures.Count;currCreat++)
                {
                    lair.Creatures[currCreat].CurrentAttack = possibleAttack.Next(
                        lair.Creatures[currCreat].MaxAttack - (int)(lair.Creatures[currCreat].MaxAttack / 1.5),
                        lair.Creatures[currCreat].MaxAttack);
                    this.currentHealth -= lair.Creatures[currCreat].CurrentAttack;
                    //our hero attacks after last creature
                    if (currCreat == lair.Creatures.Count - 1)
                    {
                        this.currentAttackPower = possibleAttack.Next(
                            this.MaxAttack - (int)(this.MaxAttack / 1.5), this.MaxAttack);

                        lair.Creatures[currCreat].CurrentHealth -= this.currentAttackPower;

                        if (lair.Creatures[currCreat].CurrentHealth <= 0)
                            lair.Creatures.Remove(lair.Creatures[currCreat]);
                    }   
                }
            }
        }
        public void FightWith(Object enemy)
            {
                if (!(enemy is Hero || enemy is Creature) || enemy is CreatureLair<Creature>)
                    throw new ArgumentException($"{this.Name} alone can fight only with someone " +
                        $"heroes and creatures!");
            if (enemy is Hero)
                FightWithHero((Hero)enemy);
            else if (enemy is Creature)
                FightWithCreature((Creature)enemy);
            else
                FightWithCreatureLair((CreatureLair<Creature>)enemy);

            }

        public void TakeHealthPotion(PotionSize potionSize)
        {
            this.CurrentHealth += (int)potionSize;
        }

        public abstract void Talk();
        public abstract void Laugh();
        public abstract void Jump();
        public abstract void Run();
        public abstract void Roaring();
    }
    }

