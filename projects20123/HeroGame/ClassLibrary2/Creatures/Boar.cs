﻿using System;
using System.Collections.Generic;
using System.Text;

namespace creaCreatures
{
    internal class Boar : Creature
    {
        public Boar(int level, int health) : base(level, health)
        {
            this.Level = level;
            this.CurrentHealth = health;
            this.MaxAttack = level * 4;
        }
    }
}
