﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creatures
{
    class CreatureLair<Creature>
    {
        private int capacity;
        List<Creature> creatures;

        public CreatureLair(int capacity)
        {
            this.capacity = capacity;
            creatures = new List<Creature>();
        }

        public List<Creature> Creatures { get => creatures; set => creatures = value; }

        public void AddCreature(Creature creature)
        {
            if(Creatures.Count==capacity)
                throw new InvalidOperationException("It's getting overcrowded in here! No more creatures" +
                    "can join the lair!");

            creatures.Add(creature);
        }
    }
}

