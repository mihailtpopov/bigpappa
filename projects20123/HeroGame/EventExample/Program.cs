﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary2.HeroesOutlook;
using ClassLibrary2.Creatures;
namespace EventExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var barbarian = new Barbarian("Magnus",'m', new Hair(Hair.Color.White,Hair.Style.Long));
   
            var rogue = new Rogue("Arhos", 'm', new Hair(Hair.Color.Black,Hair.Style.Bold));

            rogue.FightWith(barbarian);
            barbarian.FightWith(rogue);
        }
    }
}
