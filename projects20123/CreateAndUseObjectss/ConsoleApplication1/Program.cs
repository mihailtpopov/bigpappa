﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int startTime = Environment.TickCount;
            for( int i = 0;i<10000000;i++)
            {
                sum++;
            }
            int endTime = Environment.TickCount;
            Console.WriteLine("The time elapsed is {0} sec",(endTime-startTime)/1000.0);
            //returns as a result the number of miliseconds which are elapsed after the computer has been started to
            // the moment of invoking the method.
        }
    }
}
