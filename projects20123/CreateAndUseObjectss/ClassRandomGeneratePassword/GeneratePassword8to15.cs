﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRandomGeneratePassword
{
    class GeneratePassword8to15
    {
        private const string CapitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string SmallLetters = "abcdefghijklmnopqrstuvwxyz";
        private const string Digits = "0123456789";
        private const string SpecialCharacters = "~!@#$%^&*()_+=`{}[]\\|':;.,/?<>";
        private const string AllChars = CapitalLetters + SmallLetters + Digits + SpecialCharacters;

        private static StringBuilder password = new StringBuilder();
        private static  Random rnd = new Random();
        private static void InsertAtRandomPosition(StringBuilder password, char specialChar)
        {
            int randomPosition = rnd.Next(password.Length+1);
            password.Insert(randomPosition, specialChar);
        }
        private static char GenerateChar(string availableChar)
        {
            int randomIndex = rnd.Next(availableChar.Length);
            char theChoosen = availableChar[randomIndex];
            return theChoosen;
        }
        static void Main(string[] args)
        {
            for(int i =0;i<2;i++)
            {
                char generatedCapital = GenerateChar(CapitalLetters);
                InsertAtRandomPosition(password, generatedCapital);
            }
            for(int i = 0;i<2;i++)
            {
                char generatedSmall = GenerateChar(SmallLetters);
                InsertAtRandomPosition(password, generatedSmall);
            }
            
                char generatedDigit = GenerateChar(Digits);
                InsertAtRandomPosition(password, generatedDigit);
            
            for(int i = 0;i<3;i++)
            {
                char generatedSpecialChar = GenerateChar(SpecialCharacters);
                InsertAtRandomPosition(password, generatedSpecialChar);
            }

            int count = rnd.Next(8);
            for(int i = 0;i<count;i++)
            {
                char specialChar = GenerateChar(AllChars);
                InsertAtRandomPosition(password, specialChar);
            }

            Console.WriteLine(password);
        }
    }
}
