﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateTriangleArea
{
    class Triangle
    {
      private static double TriangleArea(double firstSide, double secondSide, double angleInDegrees)
        {
            double angleInRadians = Math.PI * angleInDegree / 180.0;
            double area = 0.5 * firstSide * secondSide * angleInRadians;
            return area;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("This program calculates the area of a triangle by given two sides and angle in between");
            Console.WriteLine("Enter the first side:");
            double firstSide = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter the second side:");
            double secondSide = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter the angle in degrees:" );
            double angleInDegrees = double.Parse(Console.ReadLine());
            double area = TriangleArea(firstSide, secondSide, angleInDegrees);
            Console.WriteLine("The are of the triangle is: {0}",area);
        }
    }
}
