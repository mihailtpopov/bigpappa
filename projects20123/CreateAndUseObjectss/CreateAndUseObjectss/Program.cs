﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateAndUseObjectss
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat firstCat = new Cat();
            firstCat.Name = "Tony";
            firstCat.SayMiaau();
            Cat secondCat = new Cat("Adam", "white");
            secondCat.SayMiaau();
            Console.WriteLine("Cat {0} is {1}",secondCat.Name,secondCat.Color);

        }
    }
}
