﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_ChangeBitsss
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            string nInBinary = Convert.ToString(n, 2);
            int v = int.Parse(Console.ReadLine());
            int p = int.Parse(Console.ReadLine());
            int i = 1<<p;
            int temp = n | i;
            if(temp!=n)
            {
               if(v==1)
               {
                   n = n | i;
               }
            }
            else
            {
                if(v==0)
                {
                    n = n ^ i;
                }
            }
            Console.WriteLine(n);
            Console.ReadKey();
            
        }
    }
}
