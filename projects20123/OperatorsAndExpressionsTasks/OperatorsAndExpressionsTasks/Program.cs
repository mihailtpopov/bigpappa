﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsAndExpressionsTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            string oddOrEven;
            if(number % 2 == 0)
            {
                oddOrEven="even";
            }
            else
            {
                oddOrEven = "odd";
            }
            Console.WriteLine("The number you have entered is " + oddOrEven + ".");
        }
    }
}
