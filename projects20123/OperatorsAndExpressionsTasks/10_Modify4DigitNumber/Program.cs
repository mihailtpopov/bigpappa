﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_Modify4DigitNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            string strNumber = Console.ReadLine();
           if(strNumber.Length!=3)
           {
               throw new ArgumentException("You must enter a 4-digit number!");
           }
           int digit = default(int);
            int[] intArr = new int[3];
            int sumDigits = default(int);
            for(int i = 0; i<strNumber.Length;i++)
            {
                try
                {
                    digit = (int)Char.GetNumericValue(strNumber[i]);
                }
                catch (FormatException f)
                {
                    Console.WriteLine(f.Message);
                }
                intArr[i] = digit;
            }

            //1. Sum the digits
           foreach(int number in intArr)
           {
               sumDigits += number;
           }
           //2. Print number backwards
            for(int i = intArr.Length;i>=0;i--)
            {
                Console.Write(intArr[i]);
            }
            //3.
            int[] newIntArr = new int[3];
           intArr.CopyTo(newIntArr,0);

        }
    }
}
