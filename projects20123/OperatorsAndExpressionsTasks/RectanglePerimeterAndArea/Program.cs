﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectanglePerimeterAndArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first rectangle side: ");
            double length = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter second rectangle side: ");
            double height = double.Parse(Console.ReadLine());
            double rectanglePerimeter = 2*(length + height);
            double rectangleArea = length * height;
            Console.WriteLine("The rectangle area is: " + rectangleArea);
            Console.WriteLine("The rectangle perimeter is: " + rectanglePerimeter);
        }
    }
}
