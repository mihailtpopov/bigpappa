﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14_ChangeBitsOfPosition
{
    class Program
    {
        static void Main(string[] args)
        {
            uint n = uint.Parse(Console.ReadLine());
            Console.WriteLine("In binary:" + Convert.ToString(n, 2).PadLeft(32, '0'));
            int p;
            int temp;
            for(int i = 2;i<5;i++)
            {
                if(((n>>i)&1U) != ((n>>i+21)&1U))
                {
                    if(((n>>i)&1U)==1)
                    {
                        n = ~(1U << i) & n;
                        n = (1U << (i + 21)) | n;
                    }
                    else
                    {
                        n = (1U << i) | n;
                        n = ~(1U << (i + 21)) & n;
                    }
                }
            }
            Console.WriteLine("After change the number is:");
            Console.WriteLine(n);
            Console.WriteLine("In binary " + Convert.ToString(n,2).PadLeft(32,'0'));
        }
    }
}
