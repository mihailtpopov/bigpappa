﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindIfPointIsInsideCircle
{
    class Program
    {
        static void Main(string[] args)
        {
            //when x and y of the circle are 0,0 the formula is x^2 + y^2 = r^2,
            //but usually it is (x2-x1)^2+(y2-y1)^2=r^2
            int r = 5;
            Console.WriteLine("K(0,0),5");
            Console.WriteLine("Enter x and y for given point to check if its inside the circle");
            string strX = Console.ReadLine();
            double x;
            bool isXParsable = double.TryParse(strX, out x);
            string strY = Console.ReadLine();
            double y;
            bool isYParsable = double.TryParse(strY, out y);
            bool isPointInCircle = true;
            if(isXParsable&&isYParsable)
            {
                if((Math.Pow(x,2)+Math.Pow(y,2))<Math.Pow(r,2))
                {
                  isPointInCircle=true;
                }
                   else if((Math.Pow(x,2)+Math.Pow(y,2))==Math.Pow(r,2))
                {
                    Console.WriteLine("The point is on the circle");
                    return;
                }
                else
                {
                    isPointInCircle = false;
                }
            }
            else
            {
                Console.WriteLine("Please enter a valid values");
            }
            Console.WriteLine("The point is inside the circle: " + isPointInCircle);
        }
    }
}
