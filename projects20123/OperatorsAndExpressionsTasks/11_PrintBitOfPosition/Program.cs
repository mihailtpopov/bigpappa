﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_PrintBitOfPosition
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            string numberInBinary = Convert.ToString(n, 2);
            Console.WriteLine(numberInBinary);
            int p = int.Parse(Console.ReadLine());
            if(p<=0)
            {
                throw new ArgumentException("Position must be a positive number!");
            }
            if(p-1>numberInBinary.Length)
            {
                throw new ArgumentOutOfRangeException("Position is out of range");
            }
            Console.WriteLine(numberInBinary[p-1]);

           // int number = int.Parse(Console.ReadLine());
           // int p1 = int.Parse(Console.ReadLine());
           // int i = 1;
           // int mask = i << p1;
           // Console.WriteLine((number&mask)!=0?1:0);
        }
    }
}
