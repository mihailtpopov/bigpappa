﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_PointInsideBothCircleAndRectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program is checking if a given point is inside both circle and rectangle");
            //p = point, r = rectangle
            int circleR = 5;
            int rX1 = -1;
            int rY1 = 1;
            int rX2 = 5;
            int rY2 = 5;
            bool isInsideBoth;
            try
            {
                Console.Write("Enter point x: ");
                double pX = double.Parse(Console.ReadLine());
                Console.Write("Enter point y: ");
                double pY = double.Parse(Console.ReadLine());
                if(((pX>=rX1&& pX<=rX2)&&(pY>=rY1&&pY<=rY2))&&(Math.Pow(pX,2) + Math.Pow(pY,2) <= Math.Pow(circleR,2)))
                {
                    isInsideBoth = true;
                }
                else
                {
                    isInsideBoth = false;
                }
                Console.WriteLine("The point is inside both circle and rectangle - " + isInsideBoth);
            }
            catch(Exception)
            {
              Console.WriteLine("Enter a valid value! Try again!"); 
            }
        
            
        }
    }
}
