﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_CheckIf3rdDigitIs7
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            string strNumber = number.ToString();
            if(strNumber.Length<3)
            {
                throw new ArgumentException("The number you have entered does not met the requirements of this task");
            }
            if (strNumber[2] == '7')
            {
                Console.WriteLine("The third digit in this number is 7");
            }
            else
            {
                Console.WriteLine("The third digit in this number is different than 7");
            }
           
        }
    }
}
