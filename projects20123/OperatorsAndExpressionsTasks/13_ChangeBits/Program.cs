﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_ChangeBits
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Number must be from 1-100");
            int number = int.Parse(Console.ReadLine());
            bool isSimple = true;
            int counter = 0;
            if (number > 100 || number < 1)
            {
                throw new ArgumentException("Please, enter number between 1 and 100");
            }
            for (int i = 2; i < number; i++)
            {
                if (number % i == 0)
                {
                    counter++;
                    if(counter>0)
                    {
                        Console.WriteLine("The number you've entered is not prime");
                        return;
                    }
                }
            }
            Console.WriteLine("The number you've entered is prime");
            
        }
    }
}
