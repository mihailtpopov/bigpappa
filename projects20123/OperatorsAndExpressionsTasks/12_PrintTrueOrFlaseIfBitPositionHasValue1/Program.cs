﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_PrintTrueOrFlaseIfBitPositionHasValue1
{
    class Program
    {
        static void Main(string[] args)
        {
            int v = int.Parse(Console.ReadLine());
            string numberInBinary = Convert.ToString(v, 2);
            Console.WriteLine(numberInBinary);
            int p = int.Parse(Console.ReadLine());
            bool isBit1=false;
            if(p<=0)
            {
                throw new ArgumentException("Position must be a positive number!");
            }
            try
            {
               if(numberInBinary[p]==0)
               {
                   isBit1 = true;
               }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("v={0},p={1} -> {2}",v,p,isBit1);

            int number = int.Parse(Console.ReadLine());
            int position = int.Parse(Console.ReadLine());
            int i = 1;
            int mask = i << p;
            bool checkBit = (number & mask) != 0;
            Console.WriteLine("number={0},position={1} -> {2}",number,position,checkBit);
        }

    }
}
