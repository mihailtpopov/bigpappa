﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_CheckIfThirdBinaryDigitIs1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            string numberInBinary = Convert.ToString(number, 2);
            char thirdBit = numberInBinary[2];
            Console.WriteLine(numberInBinary);
            Console.WriteLine("The third bit of this number is " + thirdBit);
        }
    }
}
