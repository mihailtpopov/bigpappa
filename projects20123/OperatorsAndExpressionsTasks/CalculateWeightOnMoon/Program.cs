﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateWeightOnMoon
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your weight:");
            double weightOnEarth = double.Parse(Console.ReadLine());
            double weightOnMoon = weightOnEarth * 0.17;
            Console.WriteLine("Your weight on the moon is: " + weightOnMoon);
        }
    }
}
