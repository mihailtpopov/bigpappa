﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_DividingBy5And7WithoutRemainde
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            bool withoutRemainder = number % 5 == 0 && number % 7 == 0;
            if(withoutRemainder)
            {
                Console.WriteLine("The number can be divided by 5 and 7 without remainder");
            }
            else
            {
                Console.WriteLine("The number can not be divided by 5 and 7 without remainder");
            }

        }
    }
}
