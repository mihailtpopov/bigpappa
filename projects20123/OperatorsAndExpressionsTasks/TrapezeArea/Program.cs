﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrapezeArea
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program calculates a trapeze area");
            Console.WriteLine("Enter one side: ");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter other side: ");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("The the height: ");
            double h = double.Parse(Console.ReadLine());
            double trapezeArea = ((a + b) * h) / 2;
            Console.WriteLine("S=" + trapezeArea);

        }
    }
}
