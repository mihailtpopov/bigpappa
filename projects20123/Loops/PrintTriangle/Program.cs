﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintTriangle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter n to represent the length of the triangle: ");
            int n = int.Parse(Console.ReadLine());
            for(int row = 1;row<=n;row++)
            {
                for(int col = 1;col<=row;col++)
                {
                    Console.Write(col + " ");
                }
                Console.WriteLine();
            }

        }
    }
}
