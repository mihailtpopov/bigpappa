﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbersInGivenRange
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int divider = 2;
            for(int i = n;i<=m;i++)
            {
                bool prime = true;
                int sqrt = (int)Math.Sqrt(i);
                for(int j = divider;j<=sqrt;j++)
                {
                    if(i%j==0)
                    {
                        prime = false;
                        break;
                    }
                }
                if(prime)
                {
                    Console.Write(i + ", ");
                }
            }
        }
    }
}
