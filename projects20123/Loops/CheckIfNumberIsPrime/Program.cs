﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIfNumberIsPrime
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.Write("Enter positive number:");
            int number = int.Parse(Console.ReadLine());
            bool prime = true;
            int divider = 2;
            int maxDivider = (int)Math.Sqrt(number);
            while(prime && divider<=maxDivider)
            {
                if(number%divider==0)
                {
                    prime = false;
                }
                divider++;
            }
            Console.WriteLine("Prime?" + prime);
        }
    }
}
