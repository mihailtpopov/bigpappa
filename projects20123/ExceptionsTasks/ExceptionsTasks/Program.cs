﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionsTasks
{
    class Program
    {
        static uint GetPositiveNumber(string number)
        {
            uint intNumber=0;
            try
            {
                intNumber = uint.Parse(Console.ReadLine());
            }
            catch(FormatException)
            {
                Console.WriteLine("Invalid number!");
            }
            finally
            {
                Console.WriteLine("Good Bye!");
            }
            return intNumber;
        }
        static void Main(string[] args)
        {
            
        }
    }
}
