﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace PrintFileText
{
    class Program
    {
        static StringBuilder ReadFile(string fileName)
        {
            StringBuilder fileText = new StringBuilder();

            if(!File.Exists(fileName))
            {
                Console.WriteLine("The file {0} does not exists!",fileName);
                return fileText;
            }

            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    while (reader.EndOfStream)
                    {
                        fileText.Append(reader.ReadLine());
                    }
                    return fileText;
                }
            }
            catch(UnauthorizedAccessException)
            {
                Console.WriteLine("The file is open in another program! Please close it!");
            }
          
        }
        static void Main(string[] args)
        {
            string fileName = "WrongFile.txt";
            
        }
    }
}
