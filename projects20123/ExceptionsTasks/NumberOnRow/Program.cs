﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace NumberOnRow
{
    class Program
    {
        static void CheckForNumber(string fileName)
        {
            if(!File.Exists(fileName))
            {
                Console.WriteLine("The file {0} does not exist!",fileName);
                return;
            }
            
                using(StreamReader reader = new StreamReader(fileName))
                {
                    int countRows = 0;
                    while(reader.EndOfStream)
                    {
                        countRows++;
                        int number;
                        bool numberOnBoard=true;
                        string line = reader.ReadLine();
                        string[] lineArray = line.Split(' ');
                        for(int i=0;i<lineArray.Length;i++)
                        {
                             numberOnBoard = int.TryParse(lineArray[i], out number);
                            if(numberOnBoard)
                            {
                                break;
                            }
                        }
                        if(numberOnBoard == false)
                        {
                            throw new FormatException(string.Format("No number has been find on row {0} in the file {1}",
                                countRows, fileName));
                        }
                    }
                }
          }
        static void Main(string[] args)
        {
        }
    }
}
