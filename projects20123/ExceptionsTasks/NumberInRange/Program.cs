﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberInRange
{
    class Program
    {
        static int ReadNumber(int start, int end)
        {
            try
            {
                
                int number = int.Parse(Console.ReadLine());
                if(number>end || number<start)
                {
                    throw new ApplicationException(string.Format("The number must be between [{0}...{1}]",start,end));
                }
                return number;
            }
            catch(FormatException)
            {
                throw new FormatException("Invalid number");
            }
        }
        static List<int> GetAscendingSeq(int seqLength, int rangeStart, int rangeEnd)
        {
           int maxNum = int.MinValue;
           List<int> ascendingIntsInRange = new List<int>();
           for(int i = 0;i<seqLength;i++)
           {
               int num = ReadNumber(rangeStart, rangeEnd);
               if (num > maxNum)
               {
                   maxNum = num;
                   ascendingIntsInRange.Add(maxNum);
                   continue;
               }
                   throw new ApplicationException("The number must be larger than the previous one");
           }
           return ascendingIntsInRange;
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine("Numbres must be in the given range and every number must be larger than the previous one!");
            Console.WriteLine("Enter range: ");
            Console.Write("Beginning of the range: ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("End of the range: ");
            int end = int.Parse(Console.ReadLine());
            Console.Write("Enter sequence length: ");
            int length = int.Parse(Console.ReadLine());
            List<int> ascendingIntsInRange = GetAscendingSeq(length, start, end);
        }
    }
}
