﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Summary
{
    class AdjancyMatrixGraph
    {
        private int[,] vertices;
        public AdjancyMatrixGraph(int[,] vertices)
        {
            this.vertices = vertices;
        }

        public void AddEdge(int i, int j)
        {
            vertices[i, j] = 1;
        }
        public void RemoveEdge(int i, int j)
        {
            vertices[i, j] = 0;
        }
        public bool HasEdge(int i, int j)
        {
            bool hasEdge = vertices[i, j] == 1;
            return hasEdge;
        }
        public IList<int> GetSuccessors(int i)
        {
            IList<int> successors = new List<int>();
            for(int j = 0;j<vertices.GetLength(1);j++)
            {
                if(vertices[i,j]==1)
                {
                    successors.Add(j);
                }
            }
            return successors;
        }
    }
}
