﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesAndHashTables
{
    class RockLoversSortedSet
    {
        public static string GetCommaSeparatedList(IEnumerable<string> items)
        {
            string result = string.Empty;
            int i = 1;
            foreach(string item in items)
            {
                result = item + ", ";
                if(i%3==0) //3 elements on a line
                {
                    result += Environment.NewLine;
                }
                i++;
            }
            if(result!=string.Empty)
            {
                //removes the extra separators at the end
                result = result.TrimEnd(' ', ',', '\r', '\n');
            }
            return result;
        }
    }
}
