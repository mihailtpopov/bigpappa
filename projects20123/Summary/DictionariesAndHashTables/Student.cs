﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesAndHashTables
{
    class StudentComparer:IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Age.CompareTo(y.Age);
        }
    }
    class Student : IComparable<Student>
    {
        int age;
        string name;
        public int Age
        {
            get { return this.age; }
            set { this.age = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Student(string name, int age)
        {
            this.name = name;
            this.age = age;
        }
        public static void PrintStudentData(IDictionary<Student, int> students)
        {
            foreach(KeyValuePair<Student, int> student in students)
            {
                Console.WriteLine("{0} - {1}",student.Key.Name,student.Key.Age);
            }
        }

        public int CompareTo(Student other)
        {
          return this.Name.CompareTo(other.Name);
        }
    }
}
