﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesAndHashTables
{
    class SortedDictionaryFindWordNumber
    {
        public static IDictionary<string, int> GetWordOccurenceMap(string text)
        {
            string[] tokens = text.Split('.', ' ', ',', '-', '?', '!');
            IDictionary<string,int> wordOccurenceMap  = new SortedDictionary<string,int>();
            foreach(string word in tokens)
            {
                if(string.IsNullOrEmpty(word.Trim()))
                {
                    continue;
                }

                int count;
                if(!wordOccurenceMap.TryGetValue(word, out count))
                {
                    count = 0;
                }
                wordOccurenceMap[word] = count + 1;
            }
            return wordOccurenceMap;
        }
        public static void PrintWordOccurenceMap(IDictionary<string, int> wordOccurenceMap)
        {
            foreach(KeyValuePair<string, int> word in wordOccurenceMap)
            {
              if(word.Value>1)
              {
                  Console.WriteLine("The word {0} occurs {1} times in the text",word.Key,word.Value);
                  continue;
              }
              Console.WriteLine("The word {0} occurs {1} time in the text", word.Key, word.Value);
            }
        }
    }
}
