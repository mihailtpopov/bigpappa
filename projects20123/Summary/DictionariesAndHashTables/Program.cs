﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DictionariesAndHashTables;

namespace DictionariesAndHashTables
{
    class Program
    {
         private static readonly string TEXT =
            "She uchish li she bachkash li? Be kvo she bachkash " +
            "be? Tui vashto uchene li e? Ia po-hubavo opitai da " +
            "BACHKASH da se uchish malko! Uchish ne uchish trqbva " +
            "da bachkash!";
        static void Main(string[] args)
        {
            IDictionary<string, int> wordOccurenceMap;
            wordOccurenceMap = SortedDictionaryFindWordNumber.GetWordOccurenceMap(TEXT);
            SortedDictionaryFindWordNumber.PrintWordOccurenceMap(wordOccurenceMap);

            IDictionary<Student,int> students = new SortedDictionary<Student,int>(new StudentComparer());

            students.Add(new Student("Kolio", 20), 1);
            students.Add(new Student("Balduin", 18), 2);
            students.Add(new Student("Asparuh", 19), 3);
            students.Add(new Student("Akakii", 21), 4);
            students.Add(new Student("Evritii", 17), 5);

            Student.PrintStudentData(students);

            //HASHSET EXAMPLE
            HashSet<string> aspStudents = new HashSet<string>();
            aspStudents.Add("S. Nakov");
            aspStudents.Add("V. Kolev");
            aspStudents.Add("M. Popov");

            HashSet<string> silverlightStudents = new HashSet<string>();
            silverlightStudents.Add("S. Guthrie");
            silverlightStudents.Add("M. Popov");

            HashSet<string> allStudents = new HashSet<string>();
            allStudents.UnionWith(aspStudents);
            allStudents.UnionWith(silverlightStudents);

            HashSet<string> intersectStudents = new HashSet<string>();
            intersectStudents.IntersectWith(aspStudents);
            intersectStudents.IntersectWith(silverlightStudents);

            Console.WriteLine("ASP.NET students:");
            Console.WriteLine(StudentCoursesHashSet.GetListOfStudents(aspStudents));
            Console.WriteLine("Silverlight students:");
            Console.WriteLine(StudentCoursesHashSet.GetListOfStudents(aspStudents));
            Console.WriteLine("All students:");
            Console.WriteLine(StudentCoursesHashSet.GetListOfStudents(allStudents));
            Console.WriteLine("Intersect students");
            Console.WriteLine(StudentCoursesHashSet.GetListOfStudents(intersectStudents));


           //SORTEDSET EXAMPLE
            SortedSet<string> ivanchosBands = new SortedSet<string>()
        {
            "manowar", "blind guardian", "dio", "grave digger", "kiss",
            "dream theater", "manowar", "megadeth", "dream theater",
            "dio", "judas priest", "manowar", "kreator",
            "blind guardian", "iron maiden", "accept", "dream theater"
        };
            SortedSet<string> mariikasBands = new SortedSet<string>()
        {
            "iron maiden", "dio", "accept", "manowar", "slayer",
            "megadeth", "dio", "manowar", "running wild", "grave digger",
            "accept", "kiss", "manowar", "iron maiden", "manowar",
            "judas priest", "iced earth", "manowar", "dio",
            "iron maiden", "manowar", "slayer"
        };

            Console.WriteLine("Ivancho likes these bands:");
            Console.WriteLine(RockLoversSortedSet.GetCommaSeparatedList(ivanchosBands));
            Console.WriteLine();

            Console.WriteLine("Mariika likes these bands:");
            Console.WriteLine(RockLoversSortedSet.GetCommaSeparatedList(mariikasBands));
            Console.WriteLine();

            SortedSet<string> intersectBands = new SortedSet<string>();
            intersectBands.UnionWith(ivanchosBands);
            intersectBands.IntersectWith(mariikasBands);
            Console.WriteLine("Does Ivancho like Mariika? - {0}", intersectBands.Count>5?"Yes":"No");
            Console.WriteLine("Because Ivancho and Mariika both like: ");
            Console.WriteLine(RockLoversSortedSet.GetCommaSeparatedList(intersectBands));
            Console.WriteLine();

            SortedSet<string> unionBands = new SortedSet<string>();
            unionBands.UnionWith(ivanchosBands);
            unionBands.UnionWith(mariikasBands);
            Console.WriteLine("All bands that both Ivancho and Mariika like");
            Console.WriteLine(RockLoversSortedSet.GetCommaSeparatedList(unionBands));

        }
    }
}
