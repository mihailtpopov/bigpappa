﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesAndHashTables
{
    class StudentCoursesHashSet
    {
       public static string GetListOfStudents(IEnumerable<string> students)
        {
            string result = string.Empty;
           foreach(string student in students)
           {
               result = student + ", ";
           }
           if(result!=string.Empty)
           {
               //remove the extra separator at the end
               result = result.TrimEnd(' ', ',','\n');
           }
           return result;
        }
    }
}
