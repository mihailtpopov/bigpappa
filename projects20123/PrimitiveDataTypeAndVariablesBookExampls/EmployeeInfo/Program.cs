﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = Console.ReadLine();
            string familyName = Console.ReadLine() ;
            byte age = byte.Parse(Console.ReadLine());
            char gender = char.Parse(Console.ReadLine());
            if(gender!='m' || gender!='f')
            {
                throw new ArgumentException("Invalid gender!Please try again!");
            }
            int identityNumber = int.Parse(Console.ReadLine());
            if(identityNumber<27560000 || identityNumber>27569999)
            {
                throw new ArgumentException("Invalid employee identity number! Please, try again!");
            }
        }
    }
}
