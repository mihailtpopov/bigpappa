﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableDataType
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 5;
            int? ni = i;
            Console.WriteLine(ni); //5
            Console.WriteLine(ni.HasValue);//True
            i = ni.Value;
            Console.WriteLine(i);//5
            ni = null;
            Console.WriteLine(ni.HasValue);//False
            i = ni.GetValueOrDefault();
            Console.WriteLine(i);//0

        }
    }
}
