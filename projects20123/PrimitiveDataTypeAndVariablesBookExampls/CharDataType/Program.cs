﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharDataType
{
    class Program
    {
        static void Main(string[] args)
        {
            char symbol = 'a';
            Console.WriteLine("The code of " + symbol + " is " + (int)symbol);
            char symbol2 = 'b';
            Console.WriteLine("The code of " + symbol2 + " is " + (int)symbol2);
            char symbol3 = 'A';
            Console.WriteLine("The code of " + symbol3 + " is " + (int)symbol3);
        }
    }
}
