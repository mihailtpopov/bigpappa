﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MistakesCalculatingFloatingDataTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            float fnumber = 0.1f;
            Console.WriteLine(fnumber); //0.1 correct due to rounding
            double dnumber = 0.1;
            Console.WriteLine(dnumber);//incorrect
            float f = 1f / 3;
            Console.WriteLine(f);//correct due to rounding
            double d = f;
            Console.WriteLine(d);//incorrect
        }
    }
}
