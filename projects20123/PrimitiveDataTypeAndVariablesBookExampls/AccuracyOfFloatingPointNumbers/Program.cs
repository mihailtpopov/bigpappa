﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccuracyOfFloatingPointNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            float floatPI = 3.141592653589793238f;
            double doublePI = 3.141592653589793238;
            Console.WriteLine("Float PI is: " + floatPI);
            Console.WriteLine("Double PI is: " + doublePI);

        }
    }
}
