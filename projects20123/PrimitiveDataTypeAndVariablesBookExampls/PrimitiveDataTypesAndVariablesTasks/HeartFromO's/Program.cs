﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeartFromO_s
{
    class Program
    {
        static void Main(string[] args)
        {
            char symbol = 'o';
            Console.WriteLine("      o o       o o");
            Console.WriteLine("    o     o   o     o");
            Console.WriteLine("   o       o o       o");
            Console.WriteLine("  o                   o");
            Console.WriteLine("   o                 o");
            Console.WriteLine("    o               o");
            Console.WriteLine("     o             o");
            Console.WriteLine("      o           o");
            Console.WriteLine("       o         o");
            Console.WriteLine("        o       o");
            Console.WriteLine("         o     o");
            Console.WriteLine("          o   o");
            Console.WriteLine("            o");
        }
    }
}
