﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChangeValues
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstNumber;
            int secondNumber;
            firstNumber = 5;
            secondNumber = 10;
            int temp;
            temp = firstNumber;
            firstNumber = secondNumber;
            secondNumber = temp;
            Console.WriteLine("The first number was 5 and now is " + firstNumber );
            Console.WriteLine("The second number was 10 and now is " + secondNumber);
        }
    }
}
