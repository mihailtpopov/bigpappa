﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoundOn7thDigit
{
    class Program
    {
        static void Main(string[] args)
        { 
            decimal realNumber = decimal.Parse(Console.ReadLine());
            decimal roundedDecimal = Math.Round(realNumber,6);
            Console.WriteLine("The number rounded on 7th digit is " + roundedDecimal);

        }
    }
}
