﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldTypeCasting
{
    class Program
    {
        static void Main(string[] args)
        {
            string hello = "Hello";
            string world = "World";
            object helloWorld = hello + " " + world;
            string stringHelloWorld = (string)helloWorld;
            Console.WriteLine(stringHelloWorld);

            //double y = 123.81;
           // int x;
           // x = (int)y;
           // Console.WriteLine(x);

        }
    }
}
