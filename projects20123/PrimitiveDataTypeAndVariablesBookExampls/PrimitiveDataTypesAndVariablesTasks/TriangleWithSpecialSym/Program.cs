﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleWithSpecialSym
{
    class Program
    {
        static void Main(string[] args)
        {
            char s = '\u00A9';
            Console.WriteLine("         " +s);
            Console.WriteLine("        "+s+" "+s);
            Console.WriteLine("       "+s+"   "+s);
            Console.WriteLine("      "+s+" "+s+" "+s+" "+s);
        }
    }
}
