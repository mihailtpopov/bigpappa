﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuotedString
{
    class Program
    {
        static void Main(string[] args)
        {
            string quotedString;
            string notQuotedString;
            quotedString = "The \"use\" of quoted string causes difficulties";
            notQuotedString = @"The ""use"" of quoted string causes difficulties";
            Console.WriteLine(quotedString);
            Console.WriteLine(notQuotedString);
        }
    }
}
