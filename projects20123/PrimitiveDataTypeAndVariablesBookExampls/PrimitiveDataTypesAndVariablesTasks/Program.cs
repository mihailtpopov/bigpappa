﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimitiveDataTypesAndVariablesTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            ushort numberUShort = 52130;
            sbyte numberSByte = -115;
            int numberInt = 4825932;
            sbyte numberSByte1 = 97;
            short numberShort = -10000;
            short numberShort1 = 20000;
            byte numberByte = 224;
            int numberInt1 = 970700000;
            sbyte numberSByte2 = 112;
            sbyte numberSByte3 = -44;
            int numberInt2 = -1000000;
            short numberShort2 = 1990;
            long numberLong = 123456789123456789;

        }
    }
}
