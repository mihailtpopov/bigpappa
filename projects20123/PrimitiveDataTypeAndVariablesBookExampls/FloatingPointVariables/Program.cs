﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatingPointVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            float floatPI = 3.14f;
            Console.WriteLine(floatPI);
            double doublePI = 3.14;
            Console.WriteLine(doublePI);
            double nan = double.NaN;
            Console.WriteLine(nan);
            float infinity = float.PositiveInfinity;
            Console.WriteLine(infinity);
        }
    }
}
