﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringDataType
{
    class Program
    {
        static void Main(string[] args)
        {
            string firstName = "Ivan";
            string familyName = "Ivanov";
            string fullName = firstName + " " + familyName;
            Console.WriteLine("Hello " + firstName + "!");
            Console.WriteLine("Your full name is: " + fullName);

        }
    }
}
