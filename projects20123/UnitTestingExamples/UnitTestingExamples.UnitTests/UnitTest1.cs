﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestingExamples;

namespace UnitTestingExamples.UnitTests
{
    [TestClass]
    public class PersonTests
    {
        [TestMethod]
        public void GreetPerson_CapitalLetter_ReturnsTrue()
        {
            var person = new Person("akakii");
            bool isUpper = char.IsUpper(person.name[0]);
            Assert.IsTrue(isUpper);
        }
    }
}
