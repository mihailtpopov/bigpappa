﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertingToString
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 7;
            string correct = "Sum" + (a + b);
            Console.WriteLine(correct);
            string incorrect = "Sum" + a + b;
            Console.WriteLine(incorrect);
            Console.WriteLine("Perimeter" + 2*(a+b));
        }
    }
}
