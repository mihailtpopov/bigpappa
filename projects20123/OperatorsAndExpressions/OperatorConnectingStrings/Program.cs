﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorConnectingStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string csharp = "C#";
            string dotnet = ".NET";
            string csharpDotnet = csharp + dotnet;
            Console.WriteLine(csharpDotnet);
            int number = 4;
            string csharpDotnet4 = csharpDotnet + " " + number;
            Console.WriteLine(csharpDotnet4);

        }
    }
}
