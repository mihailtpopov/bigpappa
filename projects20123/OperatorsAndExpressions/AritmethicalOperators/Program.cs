﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AritmethicalOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            double squarePerimeter = 17;
            double squareSide = squarePerimeter / 4.0; //4,25
            double squareArea = squareSide * squareSide; //18,0625
            Console.WriteLine(squareSide);
            Console.WriteLine(squareArea);
            int b = 4;
            int a = 5;
            Console.WriteLine(a+b); //9
            Console.WriteLine(a+b++); //9
            Console.WriteLine(a+b); //10
            Console.WriteLine(a + (++b)); //11
            Console.WriteLine(a+b); //11
            Console.WriteLine(14/a); //2
            Console.WriteLine(14%a); //4

            int one = 1;
            int zero = 0;
            Console.WriteLine(one/zero);

            double dMinusOne = -1.0;
            double dZero = 0.0;
            Console.WriteLine(dMinusOne/dZero); // -Infinity
            Console.WriteLine(one/dZero); //Infinity

        }
    }
}
