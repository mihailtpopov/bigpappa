﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparingOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 5; int y = 10;
            Console.WriteLine("x>y - " + (x>y));
            Console.WriteLine("x<y - " + (x<y));
            Console.WriteLine("x<=y - " + (x<=y));
            Console.WriteLine("x>=y - " + (x>=y));
            Console.WriteLine("x==y - " + (x==y));
            Console.WriteLine("x!=y - " + (x!=y));

        }
    }
}
