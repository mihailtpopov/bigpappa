﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicalOpeators
{
    class Program
    {
        static void Main(string[] args)
        {
            bool a = true;
            bool b = false;
            Console.WriteLine(a&&b); //false
            Console.WriteLine(a||b); //true
            Console.WriteLine(!a); //false
            Console.WriteLine((5>7)^(a==b)); // false
        }
    }
}
