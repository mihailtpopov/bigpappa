﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplicitTypeCasting
{
    class Program
    {
        static void Main(string[] args)
        {
            int myInt = 5;
            Console.WriteLine(myInt); //5
            long myLong = myInt;
            Console.WriteLine(myLong); //5
            Console.WriteLine(myLong+myInt); //10
        }
    }
}
