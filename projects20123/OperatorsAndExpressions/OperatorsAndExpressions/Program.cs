﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsAndExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 7 + 9;
            Console.WriteLine(a);
            string firstName = "Mihail";
                string secondName = "Popov";
                string fullName = firstName + " " + secondName;
                Console.WriteLine(fullName);
        }
    }
}
