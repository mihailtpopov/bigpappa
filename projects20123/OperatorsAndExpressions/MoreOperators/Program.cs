﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            // Operator .
            Console.WriteLine(DateTime.Now);
            //Operator [] -
            int[] arr = { 1, 2, 3 };
            Console.WriteLine(arr[0]); //1
            string str = "Hello";
            Console.WriteLine(str[1]); //e
            //Operator ?? returns the left side only if its not null
            int? a = 2;
            Console.WriteLine(a??-1);//2
            string noName = null;
            Console.WriteLine(noName??"no name");
            //Operator ''is'' shows if a given object is suitable with data type
            string s = "Beer";
            Console.WriteLine(s is string);
            string notNullString = s;
            string nullString = null;
            Console.WriteLine(nullString??"Unspecified");
            Console.WriteLine(notNullString??"Specified");
        }
    }
}
