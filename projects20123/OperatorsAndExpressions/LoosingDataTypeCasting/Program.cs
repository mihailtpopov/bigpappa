﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoosingDataTypeCasting
{
    class Program
    {
        static void Main(string[] args)
        {
            long myLong = long.MaxValue;
            int myInt = (int)myLong;
            Console.WriteLine(myLong);
            Console.WriteLine(myInt);


            float heightInMeters = 1.74f;
            double maxHeight = heightInMeters; //Implicit
            double minHeight = (double)heightInMeters; //Explicit 
            float actualHeight = (float)maxHeight; //Explicit
            // float maxHeightFloat = maxHeight; //Complication error!

        }
    }
}
