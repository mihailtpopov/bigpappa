﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = ++a;
            Console.WriteLine(a);
            Console.WriteLine(b);
            b = ++a;
            Console.WriteLine(a);
            Console.WriteLine(b);

            double d = 1/2;
            Console.WriteLine(d);
            d = 1.0 / 2;
            Console.WriteLine(d);

            float negInfinity = float.NegativeInfinity;
            Console.WriteLine(negInfinity);

            int num = 1;
            double dZero = 0;
            int intZero = (int)dZero;
            Console.WriteLine(num/dZero); //infinity
            Console.WriteLine(dZero/intZero); //NaN
         //   Console.WriteLine(num/intZero); //Exception
         //   Console.WriteLine(intZero/intZero);  //Exception

            double incorrect = (double)((1 + 2) / 4);
            double correct = ((double)(1+2))/4;
            Console.WriteLine(incorrect);
            Console.WriteLine(correct);

            Console.WriteLine("2+3=" + 2 + 3); //23
            Console.WriteLine("2+3=" + (2+3)); //5
        }
    }
}
