﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExplicitTypeCasting
{
    class Program
    {
        static void Main(string[] args)
        {
            double myDouble = 5.1d;
            Console.WriteLine(myDouble); //5.1
            long myLong = (long)myDouble;
            Console.WriteLine(myLong); //5
            myDouble = 5e9d;
            Console.WriteLine(myDouble); //5000000000
            int myInt = (int)myDouble;
            Console.WriteLine(myInt);
            Console.WriteLine(int.MinValue);

        }
    }
}
