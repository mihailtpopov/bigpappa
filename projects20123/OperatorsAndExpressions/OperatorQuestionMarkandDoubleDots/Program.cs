﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorQuestionMarkandDoubleDots
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 4;
            int y = 6;
            Console.WriteLine((x>y)?"x>y":"x<y"); //displayed message x<y
            int num = x == y ? 1 : -1; //num will have value -1
        }
    }
}
