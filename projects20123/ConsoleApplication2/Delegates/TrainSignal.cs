﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    //ObserverDesignPattern
    class TrainSignal
    {
        public Action TrainsAComing;
        public void HereComesATrain()
        {
            TrainsAComing();
        }
    }
    class Car
    {
        public Car(TrainSignal trainSignal)
        {
            trainSignal.TrainsAComing += StopTheCar;
        }
        void StopTheCar()
        {
            Console.WriteLine("Stoooooooooooooooop");
        }
    }
}
