﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    public delegate void SomeDelegate();
    class Program
    {
        static void Main(string[] args)
        {
            SomeDelegate del = new SomeDelegate(SomeMethod1);
            del += SomeMethod2;
            del += SomeMethod3;
            del();

            EmployeeDelegateExample employee1 = new EmployeeDelegateExample("John", "1231bs21", 5, 2000);
            EmployeeDelegateExample employee2 = new EmployeeDelegateExample("Andrei", "1231bs21", 4, 2000);
            EmployeeDelegateExample employee3 = new EmployeeDelegateExample("Bill", "1231bs21", 6, 2000);
            EmployeeDelegateExample employee4 = new EmployeeDelegateExample("Rob", "1231bs21", 3, 2000);

            EmployeeDelegateExample.PromoteEmployee(EmployeeDelegateExample.EmployeeList, emp => emp.Experience >= 5);

            Func<EmployeeDelegateExample,bool> someFunc = new Func<EmployeeDelegateExample,bool>(emp=>emp.Experience>5);
            EmployeeDelegateExample.PromoteEmployee(EmployeeDelegateExample.EmployeeList, someFunc); 
        }
        public static void SomeMethod1()
        {
            Console.WriteLine("SomeMethod1");
        }
        public static void SomeMethod2()
        {
            Console.WriteLine("SomeMethod2");
        }
        public static void SomeMethod3()
        {
            Console.WriteLine("SomeMethod3");
        }
    }

}
