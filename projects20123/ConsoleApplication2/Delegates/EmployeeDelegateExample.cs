﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    
    class EmployeeDelegateExample
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string ID;
        private int experience;

        public int Experience
        {
            get { return experience; }
            set { experience = value; }
        }
        private int salary;
        private static List<EmployeeDelegateExample> employeeList = new List<EmployeeDelegateExample>();
        public static List<EmployeeDelegateExample> EmployeeList
        {
            get { return employeeList; }
            set { employeeList = value; }
        }
        
        public EmployeeDelegateExample(string name, string ID, int experience, int salary)
        {
            this.name = name;
            this.ID = ID;
            this.experience = experience;
            this.salary = salary;
            employeeList.Add(this);
        }
        
        public static void PromoteEmployee(List<EmployeeDelegateExample> empList, Func<EmployeeDelegateExample,bool> eligibleToPromote)
        {
           foreach(EmployeeDelegateExample employee in empList)
           {
               if(eligibleToPromote(employee))
               {
                   Console.WriteLine("{0} is promoted!",employee.Name);
               }
           }
        }
    }
}
