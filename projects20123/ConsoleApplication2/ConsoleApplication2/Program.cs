﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    interface I1
    {
        void InterfaceMethod()
        {
            Console.WriteLine("I1 Interface method");
        }
    }
    interface I2
    {
        void InterfaceMethod()
        {
            Console.WriteLine("I2 Interface method");
        }
    }
    class Program : I1,I2
    {
        static void Main(string[] args)
        {


            try
            {
                int interestRate = int.Parse(Console.ReadLine());
            }
            catch(FormatException fe)
            {
                throw new InterestCalculationException("Въвели сте грешен лихвен процент.", fe);
            }
            Program p = new Program();
            ((I1)p).InterfaceMethod();
        }

        void I1.InterfaceMethod()
        {
            throw new NotImplementedException();
        }
    }
}
