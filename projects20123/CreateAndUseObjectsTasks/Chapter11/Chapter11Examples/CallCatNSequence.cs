﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chapter11.Chapter11Examples
{
    class CallCatNSequence
    {
        public static void CallBothClasses(Cat cat, Sequence sequence)
        {
            Cat.SayMiau();
        }
    }
}
