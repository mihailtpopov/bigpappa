﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chapter11.Chapter11Examples;

namespace Chapter11
{
   class Cat
    {
       private string name;
       public string Name
       {
           get { return name; }
           set { this.name = value; }
       }
       public Cat(string name)
       {
           this.name = name;
       }
        public static string SayMiau()
        {
           string miau = "Miaaaaaau";
           return miau;
        }
    }
    class Sequence
    {
        public static void PrintSequence(int number)
        {
            for(int i = 0;i<number;i++)
            {
                Console.WriteLine(i);
            }
        }
    }

    class Program
    {

        static void Main(string[] args)
        {
            CallCatNSequence.CallBothClasses(new Cat("Hasan"), new Sequence());
            Cat cat1 = new Cat("Kolio");
            Cat cat2 = new Cat("Hasan");
            Cat cat3 = new Cat("Avram");
            Cat cat4 = new Cat("Akakii");
            Cat cat5 = new Cat("Adem");
            Cat cat6 = new Cat("Petio");
            Cat cat7 = new Cat("Goshko");
            Cat cat8 = new Cat("Misho");
            Cat cat9 = new Cat("Choshko");
            Cat cat10 = new Cat("Mancho");
            Console.WriteLine("Cat {0} said: {1}", cat1.Name,Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat2.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat3.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat4.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat5.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat6.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat7.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat8.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat9.Name, Cat.SayMiau());
            Console.WriteLine("Cat {0} said: {1}", cat10.Name, Cat.SayMiau());

        }
    }

}
