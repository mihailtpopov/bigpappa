﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomAdvertisingMessageGenerator
{
    class AdvertisingMessage
    {
        static string[] greetingPhrases = { "Продуктът е отличен.", "Това е страхотен продукт.",
                               "Постоянно ползвам този продукт.", "Това е най-добрият продукт от тази категория."};
        static string[] greetingsOutcomes = { "Вече се чувствам добре.", "Успях да се променя.", "Той направи чудо.",
                         "Не мога да повярвам, но вече се чувствам страхотно.", "Опитайте и вие. Аз съм много доволна."};
        static string[] authorsName = { "Диана", "Петя", "Стела", "Елена", "Катя" };
        static string[] authorsSecondName = { "Иванова", "Петрова", "Кирова" };
        static string[] authorsCity = { "София", "Пловдив", "Варна", "Русе", "Бургас" };

        public static void PrintRandomAdvertisingMessage()
        {
            Random phrase = new Random();
            Random outcome = new Random();
            Random authorName = new Random();
            Random authorCity = new Random();
            Random authorSecondName = new Random();

            int rndPhrase  = phrase.Next(greetingPhrases.Length - 1);
            int rndOutcome = outcome.Next(greetingsOutcomes.Length - 1);
            int rndAuthor = authorName.Next(authorsName.Length - 1);
            int rndAuthorCity =  authorCity.Next(authorsCity.Length - 1);
            int rndAuthorSecName = authorSecondName.Next(authorsSecondName.Length - 1);

            Console.WriteLine("{0} {1} - {2} {3}, {4}",greetingPhrases[rndPhrase],greetingsOutcomes[rndOutcome],
                authorsName[rndAuthor],authorsSecondName[rndAuthorSecName], authorsCity[rndAuthorCity]);
        }

        static void Main(string[] args)
        {
            PrintRandomAdvertisingMessage();
        }
    }
}
