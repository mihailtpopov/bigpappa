﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_SumStringNumbers
{
    class StringNumbers
    {
        private string numbersSeq;
        string NumbersToSum
        {
            get
            {
                return numbersSeq;
            }
            set
             {
                this.numbersSeq=value;
             }
        }
        public StringNumbers(string numbersSeq)
        {
            this.numbersSeq = numbersSeq;
        }
        
       
        
         public static int GetSum(string stringNumbers)
        {
            int sum = 0;
             List<int> intList = new List<int>();
             string[] numbers = stringNumbers.Split(' ');
            for(int i = 0;i<numbers.Length;i++)
            {
                intList.Add(int.Parse(numbers[i]));
                sum += intList[i];
            }
            return sum;
        }
        static void Main(string[] args)
        {

            StringNumbers firstNumSeq = new StringNumbers("48 15 23 0 2");
            int firstNumSeqSum = GetSum(firstNumSeq.NumbersToSum);
            Console.WriteLine(firstNumSeqSum);
        }
    }
}
