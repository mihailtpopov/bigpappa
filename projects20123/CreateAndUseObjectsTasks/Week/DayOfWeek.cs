﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week
{
    class DayOfWeek
    {
        private string name;
        string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public DayOfWeek(string name)
        {
            this.name = name;
        }
        static void Main(string[] args)
        {
            DayOfWeek day = new DayOfWeek("Wednesday");
            Console.WriteLine("Today is: " + day.Name);
        }
    }
}
