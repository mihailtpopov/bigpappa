﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondFigure
{
    class Triangle
    {
        public static double Area(double firstSide, double secondSide, double thirdSide)
        {
            double halfPerimeter = (firstSide + secondSide + thirdSide) / 2;
            double area = Math.Sqrt(halfPerimeter*(halfPerimeter-firstSide)*(halfPerimeter-secondSide)*(halfPerimeter-thirdSide));
            return area;
        }
        public static double Area(double side,double sideHeigth)
        {
            double area = (side * sideHeigth) * 0.5;
            return area;
        }
        public static double Area(double firstSide, double secondSide, double angleInDegrees)
        {
            double angleInRadians = angleInDegrees * Math.PI / 180;
            double area = (firstSide * secondSide * Math.Sin(angleInRadians)) * 0.5;
            return area;
        }
        static void Main(string[] args)
        {
        }
    }
}
