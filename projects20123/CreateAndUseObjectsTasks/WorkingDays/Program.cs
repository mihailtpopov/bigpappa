﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingDays
{
    class Program
    {
        public static int WeekendsDays(DateTime firstDate, DateTime secondTime)
        {
            int counter  = 0;
           for(DateTime i = firstDate.Date;i<secondTime.Date;i.AddDays(1.0))
           {
               if(i.DayOfWeek.ToString()=="Saturday" || i.DayOfWeek.ToString()=="Sunday")
               {
                   counter++;
               }
           }
           return counter;
        }
        static void Main(string[] args)
        {
            WeekendsDays(new DateTime(2010,2,28), new DateTime(2010,3,7));
               
        }
    }
}
