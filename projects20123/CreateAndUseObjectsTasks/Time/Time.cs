﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Time
{
    class Time
    {
        static void TimeElapsed(double secondsElapsed)
        {
            int minutesElapsed=0;
            int hoursElapsed=0;
            if(secondsElapsed/60>0)
            {
                minutesElapsed=(int)secondsElapsed/60;
                secondsElapsed=secondsElapsed%60;
            }
            if(minutesElapsed/60>0)
            {
               hoursElapsed=minutesElapsed/60;
                minutesElapsed=minutesElapsed%60;
            }
            Console.WriteLine("Time elapsed: {0} seconds, {1} minutes and {2} hours",secondsElapsed,minutesElapsed,hoursElapsed);
        }
        static void Main(string[] args)
        {
            int sum = 0;
            int startTime = Environment.TickCount;
            for(BigInteger i = 0;i<1000000000000000;i++)
            {
                sum++;
            }
            int endTime = Environment.TickCount;
            double secondsElapsed = (endTime - startTime) / 1000.0;

            TimeElapsed(secondsElapsed);
        }
    }
}
