﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateAndUseObjectsTasks
{
    class Year
    {
        private int year;
        int Year
        {
            get { return this.year;}
            set { this.year = value; }
        }
        private Year(int year)
        {
            this.year = year;
        }
        static bool IfIsIntercalary(int year)
        {
            if(year%4==0)
            {
                if(year%100!=0)
                {
                    return true;
                }
                else
                {
                    if(year%400==0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a year:");
            short year = short.Parse(Console.ReadLine());
            bool isIntercalary = IfIsIntercalary(year);
            Console.WriteLine("Year is intercalary?: {0}",isIntercalary);
        }
    }
}
