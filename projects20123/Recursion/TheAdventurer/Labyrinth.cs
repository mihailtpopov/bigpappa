﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheAdventurer
{
    class Labyrinth
    {
        private static int[,] labyrinth = 
        { 
           {' ',' ',' ','*',' ',' ',' '},
           {'*','*',' ','*',' ','*',' '},
           {' ',' ',' ',' ',' ',' ',' '},
           {' ','*','*','*','*','*',' '},
           {' ',' ',' ',' ',' ',' ','e'},
        };
        public static void FindPath(int row, int col)
        {
            if((col<0) || (row<0) || (row>=labyrinth.GetLength(0)) || (col>=labyrinth.GetLength(1)))
            {
                //We are out of the labyrinth
                return;
            }

            if (labyrinth[row, col] == 'e')
            {
                //Check if we have found the exit
                Console.WriteLine("We are out of the labyrinth!");
            }

            if(labyrinth[row,col] != ' ')
            {
                //The current cell is not free
                return;
            }
            
            labyrinth[row, col] = 's';

            FindPath(row, col - 1); //left
            FindPath(row - 1, col); //up
            FindPath(row, col + 1); //right
            FindPath(row + 1, col); //down

            //Mark back the current cell as free
            labyrinth[row, col] = ' ';
        }
    }
}
