﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursion
{
    class Program
    {
        public static float Power(float num, int powerOf)
        {
            if(powerOf==1)
            {
                return num;
            }
            return num * Power(num, powerOf - 1);
        }
        //Power(2,3) = 2*Power(2,2)
        //Power(2,2) = 2*Power(2,1)
        //Power(2,1) = 2;
        static void Main(string[] args)
        {
            float power = Power(2, 20);
            Console.WriteLine(power);
        }
    }
}
