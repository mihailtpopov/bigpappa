﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class GenericClass<T>
    {
      private T[] elements;
      private static int numberElements;
      private int usedPlaces;
      public GenericClass(int arrayCapacity)
      {
          elements = new T[arrayCapacity];
      }
        public void AddElement(T element)
      {
            if(numberElements==elements.Length-1)
            {
                Array.Resize<T>(ref this.elements, elements.Length * 2);
            }
            if(elements[numberElements].Equals(default(T)))
            {
                elements[numberElements] = element;
            }
            else
            {
                elements[numberElements + 1] = element;
            }
          numberElements++;
          this.usedPlaces++;
      }
        public T GetElement(int index)
        {
            if(index<0 || index>elements.Length-1)
            {
                throw new IndexOutOfRangeException("Index must be in the range of the array!");
            }
            return this.elements[index];
        }
        public void RemoveElement(int index)
        {
            for(int i = index;i<this.usedPlaces-1;i++)
            {
                elements[i] = elements[i + 1];
            }
            this.elements[this.usedPlaces - 1] = default(T);
            this.usedPlaces--;
        }
        public void InsertElement(T element, int index)
        {
            if(this.usedPlaces==this.elements.Length)
            {
                throw new ArgumentException("The list is on full capacity! You can't insert an element!");
            }
            if(this.elements[index]!=null)
            {
                for(int i = index;i<this.usedPlaces-1;i++)
                {
                    this.elements[i] = this.elements[i + 1];
                }
                this.elements[index] = element;
            }
            else
            {
                this.elements[index] = element;
            }
        }
        public void ClearArray()
        {
            this.elements = null;
        }
        //public T FindElement(T value)
        //{
          //  T occurence = Array.Find(elements, x => x = value);
       // }
    }
}
