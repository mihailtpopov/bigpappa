﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            GenericClass<int> intElements = new GenericClass<int>(3);
            intElements.AddElement(1);
            intElements.AddElement(5);
            intElements.InsertElement(5, 2);
            intElements.AddElement(6);
            int element = intElements.GetElement(3);
            int element2 = intElements.GetElement(0);
            int element3 = intElements.GetElement(1);
            Console.WriteLine(element);
            Console.WriteLine(element2);
            Console.WriteLine(element3);
           
        }
    }
}
