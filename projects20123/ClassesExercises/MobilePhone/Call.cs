﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhone
{
    class Call
    {
        private DateTime callDate;
        private TimeSpan callStartTime;
        private TimeSpan callLength;
        private static List<Call> callHistory;

        public static List<Call> CallHistory
        {
            get { return callHistory; }
            set { callHistory = value; }
        }
        public DateTime CallDate
        {
            get { return this.callDate; }
            set { this.callDate = value; }
        }
        public TimeSpan CallStartTime
        {
            get { return this.callStartTime; }
            set { this.callStartTime = value; }
        }
        public TimeSpan CallLength
        {
            get { return this.callLength; }
            set { this.callLength = value; }
        }
        public Call(DateTime callDate, TimeSpan callStartTime, TimeSpan callLength)
        {
            this.callDate = callDate;
            this.callStartTime = callStartTime;
            this.callLength = callLength;
        }
        public double GetCallMinutes()
        {
           double totalMinutes = this.CallLength.TotalMinutes;
           return totalMinutes;
        }
        public double GetCallPrice()
        {
            double callPrice = this.CallLength.Minutes*0.37;
            return callPrice;
        }
        public static List<Call> RemoveLongestCall(List<Call> callHistory)
        {
            Call longestCall = null;
            foreach(Call call in callHistory)
            {
                int longestMinutes = call.CallLength.Minutes;
                int maxMinutes = int.MinValue;
                if(longestMinutes>maxMinutes)
                {
                    maxMinutes = longestMinutes;
                    longestCall =  call;
                }
            }
            callHistory.Remove(longestCall);
            return callHistory;
        }
    }
}
        