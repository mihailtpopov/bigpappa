﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhone
{
    public class SmartPhone
    {
        public enum PhoneModel
        {
            SamsungGalaxyS9, SamsungGalaxyNote9, OnePlus6, LG_G8_ThinQ, Nokia95
        }
        private string vendor;
        private static SmartPhone nokia95 = new SmartPhone("Nokia", PhoneModel.Nokia95, 159.99, "Mihail Popov");
        public PhoneModel model;
        private double price;
        private string owner;
        private Screen screen;
        private Battery battery;
        public SmartPhone(string vendor, PhoneModel model, double price) : this(vendor,model,price,"unknown")
        {
        }
        public SmartPhone(string vendor, PhoneModel model, double price, string owner)
        {
            this.vendor = vendor;
            this.model = model;
            this.price = price;
            this.owner = owner;
            this.screen = new Screen();
            this.battery = new Battery(Battery.BatteryType.LiIon, 99, 22);
        }
        public string Vendor
        {
            get { return vendor; }
            set { this.vendor = value; }
        }
         public PhoneModel Model
        {
            get { return model; }
            set { model = value; }
        }
        public double Price
        {
            get { return price; }
            set { 
                   if(price<=0)
                   {
                    throw new ArgumentException("Price, unfortunately can't be negative! Try again!");
                   }
                     this.price = value; 
                }
        }
        public string Owner
        {
            get { return owner; }
            set { 
                   if(owner.IndexOf(' ')==-1)
                   {
                       throw new ArgumentException("You must enter both your first and family name!");
                   }
                   this.owner = value; 
                }
        }
        
        public static void Nokia95Info()
        {
            Console.WriteLine("The wonderful {0} has extremely useful features for ONLY {1}.\n The winner of the phone"
             +"is {2}!!! Congratulations!!!",nokia95.model,nokia95.price,nokia95.owner);
        }
        public string Info()
        {
            string info = String.Format("The amazing {0} has extremely useful features for only {1}."
                + "The new owner is {2}. Congratulations!", this.model, this.price, this.owner);
            return info;
        }

        public void AddCall(Call call)
        {
            Call.CallHistory.Add(call);
        }
        public void RemoveCall(Call call)
        {
            Call.CallHistory.Remove(call);
        }
        public void DeleteCallHistory()
        {
            Call.CallHistory.Clear();
        }
        public double CalculateAllCallsPrice(List<Call> callHistory)
        {
            foreach(Call call in callHistory)
            {

            }
        }
    }
}
