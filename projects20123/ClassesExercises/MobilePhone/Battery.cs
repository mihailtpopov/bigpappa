﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhone
{
   public class Battery
    {
       public enum BatteryType
       {
           LiIon, LiPoly
       }
        private BatteryType type;
        private string model;
        private double hoursTalk;
        private double idleTime;

        public BatteryType Type
        {
            get { return type; }
            set { this.type = value; }
        }
           public string Model
        {
            get { return model; }
            set { this.model = value; }
        }
           public double HoursTalk
           {
               get { return hoursTalk; }
               set { this.hoursTalk = value; }
           }
           public double IdleTime
           {
               get { return idleTime; }
               set { this.idleTime = value; }
           }
        
        public Battery(BatteryType type, double hoursTalk, double idleTime)
        {
            this.type = type;
            this.hoursTalk = hoursTalk;
            this.idleTime = idleTime;
        }

        public double GetPrice(BatteryType type)
        {
            switch (type)
            {
                case BatteryType.LiIon:
                    return 49.99;
                case BatteryType.LiPoly:
                    return 59.99;
                default: throw new ArgumentException("Unsupported battery type!");
            }
        }
    }
}
