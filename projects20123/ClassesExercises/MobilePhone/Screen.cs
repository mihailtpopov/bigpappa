﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhone
{
    public class Screen
    {
        private string[] colors;
        private double size;
       
        public string[] Colors
        {
            get { return this.colors; }
            set { this.colors = value;}
        }
        public double Size
        {
            get { return this.size; }
            set { this.size = value; }
        }
        
        public double GetSizeInInches(SmartPhone smartPhone)
        {
            switch(smartPhone.Model)
            {
                case SmartPhone.PhoneModel.SamsungGalaxyNote9:
                    return 6.4;
                case SmartPhone.PhoneModel.SamsungGalaxyS9:
                    return 6.2;
                case SmartPhone.PhoneModel.LG_G8_ThinQ:
                    return 6.1;
                case SmartPhone.PhoneModel.OnePlus6:
                    return 6.28;
                default:
                    throw new ArgumentException("Unsupported phone model!");
            }
        }
    }
}
