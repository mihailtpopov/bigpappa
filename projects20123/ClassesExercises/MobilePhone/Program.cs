﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhone
{
    class Program
    {
        static void Main(string[] args)
        {
            SmartPhone phone = new SmartPhone("Samsung", SmartPhone.PhoneModel.SamsungGalaxyS9, 219.99);
            Screen screen = new Screen();
            string phoneInfo = phone.Info();
            Console.WriteLine(phoneInfo);
            double screenSize = screen.GetSizeInInches(phone);
            Console.WriteLine(screenSize);
            SmartPhone.Nokia95Info();
           
            Call firstCall = new Call(DateTime.Today, new TimeSpan(2,15,30), new TimeSpan(0, 0, 12,15));
            Call secondCall = new Call(DateTime.Today, new TimeSpan(4,21,11), new TimeSpan(0, 0, 17, 35));
            Call thirdCall = new Call(DateTime.Today, new TimeSpan(6, 17, 2), new TimeSpan(0, 0, 22, 11));
            Call fourthCall = new Call(DateTime.Today, new TimeSpan(2, 13, 11), new TimeSpan(0, 0, 5, 29));
            List<Call> callHistory = new List<Call>();

            int firstCallLength = firstCall.CallLength.Minutes;
            int secondCallLength = secondCall.CallLength.Minutes;
            callHistory.Add(firstCall);
            callHistory.Add(secondCall);
            callHistory.Add(thirdCall);
            callHistory.Add(fourthCall);
            List<Call> newCallHistory = Call.RemoveLongestCall(callHistory);
            double firstCallPrice = firstCall.GetCallPrice();

            
        }
    }
}
