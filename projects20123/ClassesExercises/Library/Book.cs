﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Book
    {
        private string heading;
        private string author;
        private int year;
        private string ISBNnumber;
        private string publishingHouse;
        public static int bookCount;

        public string Author
        {
            get { return this.author; }
            set { this.author = value; }
        }
        public string Heading
        {
            get { return this.heading; }
            set { this.heading = value; }
        }
        public int Year
        {
            get { return this.year; }
            set 
            { 
             if(year<=0)
             {
               throw new ArgumentException("Year should be positive");
             }
            this.year = value;
            }
        }
        public string ISBNNumber
        {
            get { return this.ISBNnumber; }
            set { this.ISBNnumber = value; }
        }
        public string PublishingHouse
        {
            get { return this.publishingHouse; }
            set { this.publishingHouse = value; }
        }
        public Book(string heading) :this(heading,"unknown",0,"unknown","unknown")
        { bookCount++; }

        public Book(string heading, string author):this(heading,author,0,"unknown","unknown")
        { bookCount++; }
        public Book(string heading, string author, int year):this(heading,author,year,"unknown","unknown")
        { bookCount++; }
        public Book(string heading, string author, int year, string ISBNnumber, string publishingHouse)
        {
            this.heading = heading;
            this.author = author;
            this.year = year;
            this.ISBNnumber = ISBNnumber;
            this.publishingHouse = publishingHouse;
            bookCount++;
        }
    }
}
