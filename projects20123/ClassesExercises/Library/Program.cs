﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library("Sava Dobroplodni");
            Book theWonderfulNewWorld = new Book("The Wonderful New World", "Oldys Hucksly", 1932, "unknown", "Hermes");
            Book theAnimalsFarm = new Book("The Animals Farm", "George Oruel", 1945, "unknown", "Sun");
            Book randomBook = new Book("random heading", "random author", 1824, "random ISBN", "random publishers");
            Book it = new Book("It", "Steven King", 1985, "unknown", "Hermes");
            library.AddBook(theAnimalsFarm);
            library.AddBook(theWonderfulNewWorld);
            library.AddBook(randomBook);
            library.AddBook(it);
            Book book = library.GetBookByAuthor("George Oruel");

            library.PrintAllBooksInfo();
            List<Book> stevenKingBookList = library.FindAllStevenKingsBooks();
            library.DeleteStevenKingBooks(stevenKingBookList);
            library.PrintAllBooksInfo();
        }
    }
}
