﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Library
    {
        private string name;
        private List<Book> bookList;
        public Library(string name)
        {
            this.name = name;
            bookList = new List<Book>();
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public List<Book> BookList
        {
            get { return this.bookList; }
            set { this.bookList = value; }
        }
        public Book GetBookByAuthor(string author)
        {
            foreach(Book book in this.bookList)
            {
                if(book.Author == author)
                {
                    return book;
                }
            }
            throw new ArgumentException("There isn't book with author {0} in the library!",author);
        }
        public void AddBook(Book book)
        {
            this.bookList.Add(book);
        }
        public void DeleteBook(Book book)
        {
            this.bookList.Remove(book);
        }
        public void PrintBookInfo(Book book)
        {
            Console.WriteLine("The new masterpiece \"{0}\" has floated the whole world! {1} one more time prove his"
                + "unique skills! The book had been published in {2}. {3} helped {1} to present to us that glamorous piece of art."
                + "\nISBN Number: {4}",book.Author, book.Year,book.PublishingHouse,book.ISBNNumber);
        }

        public void PrintAllBooksInfo()
        {
            foreach (Book book in BookList)
            {
                Console.WriteLine("The new masterpiece \"{0}\" has floated the whole world! {1} one more time prove his"
                    + "unique skills! The book had been published in {2}. {3} helped {1} to present to us that glamorous piece of art."
                    + "\nISBN Number: {4}",book.Heading, book.Author, book.Year, book.PublishingHouse, book.ISBNNumber);
                Console.WriteLine();
            }
        }
       public List<Book> FindAllStevenKingsBooks()
        {
           List<Book> stevenKingBookList = new List<Book>();
           foreach(Book book in BookList)
           {
               if(book.Author == "Steven King")
               {
                   stevenKingBookList.Add(book);
               }
           }
           return stevenKingBookList;
        }
        public void DeleteStevenKingBooks(List<Book> stevenKingBookList)
       {
           stevenKingBookList = FindAllStevenKingsBooks();
            foreach(Book book in stevenKingBookList)
            {
                DeleteBook(book);
            }
       }
    }
}
