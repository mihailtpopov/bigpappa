﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesExercises
{
    class Student
    {
        private string fullName;
        private string specialty;
        private string university;
        private string email;
        private string phoneNumber;
        public static int countInstances;
        public Student() : this(null)
        {
            countInstances++;
        }
        public Student(string fullName) : this(fullName,null)
        { countInstances++; }
        public Student(string fullName, string specialty):this(fullName,specialty,null)
        {}
        public Student(string fullName, string specialty, string email):this(fullName,specialty,email,null)
        {}
        public Student(string fullName, string specialty, string email, string phoneNumber)
        {
            this.fullName = fullName;
            this.specialty = specialty;
            this.email = email;
            this.phoneNumber = phoneNumber;
        }
        public string FullName
        {
            get { return fullName; }
        }
        public string Specialty
        {
            get { return specialty; }
            set { this.specialty = value; }
        }
        public string University
        {
            get { return university; }
            set { this.university = value; }
        }
        public string Email
        {
            get { return email; }
            set { this.email = value; }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { this.phoneNumber = value; }
        }
        public void StudentInfo()
        {
            Console.WriteLine("Full name: {0}\nSpecialty: {1}\nUniversity: {2}\nEmail: {3}\nPhone number: {4}",this.fullName,
                this.specialty,this.university,this.email,this.phoneNumber);
        }
    }
}
