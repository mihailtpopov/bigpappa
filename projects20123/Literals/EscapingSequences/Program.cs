﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscapingSequences
{
    class Program
    {
        static void Main(string[] args)
        {
            char symbol = 'a';
            Console.WriteLine(symbol);
            symbol = '\u003A';
            Console.WriteLine(symbol);
            symbol = '\'';
            Console.WriteLine(symbol);
            symbol = '\\';
            Console.WriteLine(symbol);

        }
    }
}
