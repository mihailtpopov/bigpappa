﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace TextFilesExercises
{
    class DoSmth
    {
        //1
        public static void PrintOddRows(StreamReader reader)
        {
            int nextRow = 1;
            using(reader)
            {
                string line = reader.ReadLine();
               while(line !=null)
               {
                   if(nextRow%2==1)
                   {
                       Console.WriteLine(line);
                   }
                   line = reader.ReadLine();
                   nextRow++;
               }
            }
           
        }

        // 2
        public static void MergeTwoFilesInThird(StreamReader firstFile, StreamReader secondFile)
        {
            StreamWriter thirdFile = new StreamWriter("mergedFile");
            using(firstFile)
            {
                using(secondFile)
                {
                   using(thirdFile)
                  {
                      string line = firstFile.ReadLine();
                       while(line!=null)
                       {
                           thirdFile.WriteLine(line);
                           line = firstFile.ReadLine();
                       }
                       line = secondFile.ReadLine();
                       while(line!=null)
                       {
                           thirdFile.WriteLine(line);
                           line=secondFile.ReadLine();
                       }
                  }
                }
            }
        }
        public static void AddRowNumberAndPrint(string readingFileName, string writingFileName)
        {
           //FileStream fileToModify = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
           //StreamReader reader = new StreamReader(fileToModify);
           //StreamWriter writer = new StreamWriter(fileToModify);
           // using(reader)
           // {
            //    using(writer)
            //    {
             //       string line = reader.ReadLine();
                //    int countRows = 1;
                //    while(line!=null)
                //    {
                 //       writer.WriteLine(countRows + ". " + line);
                 //       countRows++;
                  //      line = reader.ReadLine();
                  //  }
                //}
            StreamReader reader = new StreamReader(readingFileName);
            StreamWriter writer = new StreamWriter(writingFileName, true);
            List<string> textList = new List<string>();
            int numberRow = 1;
            using(reader)
            {
                using (writer)
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        writer.WriteLine(line);
                        line = reader.ReadLine();
                    }
                }
            }
            using(writer)
            {
                int writeNumber = 0;
                foreach(string textRow in textList)
                {
                    if(int.TryParse(textRow[0].ToString(), out writeNumber))
                    {
                        if(textRow.StartsWith(String.Format("{0}. ",writeNumber)))
                       {
                           numberRow++;
                        continue;
                       }
                    }
                    writer.WriteLine(numberRow + ". " + textRow);
                    numberRow++;
                }
            }
        }
    }
}
