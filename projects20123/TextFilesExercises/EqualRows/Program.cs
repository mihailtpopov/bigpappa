﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace EqualRows
{
    class Program
    {
        public static int GetEqualRows(StreamReader firstFile, StreamReader secondFile)
        {
            int equalRows = 0;
            string firstFileLine = firstFile.ReadLine();
            string secondFileLine = secondFile.ReadLine();
            while (firstFileLine != null && secondFileLine != null)
            {
                if (firstFileLine == secondFileLine)
                {
                    equalRows++;
                }
                firstFileLine = firstFile.ReadLine();
                secondFileLine = secondFile.ReadLine();
            }
            return equalRows;
        }
        static void Main(string[] args)
        {
            StreamReader firstReader = new StreamReader("firstFile.txt");
            StreamReader secondReader = new StreamReader("secondFile.txt");
            int equalRows = GetEqualRows(firstReader, secondReader);
            Console.WriteLine(equalRows);
        }
    }
}
