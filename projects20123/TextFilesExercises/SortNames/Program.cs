﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace SortNames
{
    class Program
    {
        public static void SortNames(StreamReader namesFile, StreamWriter sortedNamesFile)
        {
            using(namesFile)
            {
                using(sortedNamesFile)
                {
                    List<string> nameList = new List<string>();
                    string name = namesFile.ReadLine();
                    while (name != null)
                    {
                        nameList.Add(name);
                        name = namesFile.ReadLine();
                    }
                    nameList.Sort();
                    foreach (string namee in nameList)
                    {
                        sortedNamesFile.WriteLine(namee);
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            StreamReader namesFile = new StreamReader("names.txt");
            StreamWriter sortedNamesFile = new StreamWriter("writeNames.txt");
            SortNames(namesFile, sortedNamesFile);
          
        }
    }
}
