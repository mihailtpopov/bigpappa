﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace PrintSum
{
    class Program
    {
        public static int Get2x2MatrixMaxSum(StreamReader reader, int[,] matrix)
        {

            int maxMatrixSum = int.MinValue;
            using (reader)
            {
                string line = reader.ReadLine();
                line = reader.ReadLine();
                while (line != null)
                {
                    for (int i = 0; i < line.Length; i++)
                    {
                        for (int k = 0; k < line.Length; k++)
                        {
                            matrix[i, k] = line[k];
                        }
                        line = reader.ReadLine();
                    }
                }
            }
            for(int i = 0;i<matrix.GetLength(0)-1;i++)
            {
                for(int k = 0;k<matrix.GetLength(1)-1;k++)
                {
                    int matrixSum = matrix[i, k] + matrix[i, k + 1] + matrix[i + 1, k] + matrix[i + 1, k + 1];
                    if (matrixSum > maxMatrixSum)
                    {
                        maxMatrixSum = matrixSum;
                    }
                }
            }
            return maxMatrixSum;
        }
        public static void WriteMatrixToFile(StreamWriter writer, int[,] matrix)
        {
            matrix = FillMatrix(matrix);
            try
            {
                using (writer)
                {
                    writer.WriteLine(matrix.GetLength(0));
                    for (int i = 0; i < matrix.GetLength(0); i++)
                    {
                        for (int k = 0; i < matrix.GetLength(1); i++)
                        {
                            writer.Write(matrix[i, k]);
                        }
                        writer.WriteLine();
                    }
                }
            }
            catch(IOException)
            {
                throw new ArgumentException("Problem writing in the file!");
            }
        }
        public static int[,] FillMatrix(int[,] matrix)
        {
            for(int i = 0;i<matrix.GetLength(0);i++)
            {
                for(int k =0;k<matrix.GetLength(1);k++)
                {
                    matrix[i, k] = int.Parse(Console.ReadLine());
                }
            }
            return matrix;
        }
    
        static void Main(string[] args)
        {
            int matrixSize = int.Parse(Console.ReadLine());
            int[,] matrix = new int[matrixSize,matrixSize];
            string fileName = "matrix.txt";
            StreamWriter writer = new StreamWriter(fileName);
            WriteMatrixToFile(writer,matrix);

            StreamReader reader = new StreamReader(fileName);
            int matrixMaxSum = Get2x2MatrixMaxSum(reader, matrix);
        }
    }
}
