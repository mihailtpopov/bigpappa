﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedBinaryTreeRealization
{
    public class BinarySearchTree<T> where T : IComparable<T>
    {
       
        private class BinaryTreeNode<T> : IComparable<BinaryTreeNode<T>>
            where T : IComparable<T>
        {
            internal T value;
            internal BinaryTreeNode<T> parent;
            internal BinaryTreeNode<T> leftChild;
            internal BinaryTreeNode<T> rightChild;

            //The value of the tree node
            public BinaryTreeNode(T value)
            {
                this.value = value;
                this.parent = null;
                this.leftChild = null;
                this.rightChild = null;
            }
            public override string ToString()
            {
                return this.value.ToString();
            }
            public override int GetHashCode()
            {
                return this.value.GetHashCode();
            }
            public override bool Equals(object obj)
        {
            BinaryTreeNode<T> other = (BinaryTreeNode<T>)obj;
            return this.CompareTo(other) == 0;
        }
            public int CompareTo(BinaryTreeNode<T> other)
            {
                return this.value.CompareTo(other.value);
            }
        }
        private BinaryTreeNode<T> root;
        public BinarySearchTree()
        {
            this.root = null;
        }
        public void Insert(T value)
        {
            if(value == null)
            {
                throw new ArgumentNullException("Can not insert null value");
            }
            this.root = Insert(value, null, root);
        }
        private BinaryTreeNode<T> Insert(T value, BinaryTreeNode<T> parentNode, BinaryTreeNode<T> node)
        {
            if(node==null)
            {
                node = new BinaryTreeNode<T>(value);
                node.parent = parentNode;
            }
            else
            {
                int compareTo = value.CompareTo(node.value);
                if(compareTo<0)
                {
                    node.leftChild = Insert(value, node, node.leftChild);
                }
                if(compareTo>0)
                {
                    node.rightChild = Insert(value, node, node.rightChild);
                }
            }
            return node;
        }

        private BinaryTreeNode<T> Find(T value)
        {
            BinaryTreeNode<T> node = this.root;
           
            while(node!=null)
            {
                int compareTo = value.CompareTo(node.value);
                if(compareTo<0)
                {
                    node = node.leftChild;
                }
                else if(compareTo>0)
                {
                    node = node.rightChild;
                }
                else
                {
                    break;
                }
            }
            return node;
        }
        public void Remove(T value)
        {
            BinaryTreeNode<T> node = Find(value);
            if(node==null)
            {
                return;
            }
            Remove
        }
        public void Remove(BinaryTreeNode<T> node)
        {
            //Case 3: If the node has more than one child
            if(node.leftChild!=null && node.rightChild!=null)
            {
                BinaryTreeNode<T> replacement = node.rightChild;
                while(replacement.leftChild!=null)
                {
                    replacement = replacement.leftChild;
                }
                node.value = replacement.value;
                node = replacement;
            }
            //Case 1 and 2: If the node has at most one child
            BinaryTreeNode<T> theChild = node.leftChild != null ? node.leftChild : node.rightChild;

            //If the element to be deleted has one child
            if(theChild!=null)
            {
                theChild.parent = node.parent;
                //Handle the case if the element is the root
                if(node.parent == null)
                {
                    root = theChild;
                }
                else
                {
                    //Replace the element with its child subtree
                    if(node.parent.leftChild == node)
                    {
                        node.parent.leftChild = theChild;
                    }
                    if(node.parent.rightChild==node)
                    {
                        node.parent.rightChild = theChild;
                    }
                }
            }
            else
            {
                //Handle the case when the element is the root
                if(node.parent == null)
                {
                    root = null;
                }
                else
                {
                    //Remove the element - it is a leaf
                    if(node.parent.leftChild==node)
                    {
                        node.parent.leftChild = null;
                    }
                    if(node.parent.rightChild==node)
                    {
                        node.parent.rightChild = null;
                    }
                }
            }
        }
    }
}
