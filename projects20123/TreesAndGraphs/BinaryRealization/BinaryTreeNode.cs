﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryRealization
{
    class BinaryTreeNode<T>
    {
        private T value;
        private bool hasParent;
        private BinaryTreeNode<T> leftChild;
        private BinaryTreeNode<T> rightChild;

        public BinaryTreeNode(T value, BinaryTreeNode<T> leftChild, BinaryTreeNode<T> rightChild)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Cannot insert null value!");
            }
            this.value = value;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }
        public BinaryTreeNode(T value):this(value, null,null)
        {

        }
        public T Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
        public BinaryTreeNode<T> LeftChild
        {
            get { return this.leftChild; }
            set
            {
                if(value == null)
                {
                    return;
                }
                if(value.hasParent)
                {
                    throw new ArgumentNullException("The node already has a value!");
                }
                value.hasParent = true;
                this.leftChild = value;
            }
        }
        public BinaryTreeNode<T> RightChild
        {
            get { return this.rightChild; }
            set
            {
                if(value == null)
                {
                    return;
                }
                if(value.hasParent)
                {
                    throw new ArgumentException("The node already has a value!");
                }
                value.hasParent=true;
                this.rightChild = value;
            } 
        }
    }
}
