﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DirectoryTraverseDFS
{
    class DFSAlgorithm
    {
        private static void TraverseDir(DirectoryInfo dir,string spaces)
        {
            Console.WriteLine(spaces + dir.FullName);
            DirectoryInfo[] children = dir.GetDirectories();
            foreach(DirectoryInfo child in children)
            {
                TraverseDir(child, spaces + "   ");
            }
        }
        public static void TraverseDir(string directoryPath)
        {
            TraverseDir(new DirectoryInfo(directoryPath), string.Empty);
        }
    }
}
