﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGraph
{
    public abstract class AbstractGraph<T,K> : IGraph<T,K>
    {
        protected readonly List<T> VertexSet = new List<T>();
        protected readonly List<IPairValue<T>> EdgesSet = new List<IPairValue<T>>();
        protected readonly Dictionary<IPairValue<T>, K> Weights = new Dictionary<IPairValue<T>, K>();

       
        public bool AddVertex(T vertex)
        {
            if (vertex == null)
                throw new ArgumentNullException();
            if (VertexSet.Contains(vertex))
                return false;
            VertexSet.Add(vertex);
            return true;
        }

        public void AddVertex(IEnumerable<T> vertexSet)
        {
            if (vertexSet == null)
                throw new ArgumentNullException();
            var j = vertexSet.GetEnumerator();
            while(j.MoveNext())
            {
                if(j.Current)
            }
        }

        public bool DeleteVertex(T vertex)
        {
            throw new NotImplementedException();
        }

        public void DeleteVertex(IEnumerable<T> vertexSet)
        {
            throw new NotImplementedException();
        }

        public bool AddEdge(T v1, T v2, K weight)
        {
            throw new NotImplementedException();
        }

        public K GetWeight(T v1, T v2)
        {
            throw new NotImplementedException();
        }

        public bool DeleteEdge(T v1, T v2)
        {
            throw new NotImplementedException();
        }

        public bool AreAdjacent(T v1, T v2)
        {
            throw new NotImplementedException();
        }

        public int Degree(T vertex)
        {
            throw new NotImplementedException();
        }

        public int OutDegree(T vertex)
        {
            throw new NotImplementedException();
        }

        public int InDegree(T vertex)
        {
            throw new NotImplementedException();
        }

        public int VerticesNumber()
        {
            throw new NotImplementedException();
        }

        public int EdgesNumber()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> AdjacentVertices(T vertex)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetVertexSet()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPairValue<T>> GetEdgeSet()
        {
            throw new NotImplementedException();
        }
    }
}
