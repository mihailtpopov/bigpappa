﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGraph
{
    class PairValueImplementation<T> : IPairValue<T>
    {
        private readonly T t1;
        private readonly T t2;

        public PairValueImplementation(T t1, T t2)
        {
            if (t1 == null || t2 == null)
            {
                throw new ArgumentNullException();
            }
            if (t1.GetType() != t2.GetType())
            {
                throw new ArgumentException();
            }
            this.t1 = t1;
            this.t2 = t2;
        }
        public T GetFirst()
        {
            return t1;
        }

        public T GetSecond()
        {
            return t2;
        }

        public bool Contains(T value)
        {
            return value.Equals(t1) || value.Equals(t2);
        }
        public override bool Equals(object o)
        {
            if (o == null || o.GetType() != typeof(PairValueImplementation<T>))
                return false;
            PairValueImplementation<T> pairObject = (PairValueImplementation<T>)o;
            return pairObject.t1.Equals(this.t1) && pairObject.t2.Equals(this.t2);
        }

        public override int GetHashCode()
        {
            return this.t1.GetHashCode() + this.t2.GetHashCode();
        }
    }
}
