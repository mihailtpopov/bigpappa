﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeTasks
{
    class Program
    {
        static int ReversePositiveNumber(int number)
        {
            if(number<0)
            {
                throw new ArgumentException("The numbers must be positive!");
            }
            int remainder = 0;
            int reversedNumber=0;
            while (number > 0)
            {
                remainder = number % 10;
                reversedNumber = (reversedNumber* 10) + remainder;
                number = number / 10;
            }
            return reversedNumber;
        }
        static double CalculateАverage(params int[] numbers)
        {
            if(numbers.Length==0)
            {
                throw new ArgumentException("The sequence can not be empty!");
            }
            int sumSequence = 0;
            foreach (int number in numbers)
            {
                sumSequence += number;
            }
            double average = (double)sumSequence / numbers.Length;
            return average;
        }
        static void Main(string[] args)
        {
            double average = CalculateАverage(1);
            Console.Write("Enter a positive number: ");
            int number = int.Parse(Console.ReadLine());
            int reversedNumber = ReversePositiveNumber(number);
            Console.WriteLine("The reversed number is: " + reversedNumber);
        }
    }
}
