﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindMostCommonNumber
{
    class Program
    {
        static int GetMostCommonNumber(params int[] arr)
        {
            int counter = 0;
            int maxCounter = int.MinValue;
            int mostCommon = int.MinValue;
            for(int i = 0; i<arr.Length-1;i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if(arr[i]==arr[j])
                    {
                        counter++;
                    }
                }
                if (counter >= maxCounter)
                {
                    if (counter == maxCounter)
                    {
                        mostCommon = 0;
                    }
                    else
                    {
                        mostCommon = arr[i];
                        maxCounter = counter;
                    }
                }
                counter = 0;
            }
            return mostCommon;
        }
        static void Main(string[] args)
        {
           int mostCommonNumber = GetMostCommonNumber(1, 5, 1, 2, 3, 6);
           if (mostCommonNumber != 0)
           {
               Console.WriteLine("The most common number is: " + mostCommonNumber);
               return;
           }
           Console.WriteLine("There isn't most common number! ");
        }
    }
}
