﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintN_1To100
{
    class Program
    {
       static long PrintFactorial(int number)
        {
           if(number<1 || number>100)
           {
               throw new ArgumentException("The number must be in the range [1..100]!");
           }
            int factorial = 1;
           for(int i = number;i>0;i--)
           {
               factorial *= i;
           }
           return factorial;
        }
        static void Main(string[] args)
        {
            Console.Write("Enter a number: ");
            int number = int.Parse(Console.ReadLine());
            long factorial = PrintFactorial(number);
            Console.WriteLine("The factorial of the number is: {0}",factorial);
        }
    }
}
