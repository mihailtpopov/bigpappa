﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindLargestElement
{
    class Program
    {
        static int FindLargestElement(int[] arr)
        {
            for(int i = 0; i<arr.Length-1;i++)
            {
                for(int j = i+1;j<arr.Length;j++)
                {
                    if(arr[i]>arr[j])
                    {
                        int temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr[arr.Length - 1];
        }
        static int[] AddArrayElements(int[] arr)
        {
            for(int i = 0; i<arr.Length;i++)
            {
                arr[i] = int.Parse(Console.ReadLine());
            }
            return arr;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the length of the array:");
            int length = int.Parse(Console.ReadLine());
            int[] array = new int[length];
            array = AddArrayElements(array);
            int largestElement = FindLargestElement(array);

        }
    }
}
