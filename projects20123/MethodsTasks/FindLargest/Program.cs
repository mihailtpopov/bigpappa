﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindLargest
{
    class Program
    {
        static int GetMax(int firstNum, int secondNum)
        {
           int larger = Math.Max(firstNum, secondNum);
           return larger;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter three integers: ");
            int firstNum = int.Parse(Console.ReadLine());
            int secondNum = int.Parse(Console.ReadLine());
            int thirdNum = int.Parse(Console.ReadLine());
            int largestNum = GetMax(GetMax(firstNum, secondNum), thirdNum);
            Console.WriteLine("The largest number is: " + largestNum);
        }
    }
}
