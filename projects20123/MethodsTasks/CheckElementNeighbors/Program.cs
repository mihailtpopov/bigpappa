﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckElementNeighbors
{
    class Program
    {
        static string PrintValueAccordingNeighbors(int index, params int[] arr)
        {
            string elementValueAccordingNeighbors = null;
            int saveValue = 0;
            if(index!=arr.Length-1 && index!=0)
            {
               saveValue = GetMax(arr[index],arr[index+1]);
                if(saveValue>arr[index])
                {
                    elementValueAccordingNeighbors += "Element is smaller than its right neighbor ";
                }
                else if(saveValue<arr[index])
                {
                    elementValueAccordingNeighbors += "Element is larger than its right neighbor";
                }
                else
                {
                    elementValueAccordingNeighbors += "Element is equal to its right neighbor";
                }

                saveValue = GetMax(arr[index], arr[index - 1]);
                    if (saveValue > arr[index])
                    {
                        elementValueAccordingNeighbors += "and smaller than its left neighbor ";
                    }
                    else if (saveValue < arr[index])
                    {
                        elementValueAccordingNeighbors += "and larger than its left neighbor";
                    }
                    else
                    {
                        elementValueAccordingNeighbors += "and equal to its left neighbor";
                    }
            }
            else
            {
                if(index==0)
                {
                    saveValue = GetMax(arr[index], arr[index + 1]);
                    if(saveValue>arr[index])
                    {
                        elementValueAccordingNeighbors += "Element is smaller than its neighbor";
                    }
                    else if( saveValue<arr[index])
                    {
                        elementValueAccordingNeighbors += "Element is larger than its  neighbor";
                    }
                    else
                    {
                        elementValueAccordingNeighbors += "Element is equal to its neighbor";
                    }
                }
                else
                {
                    saveValue = GetMax(arr[index], arr[index - 1]);
                    if (saveValue > arr[index])
                    {
                        elementValueAccordingNeighbors += "Element is smaller than its neighbor";
                    }
                    else if (saveValue < arr[index])
                    {
                        elementValueAccordingNeighbors += "Element is larger than its  neighbor";
                    }
                    else
                    {
                        elementValueAccordingNeighbors += "Element is equal to its neighbor";
                    }
                }
            }
            return elementValueAccordingNeighbors;
        }

        static int GetMax(int firstElement, int secondElement)
        {
            int larger = Math.Max(firstElement, secondElement);
            return larger;
        }
        static void Main(string[] args)
        {
           string elementValueAccordingNeighbors = PrintValueAccordingNeighbors(1, 1, 2, 3);
           Console.WriteLine(elementValueAccordingNeighbors);
        }
    }
}
