﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintName
{
   public class Name
    {
       public static Func<string, string> PrintName = (name) =>
        {
            return "Hello, " + name + "!";
        };
        static void Main(string[] args)
        {
            string name = Console.ReadLine();
            string printedName = PrintName(name);
        }
    }
}
