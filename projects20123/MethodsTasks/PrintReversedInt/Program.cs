﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintReversedInt
{
    class Program
    {
       static void PrintReversedInt(int number)
        {
            string strNumber = number.ToString();
            strNumber.Reverse();
            int reversedNumber = Convert.ToInt32(strNumber);
            Console.WriteLine(reversedNumber);
        }
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            PrintReversedInt(number);
        }
    }
}
