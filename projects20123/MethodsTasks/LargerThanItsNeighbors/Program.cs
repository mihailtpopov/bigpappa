﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargerThanItsNeighbors
{
    class Program
    {
        static int GetFirstLargerThanNeighborsElementIndex(params int[] arr)
        {
            for(int i = 1;i<arr.Length-1;i++)
            {
                if(arr[i]>arr[i+1] && arr[i]>arr[i-1])
                {
                    return i;
                }
            }
            return -1;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Return index of element, if element is larger than both of its neighbors or print -1");
            int result = GetFirstLargerThanNeighborsElementIndex(1, 2, 2, 5, 6, 4);
            Console.WriteLine(result);
        }
    }
}
