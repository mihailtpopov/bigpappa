﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UslovniKonstrukcii
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 6;
            switch(a)
            {
                case 1:
                case 4:
                case 6:
                case 8:
                case 10:
                    Console.WriteLine("Числото не е просто");
                    break;
                case 2:
                case 3:
                case 5:
                case 7:
                    Console.WriteLine("Числото е просто");
                    break;
                default:
                    Console.WriteLine("Не знам какво е това число!");
                    break;
            }
        }
    }
}
