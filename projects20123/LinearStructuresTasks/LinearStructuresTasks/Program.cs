﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearStructuresTasks
{
    class Program
    {
        static void Main(string[] args)
        {
                int number=0;
                List<int> numbersList = new List<int>();
                bool success = true;
                 while (success)
                {
                    string strNumber = Console.ReadLine();
                    success=int.TryParse(strNumber,out number);
                    if(success)
                    {
                      numbersList.Add(number);
                    }
                    else
                    {
                        if(strNumber == Environment.NewLine)
                        {
                            Console.WriteLine("The sum of the numbers is: " + numbersList.Sum());
                            return;
                        }
                        else
                        {
                            throw new FormatException("Invalid number or character(s) entered");
                        }
                    }
                }
        }
    }
}
