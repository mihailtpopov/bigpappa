﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Majorant
{
    class Program
    {
        public static int FindMajorant(int[] intArray)
        {
            List<int> list = new List<int>(intArray);
            list.Sort();
            int countOccurencies = 1;
            for (int index = 0; index <= list.Count - 1; index += countOccurencies)
            {
                countOccurencies = 1;
                for (int nextIndex = index + 1; nextIndex < list.Count; nextIndex++)
                {
                    if (list[index] == list[nextIndex])
                    {
                        countOccurencies++;
                    }
                    else
                    {
                        break;
                    }
                }
                if(countOccurencies > (double)list.Count/2+1)
                {
                    return list[index];
                }
            }
            return 0;
        }
        public static void AddItems(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                try
                {
                    int number = int.Parse(Console.ReadLine());
                    if (number < 0 || number > 1000)
                    {
                        throw new ArgumentException("Number must be in the range [0...1000]");
                    }
                    array[i] = number;
                }
                catch (FormatException)
                {
                    throw new FormatException("You must enter an integer!");
                }
            }
        }
        public static void PrintArray(int[] array)
        {
            Console.Write("{");
            foreach (var item in array)
            {
                Console.Write(" {0}", item);
            }
            Console.Write("}");
        }
        static void Main(string[] args)
        {
            int[] array = new int[8];
            AddItems(array);
            int majorant = FindMajorant(array);
            PrintArray(array);
            if(majorant==0)
            {
                Console.WriteLine("The majorant does not exists!");
            }
            else
            {
                Console.WriteLine(majorant);
            }
        }
    }
}
