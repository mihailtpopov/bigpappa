﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddOddsNumberNumbers
{
    class Program
    {
        public static List<int> AddNumbersWithOddOccurencies(List<int> intList)
        {
            List<int> oddOccurenciesList = new List<int>();
            intList.Sort();
            int countOccurencies = 1;
            for(int index =0;index<=intList.Count-1;index+=countOccurencies)
            {
               countOccurencies = 1;
                for(int nextIndex = index+1;nextIndex<intList.Count;nextIndex++)
                {
                    if(intList[index]==intList[nextIndex])
                    {
                        countOccurencies++;
                    }
                    else
                    {
                        break;
                    }
                }
                if(countOccurencies%2==1)
                {
                    oddOccurenciesList.Add(intList[index]);
                }
            }
            return oddOccurenciesList;
        }
        public static void AddItems(List<int> list)
        {
            for(int i = 0;i<list.Capacity;i++)
            {
                try
                {
                int number = int.Parse(Console.ReadLine());
                list.Add(number);
                }
                catch(FormatException)
                {
                    throw new FormatException("You must enter an integer!");
                }
            }
        }
        public static void PrintList(List<int> list)
        {
            Console.Write("{");
            foreach (var item in list)
            {
                Console.Write(" {0}", item);
            }
            Console.Write("}");
        }
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            List<int> oddOccurenciesNumbersList = new List<int>(10);
            AddItems(list);
            oddOccurenciesNumbersList = AddNumbersWithOddOccurencies(list);
            PrintList(oddOccurenciesNumbersList);
        }
    }
}
