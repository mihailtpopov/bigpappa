﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintNNumbersBackwards
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> intStack = new Stack<int>();
            int numbersToEnter = int.Parse(Console.ReadLine());
            for (int i = 0; i < numbersToEnter; i++)
            {
                try
                {
                    int number = int.Parse(Console.ReadLine());
                    intStack.Push(number);
                }
                catch (FormatException)
                {
                    throw new FormatException("Invalid number!");
                }
            }
            while(intStack.Count>0)
            {
                int number = intStack.Pop();
                Console.WriteLine(number);
            }
        }
    }
}
