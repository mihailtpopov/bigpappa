﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintNumbersInAscendingOrder
{
    class Program
    {
        static void Main(string[] args)
        {
            int number=0;
                List<int> numbersList = new List<int>();
            List<int> sortedList = new List<int>();
                bool success = true;
                while (success)
                {
                    string strNumber = Console.ReadLine();
                    success = int.TryParse(strNumber, out number);
                    if (success)
                    {
                        numbersList.Add(number);
                    }
                    else
                    {
                        if (strNumber == "\n")
                        {
                            sortedList = numbersList.OrderBy(i => i).ToList();
                            return;
                        }
                        else
                        {
                            throw new FormatException("Invalid number or character(s) entered");
                        }
                    }
                }
        }
    }
}
