﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveAllNegativeNumbers
{
    class Program
    {
        public static List<int> RemoveNegatives()
        {
                bool success = true;
                int number;
                List<int> nonNegative = new List<int>();
                do
                {
                    string str = Console.ReadLine();
                    success = int.TryParse(str, out number);
                    if (success)
                    {
                        if (number > 0)
                        {
                            nonNegative.Add(number);
                        }
                    }
                    else
                    {
                        if (str != "stop")
                        {
                            throw new FormatException(String.Format("You must enter a number. This is not a number - {0}", str));
                        }
                        return nonNegative;
                    }
                }
                while (success);
                return nonNegative;
        }
        public static void PrintList(List<int> list)
        {
            Console.Write("{");
            foreach(var item in list)
            {
                Console.Write(" {0}",item);
            }
            Console.Write("}");
        }
        static void Main(string[] args)
        {
            List<int> nonNegative = new List<int>();
            nonNegative = RemoveNegatives();
            PrintList(nonNegative);
        }
    }
}
