﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindLargestSequenceOfEqualElements
{
   public class Sequence
    {
        public static List<int> LargestSequenceOfEqualElements(List<int> intList)
        {
            int counter=0;
            int mostCommon = 0;
            int maxCount = int.MinValue;
            List<int> mostCommonNumberList = new List<int>();
            for(int i = 0;i<intList.Count-1;i++)
            {
                counter = 0;
                for(int k = i+1;k<intList.Count;k++)
                {
                    if(intList[i]==intList[k])
                    {
                        counter++;
                    }
                    if(counter>maxCount)
                    {
                        maxCount = counter;
                        mostCommon = intList[i];
                    }
                }
            }
            for(int i = 0;i<maxCount;i++)
            {
                mostCommonNumberList.Add(mostCommon);
            }
            return mostCommonNumberList;
        }
        public static void PrintList(List<int> list)
        {
            Console.Write("{");
            foreach(var item in list)
            {
                Console.Write(" " + item);
            }
            Console.Write(" }");
        }
    }
}
