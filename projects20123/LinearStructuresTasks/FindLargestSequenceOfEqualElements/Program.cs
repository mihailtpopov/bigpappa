﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindLargestSequenceOfEqualElements
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> intList = new List<int> { 1, 2, 3, 4, 6, 7, 12, 4, 2, 1, 2, 4, 6 };
            List<int> mostCommonNumberlist = Sequence.LargestSequenceOfEqualElements(intList);
            Sequence.PrintList(mostCommonNumberlist);
        }
    }
}
