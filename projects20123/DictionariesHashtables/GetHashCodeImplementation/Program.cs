﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetHashCodeImplementation
{
    class Point3D
    {
        private double y;
        private double x;
        private double z;

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            Point3D other = obj as Point3D;
            if (other == null)
                return false;
            if (!this.x.Equals(other.x))
                return false;
            if (!this.y.Equals(other.y))
                return false;
            if (!this.z.Equals(other.z))
                return false;

            return true;
        }

        public Point3D(double y, double z, double x)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public override int GetHashCode()
        {
            int prime = 83;
            int result = 1;

            unchecked
            {
                result = prime * result + x.GetHashCode();
                result = prime * result + y.GetHashCode();
                result = prime * result + z.GetHashCode();
            }
            return result;
        }
        static void Main(string[] args)
        {

        }
    }
}
