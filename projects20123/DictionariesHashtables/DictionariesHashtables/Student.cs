﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesHashtables
{
   public class Student
    {
        private string name;
        private int salary;

        public int Salary
        {
            get { return this.salary; }
            set { this.salary = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public Student(string name, int salary)
        {
            this.name = name;
            this.salary = salary;
        }
        public Student()
        {

        }
        public int Compare(Student x, Student y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
