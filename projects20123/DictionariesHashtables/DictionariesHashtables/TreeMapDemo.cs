﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesHashtables
{
    class TreeMapDemo
    {
        private static readonly string TEXT = "She uchish li she bachkash li? Be kvo she bachkash " +   
                "be? Tui vashto uchene li e? Ia po-hubavo opitai da " +
                "BACHKASH da se uchish malko! Uchish ne uchish trqbva " +   "da bachkash!"; 
        static void Main(string[] args)
        {
            IDictionary<Student, int> studentDictionary = new SortedDictionary<Student,int>(new SortByStudentSalary());
            studentDictionary[new Student("Slavko", 2700)] = 1102;
            studentDictionary[new Student("Misho", 1900)] = 1103;
            studentDictionary[new Student("Atanas", 1400)] = 1104;
            studentDictionary[new Student("Borislav", 1100)] = 1105;
            foreach(var pair in studentDictionary)
            {
                Console.WriteLine("Name: {0}, Salary: {1}, ID: {2}",pair.Key.Name,pair.Key.Salary,pair.Value);
            }
            IDictionary<string, int> wordOccurenceMap = GetWordOccurenceMap(TEXT);
            PrintWordOccurenceMap(wordOccurenceMap);
        }

        private static IDictionary<string, int> GetWordOccurenceMap(string text)
        {
            string[] splittedText = text.Split(' ',',','.','?','!',':',';','-');
            CaseInsensitive ci = new CaseInsensitive();
            IDictionary<string,int> words = new SortedDictionary<string,int>(ci);

            foreach(var word in splittedText)
            {
              if(string.IsNullOrEmpty(word.Trim()))
              {
                  continue;
              }
              int count;
                if(!words.TryGetValue(word,out count))
                {
                    count=0;
                }
                words[word] = count + 1;
            }
            return words;
        }
        private static void PrintWordOccurenceMap(IDictionary<string, int> wordOccurenceMap)
        {
            foreach(KeyValuePair<string,int> wordEntry in wordOccurenceMap)
            {
                Console.WriteLine("Word {0} occurs {1} times! in the text",wordEntry.Key,wordEntry.Value);
            }
        }
    }
}
