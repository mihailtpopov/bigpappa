﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesHashtables
{
    class SortByStudentSalary : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Salary.CompareTo(y.Salary);
        }
    }
}
