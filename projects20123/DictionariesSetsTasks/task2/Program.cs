﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        public static string PrintOddOcurrencies(List<int> oddOccurencies)
        {
            StringBuilder builder = new StringBuilder();
           
            builder.Append("{");
            foreach(int number in oddOccurencies)
            {
                builder.Append(number+", ");
            }

            builder.Replace(builder.ToString(),builder.ToString().TrimEnd(',', ' '));
            builder.Append("}");
            return builder.ToString();
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the length of the sequence");
            int n = int.Parse(Console.ReadLine());
            int nextNum = 0;
            List<int> oddOccurenciesList = new List<int>();
            IDictionary<int, int> numberOccurencies = new Dictionary<int, int>(n);
            for (int i = 0; i < n; i++)
            {
                nextNum = int.Parse(Console.ReadLine());
                int count;
                if (numberOccurencies.TryGetValue(nextNum, out count))
                {
                    numberOccurencies[nextNum] = count + 1;
                }
                else
                    numberOccurencies[nextNum] = 1;
            }
            foreach(KeyValuePair<int,int> numPair in numberOccurencies)
            {
                if(numPair.Value%2==1)
                {
                    oddOccurenciesList.Add(numPair.Key);
                }
            }
            string oddNumberList = PrintOddOcurrencies(oddOccurenciesList);
            Console.WriteLine(oddNumberList);
        }
    }
}
