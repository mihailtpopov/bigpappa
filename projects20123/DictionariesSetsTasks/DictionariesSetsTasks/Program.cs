﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesSetsTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the length of the sequence");
            int n = int.Parse(Console.ReadLine());
            int nextNum = 0;
            IDictionary<int, int> intDictionary = new Dictionary<int, int>(n);
            for(int  i =0; i<n;i++)
            {
                nextNum = int.Parse(Console.ReadLine());
                int count;
                if (intDictionary.TryGetValue(nextNum, out count))
                {
                    intDictionary[nextNum] = count + 1;
                }
                else
                intDictionary[nextNum] = 1;
            }
            foreach(KeyValuePair<int,int> numPair in intDictionary)
            {
                Console.WriteLine("{0} --> {1} times",numPair.Key,numPair.Value);
            }
        }
    }
}
