﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace task3
{
    class CaseInsensitiveComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return string.Compare(x, y, true);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("file4e.txt");
            string[] words = reader.ReadLine().Split(' ',',','.','!','?');
            CaseInsensitiveComparer cic = new CaseInsensitiveComparer();
            IDictionary<string,int> wordOccurencies = new Dictionary<string,int>();
            int count;
            foreach(string word in words)
            {
                if(wordOccurencies.TryGetValue(word, out count))
                {
                    wordOccurencies[word.ToLower()] = count + 1;
                    continue;
                }
                wordOccurencies[word.ToLower()] = 1;
            }
            foreach(KeyValuePair<string,int> wordOccurence in wordOccurencies.OrderBy(key=>key.Value))
            {
                Console.WriteLine("{0} -> {1} times",wordOccurence.Key,wordOccurence.Value);
            }
        }
    }
}
