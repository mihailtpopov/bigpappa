﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "abcde";
            char ch = str[0];

            string smallCSharp = "c#";
            string bigCSharp = "C#";
            Console.WriteLine(smallCSharp.Equals(bigCSharp,StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
