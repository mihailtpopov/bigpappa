﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
namespace ParseExact
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string text = "11.09.2001";
            string format = "dd.MM.yyyy";
            DateTime parsedDate = DateTime.ParseExact(text, format, CultureInfo.InvariantCulture);
            Console.WriteLine("Day: {0} \nMonth: {1} \nYear: {2}",parsedDate.Day, parsedDate.Month, parsedDate.Year);


        }
    }
}
