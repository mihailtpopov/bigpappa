﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemovingUnneccesarySymbols
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileData = "    \n\n      Ivan Ivanov     ";
            string fixedData = fileData.Trim();
            Console.WriteLine(fixedData);

            //by give list
            string fileData1 = "   111 $  %    Ivan Ivanov  ### s   ";
            char[] trimChars = new char[] {' ', '1', '$', '%', '#', 's' };
            string reduced = fileData1.Trim(trimChars);
            string almostReduced = fileData1.TrimEnd(trimChars);
            Console.WriteLine(reduced);
            Console.WriteLine(almostReduced);

        }
    }
}
