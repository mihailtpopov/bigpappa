﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllOccurrenciesInString
{
    class Program
    {
        static void Main(string[] args)
        {
            string quote = "The main purpose of the \"Intro to C#\" book is to introduce C# to newbies.";
            string keyword = "C#";
            int index = quote.IndexOf(keyword);
            while(index!=-1)
            {
                Console.WriteLine("{0} found at position {1}",keyword,index);
                index = quote.IndexOf(keyword, index + keyword.Length);
            }

        }
    }
}
