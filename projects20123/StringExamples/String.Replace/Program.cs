﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String.Replace
{
    class Program
    {
        static void Main(string[] args)
        {
            string doc = "Hello, some@gmail.com, you have been using some@gmail.com in your registration.";
            string fixedDoc = doc.Replace("some@gmail.com", "michaelp@gmail.com");
            Console.WriteLine(doc);
            Console.WriteLine(fixedDoc);
        }
    }
}
