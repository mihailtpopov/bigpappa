﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_ChangeBits
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Number must be from 1-100");
            int number = int.Parse(Console.ReadLine());
            bool isSimple = true;
            int counter =0;
            if(number>100 || number<1)
            {
                throw new ArgumentException("Please, enter number between 1 and 100");
            }
            for(int i = 1; i<number;i++)
            {
                if(number%i==0)
                {
                    counter++;
                }
            }
            if(counter>0)
            {
                Console.WriteLine("The number you've entered is not prime");
            }
            else
            {
                Console.WriteLine("The number you've entered is prime. It has {0} dividers",counter);
            }
        }
    }
}
