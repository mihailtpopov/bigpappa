﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoExampleClasses
{
    public class ShoppingCartModel
    {
        public delegate void MentionDiscount(decimal subTotal);

        public List<ProductModel> Items = new List<ProductModel>();

        public decimal GenerateTotal(MentionDiscount mentionDiscount,
            Func<List<ProductModel>, decimal, decimal> calculateDiscountedTotal)
        {
            decimal subTotal = Items.Sum(x => x.Price); //sum the items by their price
            mentionDiscount(subTotal);
            if(subTotal>100)
            {
                return subTotal*0.80M;
            }
            else if(subTotal>50)
            {
                return subTotal * 0.85M;
            }
            else if(subTotal>10)
            {
                return subTotal * 0.90M;
            }
            else
            {
                return subTotal;
            }
            return calculateDiscountedTotal(Items, subTotal);
        }
    }
}
