﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsExamples
{
    class Integer
    {
        public static void Print(int num)
        {
            Console.WriteLine(num);
        }
        public static void Modify(int num)
        {
            num = 10;
            Console.WriteLine("Modified integer:");
            Print(num);
        }
    }
}
