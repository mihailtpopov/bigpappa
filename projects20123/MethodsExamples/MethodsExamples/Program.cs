﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Array as argument example:");
            Console.WriteLine();
            int[] arr = { 1, 2, 3 };
            Console.WriteLine("Before Modify() the argument is:");
            Array.Print(arr);
            
            Array.Modify(arr);

            Console.WriteLine("After Modify() the argument is:");
            Array.Print(arr);

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Integer as argument example");
            Console.WriteLine();
            int num = 5;
            Console.WriteLine("Before Modify() the argument is:");
            Integer.Print(num);

            Integer.Modify(num);

            Console.WriteLine("After Modify() the argument is: ");
            Integer.Print(num);

        }
    }
}
