﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsExamples
{
    public class Array
    {
        public static void Modify(int[] arr)
        {
            arr[0] = 5;
            Console.WriteLine("Modified array:");
            Print(arr);
        }
        public static void Print(int[] arr)
        {
             Console.Write(arr[0]);
            for(int i = 1;i<arr.Length;i++)
            {
                Console.Write(", " + arr[i]);
            }
            Console.WriteLine();
        }

    }
}
