﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace TextFiles
{
    class CatchExceptions
    {
        public static void ReadToEndFile(StreamReader reader, string fileName)
        {
            try
            {
                reader = new StreamReader(fileName);
                Console.WriteLine("File {0} successfully opened!",fileName);
                Console.WriteLine("File contents:");
                using (reader)
                {
                    reader.ReadToEnd();
                }
            }
            catch(FileNotFoundException)
            {
                Console.Error.WriteLine("Can not find file {0}.",fileName);
            }
            catch(DirectoryNotFoundException)
            {
                Console.Error.WriteLine("Invalid directory in file path");
            }
            catch(IOException)
            {
                Console.Error.WriteLine("Can not open the file {0}!",fileName);
            }
        }
    }
}
