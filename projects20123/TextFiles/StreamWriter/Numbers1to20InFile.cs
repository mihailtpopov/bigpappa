﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace TextFiles
{
    class Numbers1to20InFile
    {
        public static void Print(StreamWriter writer)
        {
            using(writer)
            {
                for(int i = 1;i<=20;i++)
                {
                    writer.WriteLine(i);
                }
            }
        }
    }
}
