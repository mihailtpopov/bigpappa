﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace NumberOfOccurencies
{
    class SearchInTextFile
    {
        public static void FindNumberOccurrencies(StreamReader reader,string fileName,string keyword)
        {
            try
            {
                reader = new StreamReader(fileName);
                int occurencies = 0;
                using (reader)
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        int index = line.IndexOf(keyword);
                        while (index != -1)
                        {
                            occurencies++;
                            index = line.IndexOf(keyword, index + keyword.Length);
                        }
                        line = reader.ReadLine();
                    }
                }
                Console.WriteLine("The word {0} occurs {1} times in {2}",keyword,occurencies,fileName);
            }
            catch(FileNotFoundException)
            {
                Console.WriteLine("File {0} can not be found!",fileName);
            }
            catch(IOException)
            {
                Console.WriteLine("File {0} can not be open!",fileName);
            }
        }
    }
}
