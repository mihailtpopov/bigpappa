﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentModifyments
{
    class Student
    {
        private string firstName;
        private string familyName;
        private int age;

        public string FirstName
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }
        public string FamilyName
        {
            get { return this.familyName; }
            set { this.familyName = value; }
        }
        public int Age
        {
            get { return this.age; }
            set 
            {
                if (age < 18)
                {
                    throw new ArgumentException("This student is too young to be student!");
                }
                this.age = value;
            }
        }
        public Student(string firstName, string familyName, int age)
        {
            this.firstName = firstName;
            this.familyName = familyName;
            this.age = age;
        }
        public static IList<Student> GetLexycographycallyLargerFamilyNamesThanFirstNames
            (List<Student> studentsList)
        {
            var sortedStudents =
                (from student in studentsList
                where string.Compare(student.FirstName, student.FamilyName) == -1
                select student).ToList();
            return sortedStudents;
        }
    }
}
