﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentModifyments
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> studentsList = new List<Student>()
            {
                new Student("Atanas","Kolev",22),
                new Student("Kolio","Drumev",21),
                new Student("Mihail","Popov",19),
                new Student("Akakii","Akakievich",25),
                new Student("Mihail","Atanasov",21)
            };
            //task 4
            var sortedStudentsByAge =
                from student in studentsList
                where student.Age >= 18 && student.Age <= 24
                select new { FirstName = student.FirstName, FamilyName = student.FamilyName };

                foreach(var student in sortedStudentsByAge)
                {
                    Console.WriteLine(student);
                }

            //task 3
            IList<Student> sortedStudents = Student.GetLexycographycallyLargerFamilyNamesThanFirstNames(studentsList);
            foreach(var student in sortedStudents)
            {
                Console.WriteLine("{0} {1} is {2} student",student.FirstName,student.FamilyName,student.Age);
            }

            //task 5 finished with lambda 
            var sortedStudentsByNamesLambda = studentsList.OrderByDescending(x => x.FirstName).ThenByDescending(x => x.FamilyName);
            foreach(var student in sortedStudentsByNamesLambda)
            {
                Console.WriteLine("{0} {1}",student.FirstName,student.FamilyName);
            }
            //finished with LINQ
            var sortedStudentsByNamesLINQ =
                from student in studentsList
                orderby student.FirstName descending
                select new {
                    FirstName = student.FirstName, 
                    SecondName = student.FamilyName
                };
                  
               foreach(var student in sortedStudentsByNamesLINQ)
               {
                   Console.WriteLine(student);
               }
               
        }
    }
}
