﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendStringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder builder = null;
            builder.Append("The world needs more interesting people");
            StringBuilder substringBuilder = builder.Substring(-1, 5);
            Console.WriteLine(substringBuilder.ToString());
        }
    }
}
