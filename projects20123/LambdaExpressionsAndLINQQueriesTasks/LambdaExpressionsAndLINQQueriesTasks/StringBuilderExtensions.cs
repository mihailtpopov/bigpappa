﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtendStringBuilder
{
   static class StringBuilderExtensions
    {
       public static StringBuilder Substring(this StringBuilder strb,int index,int length)
       {
           if(index<0 || length<0)
           {
               throw new ArgumentException("Index and length must be 0 or positive numbers");
           }
          
           StringBuilder newBuilder = new StringBuilder(length);
           try
           {
               for(int i = length, nextChar = 0; i > 0; i--,nextChar++)
               {
                   newBuilder.Append(strb[index + nextChar]);
               }
           }
           catch(IndexOutOfRangeException)
           {
               throw new IndexOutOfRangeException("The index is outside the range of the StringBuilder variable");
           }
           return newBuilder;
       }
    }
}
