﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            //with LINQ
            List<int> intList = new List<int>() {21,5,2,2,4,5,7,42,84,1,12,13,25};
            var succesfullyDividedBySevenAndThreeSimultaneously =
                from number in intList
                where (number % 7 == 0) && (number % 3 == 0)
                select number;
            foreach(var item in succesfullyDividedBySevenAndThreeSimultaneously)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            //with Lambda
            var dividedBy7And3Simultaneously = intList.FindAll(x => ((x % 7 == 0) && (x % 3 == 0)));
            foreach(var item in dividedBy7And3Simultaneously)
            {
                Console.WriteLine(item);
            }
        }
    }
}
