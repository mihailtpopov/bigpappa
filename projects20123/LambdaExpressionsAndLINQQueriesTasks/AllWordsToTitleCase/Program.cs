﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AllWordsToTitleCase
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "this iS a Sample sentence.";
            Console.WriteLine(text);
            string textToTitleCase = text.GetTextToTitleCase();
            Console.WriteLine(textToTitleCase);
        }
    }
}
