﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
namespace AllWordsToTitleCase
{
    static class StringExtensions
    {
       public static string GetTextToTitleCase(this string str)
        {
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
            string textToTitleCase = myTI.ToTitleCase(str);
            return textToTitleCase;
        }
    }
}
