﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionariesExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            IDictionary<string, double> studentMarks = new Dictionary<string, double>();
            studentMarks["Pesho"] = 3.00;
            studentMarks["Misho"] = 4.50;
            Console.WriteLine("Pesho's mark: {0:0.00}", studentMarks["Pesho"]);
            Console.WriteLine("Misho's mark: {0:0.00}", studentMarks["Misho"]);

        }
    }
}
