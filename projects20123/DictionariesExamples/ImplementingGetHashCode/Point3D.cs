﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImplementingGetHashCode
{
    public class Point3D
    {
        private double x;
        private double y;
        private double z;

        public Point3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double X
        {
            get { return this.x; }
            set { this.x = value; }
        }
        public double Y
        {
            get { return this.y; }
            set { this.y = value; }
        }
        public double Z
        {
            get { return this.z; }
            set { this.z = value; }
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
            {
                return true;
            }
            Point3D other = obj as Point3D;
            if (other == null)
            {
                return false;
            }
            if (!this.x.Equals(other.x))
                return false;
            if (!this.y.Equals(other.y))
                return false;
            if (!this.z.Equals(other.z))
                return false;

            return true;
        }
        public override int GetHashCode()
        {
            int prime = 83;
            int result = 1;

            unchecked
            {
                result = result * prime + x.GetHashCode();
                result = result * prime + y.GetHashCode();
                result = result * prime + z.GetHashCode();
            }

            return result;
        } 
    }
}
