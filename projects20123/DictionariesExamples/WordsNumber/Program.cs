﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordsNumber
{
    class Program
    {
        private static readonly string TEXT =    "She uchish li she bachkash li? Be kvo she bachkash " 
                +   "be? Tui vashto uchene li e? Ia po-hubavo opitai da " 
                +   "BACHKASH da se uchish malko! Uchish ne uchish trqbva da bachkash!";
        static void Main(string[] args)
        {
            IDictionary<string, int> wordOccurenceMap = GetWordOccurenceMap(TEXT);
            PrintWordOccurenceMap(wordOccurenceMap);
        }

        private static IDictionary<string,int> GetWordOccurenceMap(string text)
        {
            string[] tokens = text.Split(' ', ',', '.', '!', '?', '-');
            IDictionary<string, int> words = new SortedDictionary<string,int>(new CaseInsensitiveComparer());
            foreach(string word in tokens)
            {
                if(string.IsNullOrEmpty(word.Trim()))
                {
                    continue;
                }
                int count;
                if(!words.TryGetValue(word,out count))
                {
                    count = 0;
                }
                words[word] = count + 1;
            }

            return words;
        }

         public static void PrintWordOccurenceMap(IDictionary<string,int> wordMap)
        {
             foreach(KeyValuePair<string,int> wordEntry in wordMap)
             {
                 Console.WriteLine("Word {0} occurs {1} times!",wordEntry.Key,wordEntry.Value);
             }
        }
    }
}
