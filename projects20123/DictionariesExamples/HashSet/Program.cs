﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashSet
{
    class Program
    {
        static void Main(string[] args)
        {
            HashSet<string> aspNetStudents = new HashSet<string>();

            aspNetStudents.Add("Mihail Popov");
            aspNetStudents.Add("Atanas Drumev");
            aspNetStudents.Add("Kolio Kolev");

            HashSet<string> silverlightStudents = new HashSet<string>();
            silverlightStudents.Add("S. Guhtrie");
            silverlightStudents.Add("Kolio Kolev");

            HashSet<string> allStudents = new HashSet<string>();
            allStudents.UnionWith(aspNetStudents);
            allStudents.UnionWith(silverlightStudents);

            HashSet<string> intersectStudents = new HashSet<string>(aspNetStudents);
            intersectStudents.IntersectWith(silverlightStudents);

            Console.WriteLine("All ASP.NET students: " + GetListOfStudents(aspNetStudents));
            Console.WriteLine("Silverlight students: " + GetListOfStudents(silverlightStudents));
            Console.WriteLine("All students: " + GetListOfStudents(allStudents));
            Console.WriteLine("Students in both ASP.NET аnd Silverlight: " + GetListOfStudents(intersectStudents));
        }
        static string GetListOfStudents(IEnumerable<string> students)
        {
            string result = string.Empty;
            foreach (string student in students)
            {
                result += student + " ";
            }
            if(result!=string.Empty)
            {
                result = result.TrimEnd(',', ' ');
            }
            return result;
        }
    }
}
