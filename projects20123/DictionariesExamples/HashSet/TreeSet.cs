﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashSet
{
    class TreeSet<T>: ICollection<T>
    {
        private SortedDictionary<T, bool> innerDictionary;
        public TreeSet(IEnumerable<T> element): this()
        {
            foreach(T item in element)
            {
                this.Add(item);
            }
        }
        public TreeSet()
        {
            this.innerDictionary = new SortedDictionary<T, bool>();
        }
        public bool Add(T element)
        {
            if(!innerDictionary.ContainsKey(element))
            {
                this.innerDictionary[element] = true;
                return true;
            }
            return false;
        }
        public List<T> IntersectWith(TreeSet<T> other)
        {
            List<T> intersectedList = new List<T>();
            foreach(T key in this.innerDictionary.Keys)
            {
                if(other.Contains(key))
                {
                    intersectedList.Add(key);
                }
            }
            return intersectedList;
        }
        public List<T> UnionWith(TreeSet<T> other)
        {
            List<T> unionList = new List<T>(other);
           foreach(T key in this.innerDictionary.Keys)
           {
               if(unionList.Contains(key))
               {
                   continue;
               }
               unionList.Add(key);
           }
           return unionList;
        }
    }
}
