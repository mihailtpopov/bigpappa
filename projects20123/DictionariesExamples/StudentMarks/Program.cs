﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMarks
{
    class Program
    {
        static void Main(string[] args)
        {
            IDictionary<string, double> studentMarks = new Dictionary<string, double>(6);
            studentMarks["Pesho"] = 3.00;
            studentMarks["Dakata"] = 4.50;
            studentMarks["Daniel"] = 3.75;
            studentMarks["Husito"] = 5.25;
            studentMarks["Misho"] = 2.30;
            studentMarks["Kerim"] = 6.00;

            double peshosMark = studentMarks["Pesho"];
            Console.WriteLine("Pesho's mark: {0:0.00}", peshosMark);

            studentMarks.Remove("Pesho");
            Console.WriteLine("Pesho's mark removed.");

            Console.WriteLine("Is Pesho in the dictionary? - {0}",studentMarks.ContainsKey("Pesho")?"Yes":"No");

            Console.WriteLine("Dakata's mark: {0:0.00}",studentMarks["Dakata"]);
            studentMarks["Dakata"] = 3.25;
            Console.WriteLine("But we all know he deserves no more than {0:0.00}",studentMarks["Dakata"]);

            double mishosMark;
            bool findMisho = studentMarks.TryGetValue("Misho", out mishosMark);
            Console.WriteLine("Is Misho mark in the dictionary? - {0}", findMisho?"Yes":"No");

            studentMarks["Misho"] = 6.00;
            findMisho = studentMarks.TryGetValue("Misho", out mishosMark);
            Console.WriteLine("Let's try again: {0}, Misho's mark is: {1}",findMisho?"Yes!":"No!",mishosMark);

            Console.WriteLine("Students and marks");
            foreach(KeyValuePair<string, double> studentMark in studentMarks)
            {
                Console.WriteLine("{0} has {1:0.00}",studentMark.Key,studentMark.Value);
            }

            Console.WriteLine("There are {0} students in the dictionary",studentMarks.Count);
            studentMarks.Clear();
            Console.WriteLine("Students dictionary cleared");
            Console.WriteLine("Is dictionary empty? - {0}",studentMarks.Count==0);

            Console.ReadLine();
        }
    }
}
