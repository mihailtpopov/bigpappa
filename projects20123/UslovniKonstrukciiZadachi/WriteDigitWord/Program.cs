﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WriteDigitWord
{
    class Program
    {
        static void Main(string[] args)
        {
            string number = Console.ReadLine();
            if (number.Length == 3)
            {
                NumberWithWords.ThreeDigitNumber(number);
            }
            else if (number.Length == 2)
            {
                NumberWithWords.TwoDigitNumber(number);
            }
            else if (number.Length == 1)
            {
                NumberWithWords.OneDigitNumber(number);
            }
            else
            {
                throw new ArgumentException("The number must in the range [0..999]");
            }
        }
    }
}
