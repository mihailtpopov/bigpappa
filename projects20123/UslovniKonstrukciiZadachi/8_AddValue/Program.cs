﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_AddValue
{
    class Program
    {
        static void Main(string[] args)
        {
            //string value = Console.ReadLine();
            //int intNumber;
            //double doubleNumber;
            //bool intParse = int.TryParse(value, out intNumber);
            //bool doubleParse = double.TryParse(value, out doubleNumber);
            //if (intParse)
            //{
            //    intNumber++;
            //    Console.WriteLine(intNumber);
            //}
            //else if (doubleParse)
            //{
            //    doubleNumber++;
            //    Console.WriteLine(doubleNumber);
            //}
            //else
            //{
            //    value = value + "*";
            //    Console.WriteLine(value);
            //}

            //or
            Console.WriteLine("Enter integer, floating-point number or string. For integer enter 0, floating point-1,string-2");
            int entry =int.Parse(Console.ReadLine());
            string value1 = Console.ReadLine();
            switch(entry)
            {
                case 0:
                    int intNum = int.Parse(value1);
                    Console.WriteLine(intNum+1);
                    break;
                case 1:
                    double dNum = double.Parse(value1);
                    Console.WriteLine((dNum+1));
                    break;
                case 2:
                    Console.WriteLine(value1+"*");
                    break;
                default:
                    Console.WriteLine("You must enter 0, 1 or 2 to choose a type");
                    break;
            }

        }
    }
}
