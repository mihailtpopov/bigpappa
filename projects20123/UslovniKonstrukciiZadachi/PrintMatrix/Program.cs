﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int tempN = n;
            if(n<20)
            {
                for(int i = 1;i<=tempN;i++,n++)
                {
                    for(int j=i;j<=n;j++)
                    {
                        Console.Write(j+ " ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
