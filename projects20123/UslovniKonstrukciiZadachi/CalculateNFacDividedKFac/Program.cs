﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateNFacDividedKFac
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter two positive integers N and K. N must be larger than K");
            Console.Write("Enter N: ");
            uint N = uint.Parse(Console.ReadLine());
            Console.Write("Enter K: ");
            uint K = uint.Parse(Console.ReadLine());
            uint nFactorial=1;
            uint kFactorial=1;
           if(N>K)
           {
               for(uint i = 2;i<=K;i++)
               {
                   kFactorial *= i;
               }
               for(uint j = 2;j<=N;j++)
               {
                   nFactorial *= j;
               }
           }
           else
           {
               throw new ArgumentException("N must be larger than K");
           }
           double factorialDivision = nFactorial / (double)kFactorial;
           Console.WriteLine("{0}!/{1}! = {2}",N,K,factorialDivision);
        }
    }
}
