﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatalansNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int divisible = 2 * n;
            int divider = n + 1;
            int divisibleFactorial = 1;
            int dividerFactorial = 1;
            int nFactorial = 1;
            for(int i = 1;i<=divisible;i++)
            {
                divisibleFactorial *= i;
            }
            for(int i = 1;i<=divider;i++)
            {
                dividerFactorial *= i;
                if(i==divider)
                {
                    break;
                }
                nFactorial *= i;
            }
            int nCatalan = divisibleFactorial/(dividerFactorial*nFactorial);
            Console.WriteLine("Cn = 2n!/(n+1)!n!={0}!/{1}!{2}!={3}",divisibleFactorial,dividerFactorial,nFactorial,nCatalan);
        }
    }
}
