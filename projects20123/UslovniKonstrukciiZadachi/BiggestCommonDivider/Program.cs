﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiggestCommonDivider
{
    class Program
    {
        static void Main(string[] args)
        {
            //int first = int.Parse(Console.ReadLine());
            //int second = int.Parse(Console.ReadLine());
            //int smaller=0;
            //int biggestCommonDivider=1;
            //if(first>second)
            //{
            //    smaller = second;
            //}
            //else if(second>first)
            //{
            //    smaller = first;
            //}
            //for(int i=2;i<=smaller;i++)
            //{
            //    if(first%i==0 && second%i==0)
            //    {
            //        biggestCommonDivider = i;
            //    }
            //}
            //switch(biggestCommonDivider)
            //{
            //    case 1: Console.WriteLine("There isn't biggest common divider!"); break;
            //    default: Console.WriteLine("The biggest common divider is: " + biggestCommonDivider); break;
            //}

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            while(b!=0)
            {
                int oldB = b;
                b = a % b;
                a = oldB;
            }
            Console.WriteLine("GCD="+a);
        }
    }
}
