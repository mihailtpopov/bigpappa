﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int x = int.Parse(Console.ReadLine());
            int nFactorial = 1;
            double sum = 1;
            Console.Write(sum + " + ");
            for(int i = 1;i<=n;i++)
            {
                for(int j = 1;j<=i;j++)
                {
                    nFactorial *= j;
                }
                sum += nFactorial / (double)Math.Pow(x, i);
                nFactorial = 1;
                if (i < n)
                {
                    Console.Write("{0}!/{1}^{0} + ", i, x);
                }
                else
                {
                    Console.Write("{0}!/{1}^{0} ", i, x);
                }
            }
            Console.Write("= {0}",sum);
            
        }
    }
}
