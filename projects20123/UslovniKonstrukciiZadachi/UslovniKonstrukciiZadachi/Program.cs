﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UslovniKonstrukciiZadachi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first integer number");
            int firstNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter second integer number");
            int secondNumber = int.Parse(Console.ReadLine());
            int temp=0;
            if(firstNumber>secondNumber)
            {
                temp = firstNumber;
                firstNumber = secondNumber;
                secondNumber = temp;
                Console.WriteLine("The numbers have changed their values!");
            }

           
        }
    }
}
