﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLargestOfThreeNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            LargestOfThree.WithLoop();
            Console.WriteLine("Enter three numbers:");
            double first = double.Parse(Console.ReadLine());
            double second = double.Parse(Console.ReadLine());
            double third = double.Parse(Console.ReadLine());
            LargestOfThree.WithMathMax(first, second, third);
        }
    }
}
