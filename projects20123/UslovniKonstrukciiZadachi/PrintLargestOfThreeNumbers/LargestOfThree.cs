﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLargestOfThreeNumbers
{
    class LargestOfThree
    {
        public static void WithLoop()
        {

            Console.WriteLine("Enter three numbers and print the largest");
            double largestNumber = double.MinValue;
            for (int i = 0; i < 3; i++)
            {
                double number = double.Parse(Console.ReadLine());
                if (number > largestNumber)
                {
                    largestNumber = number;
                }
            }
            Console.WriteLine("The largest number is: " + largestNumber);
        }
        public static void WithMathMax(double first, double second, double third)
        {
            double larger = Math.Max(first, second);
            double largest = Math.Max(larger, third);
            Console.WriteLine("Largest number is: " + largest);
        }
    }
}
