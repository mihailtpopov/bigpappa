﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print3NumbersInDescending
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            for(int i = 0;i<3;i++)
            {
                int number = int.Parse(Console.ReadLine());
                list.Add(number);
            }
            list.Sort();
            list.Reverse();
            for(int i = 0;i<list.Count;i++)
            {
                Console.WriteLine(list[i]);
            }
            
        }
    }
}
