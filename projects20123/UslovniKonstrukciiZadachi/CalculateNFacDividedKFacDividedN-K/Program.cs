﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateNFacDividedKFacDividedN_K
{
    class Program
    {
        static void Main(string[] args)
        {
            uint N = uint.Parse(Console.ReadLine());
            uint K = uint.Parse(Console.ReadLine());
            uint kFactorial = 1;
            uint nFactorial = 1;
            uint nMinusKFactorial = 1;
            if(N>K)
            {
                for(uint i = 2;i<=N;i++)
                {
                    nFactorial *= i;
                }
                for(uint j = 2;j<=K;j++)
                {
                    kFactorial *= j;
                }
                for(uint m = 1; m<=N-K;m++)
                {
                    nMinusKFactorial *= m;
                }
            }
            else
            {
                throw new ArgumentException("N must be larger than K");
            }
            uint factorialDivision = (nFactorial*kFactorial) / nMinusKFactorial;
            Console.WriteLine("{0}!*{1}!/({0}-{1})! = {2}",N,K,factorialDivision);
        }
    }
}
