﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckIfSumIs0
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[5];
            List<int> subset = new List<int>();
            int sum = 0;
            int next = 0;
            for(int i = 0;i<array.Length;i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }
            for(int i = 0;i<array.Length-1;i++, sum=0, next = 0)
            {
                sum = array[i];
                next = i;
                int k = 1;
                while (next < array.Length)
                {
                    for (int j = i+k; j < array.Length; j++)
                    {
                        sum += array[j];
                        if (sum == 0)
                        {
                            subset.Add(array[i]);  
                            while (k<=j)
                            {
                                subset.Add(array[k]);
                                k++;
                            }
                            goto subset;
                        }
                    }
                    k++;
                    sum = array[i];
                    next++;
                }

            }
            subset:
            int subsetSum = 0;
            foreach(int number in subset)
            {
                subsetSum += number;
            }
            if(subsetSum!=0)
            {
               subset.Remove(subsetSum);
            }
            foreach(int number in subset)
                Console.Write(number + ", ");
        }
    }
}
