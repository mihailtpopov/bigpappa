﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_PrintSignOfMultiplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter three real numbers");
            double first = double.Parse(Console.ReadLine());
            double second = double.Parse(Console.ReadLine());
            double third = double.Parse(Console.ReadLine());
           int counter = 0;
            if(first<0)
            {
                counter++;
            }
            if(second<0)
            {
                counter++;
            }
            if(third<0)
            {
                counter++;
            }
            if(counter%2==0)
            {
                Console.WriteLine("+");
            }
            else
            {
                Console.WriteLine("-");
            }
        }
    }
}
