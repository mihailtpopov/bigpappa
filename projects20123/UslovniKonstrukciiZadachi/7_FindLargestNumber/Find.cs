﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_FindLargestNumber
{
    class Find
    {
        public static double LargestNumber()
        {
            int largestNumber = int.MinValue;
            for (int i = 0; i < 5; i++)
            {
                int number = int.Parse(Console.ReadLine());
                if (number > largestNumber)
                {
                    largestNumber = number;
                }
            }
            return largestNumber;
        }
    }
}
