﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckFactorialEnds
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            long nFactorial = 1;
            int endZeros = 0;
            for(int i = 2;i<=n;i++)
            {
                nFactorial *= i;
            }

            long formatedFactorial = nFactorial;

            while(formatedFactorial%10==0)
            {
                endZeros++;
                formatedFactorial/= 10;
            }
            Console.WriteLine("{0}! = {1} has {2} zeros at the end",n,nFactorial,endZeros);
            
        }
    }
}
