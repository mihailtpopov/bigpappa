﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintRangeOfNumbersRandomly
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int temp = n;
            List<int> intList = new List<int>();
            Random rand = new Random();
            while(intList.Count<n)
            {
                int randomNum = rand.Next(1,temp+1);
                if (intList.Contains(randomNum))
                {
                    continue;
                }
                intList.Add(randomNum);
                if (randomNum == n)
                {
                    temp--;
                }

            }
            for(int i = 0;i<intList.Count;i++)
            {
                Console.WriteLine(intList[i]);
            }
        }
    }
}
