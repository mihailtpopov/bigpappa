﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintNFibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int first = 0;
            int next = 1;
            int sum = first + next;
            Console.WriteLine(first);
            Console.WriteLine(next);
            for(int i = 0;i<n;i++)
            {
                Console.WriteLine(sum);
                sum += next;
                next = sum - next;
            }
        }
    }
}
