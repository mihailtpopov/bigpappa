﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
           watch.Start();
           long sum = 0;
            for(int i = 0;i<1000;i++)
            {
                for (int j = 2; j < Math.Sqrt(i); j++)
                {
                    if (i % j == 0)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine(i);
                        break;
                    }
                }
            }
            watch.Stop();
            Console.WriteLine();
            Console.WriteLine("Time elapsed in seconds: {0}", watch.ElapsedMilliseconds);
            Console.WriteLine();
            int[] numbers = new int[0];
            Stopwatch watch2 = new Stopwatch();
            watch2.Start();
            long total = 0;
            //Parallel.For<long>(0, 200000, () => 0, (j,loop,subtotal) =>
             //   {
              //      subtotal += j;
              //      return subtotal;
              //  },
           // (subtotal) => Interlocked.Add(ref total, subtotal)
           // );

            Parallel.For(0, 1000, (index) =>
                {
                    for (int j = 2; j <= Math.Sqrt(index); j++)
                    {
                        if (index % j == 0)
                        {
                            break;
                        }
                        else
                        {
                            Console.WriteLine(index);
                            break;
                        }
                    }
                });
            
            watch2.Stop();
            Console.WriteLine();
            Console.WriteLine("Time elapsed in seconds: {0}", watch.ElapsedMilliseconds);
        }
    }
}
