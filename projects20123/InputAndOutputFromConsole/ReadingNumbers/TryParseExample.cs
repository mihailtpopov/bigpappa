﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingNumbers
{
    class TryParseExample
    {
        public static void parseSuccess()
        {
        string str = Console.ReadLine();
        int intValue;
        bool parseSuccess = Int32.TryParse(str, out intValue);
        string parseAnswer = parseSuccess?"The square of the number is: " + Math.Pow(intValue,2):"Invalid number!";
        Console.WriteLine(parseAnswer);
        }
    }
}
