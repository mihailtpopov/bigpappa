﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingNumbers
{
    class TryToParseStringToNumber
    {
       public static void PrintParsedStringToDouble()
        {
            Console.Write("Enter a floating-point number: ");
           string text = Console.ReadLine();
           double number = double.Parse(text);
           Console.WriteLine(number);
        }
    }
}
