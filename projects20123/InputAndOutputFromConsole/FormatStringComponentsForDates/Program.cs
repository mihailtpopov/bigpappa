﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatStringComponentsForDates
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = new DateTime (int.Parse(Console.ReadLine()), 4, 10, 12, 15, 30);
            Console.WriteLine("{0:d/M/yy }",date);
            Console.WriteLine("{0:dd/MM/yyyy HH:mm:ss}",date);
        }
    }
}
