﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithConsoleKeyInfo
{
    class Key
    {
        public static void Info(ConsoleKeyInfo key)
        {
            Console.WriteLine("Character entered: " +key.KeyChar);
            Console.WriteLine("Special keys: " +key.Modifiers);
                
        }
    }
}
