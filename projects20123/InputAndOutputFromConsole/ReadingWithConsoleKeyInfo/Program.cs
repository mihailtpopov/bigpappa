﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithConsoleKeyInfo
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            Console.WriteLine();
            Key.Info(keyInfo);
        }
    }
}
