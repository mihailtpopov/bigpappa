﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatStringForEnumerations
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("{0:G}",DayOfWeek.Wednesday); //Wednesday Represent the enumeration as a string
            Console.WriteLine("{0:D}",DayOfWeek.Wednesday); //3 Represent the enumeration as a number
            Console.WriteLine("{0:X}",DayOfWeek.Wednesday); //00000003 Represent the enumeration in hexadecimal with 8 digits
        }
    }
}
