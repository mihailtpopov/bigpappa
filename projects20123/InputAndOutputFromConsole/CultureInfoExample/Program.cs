﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;

namespace CultureInfoExample
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = new DateTime(2020, 4, 10, 12, 41, 32);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            Console.WriteLine("{0:N}",1231456);
            Console.WriteLine("{0:D}",date);
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("bg-BG");
            Console.WriteLine("{0:N}",1231456);
            Console.WriteLine("{0:D}",date);
        }
    }
}
