﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputAndOutputFromConsoleExamples
{
    class RectangleOrTriangle
    {
        public static double CalculateArea(double a, double b, int choice)
        {
            double area = (a * b) / choice;
            return area;
        }
        public static void PrintArea()
        {
            Console.WriteLine("This program calculates the area of a rectangle or triangle");
            Console.WriteLine("Enter a and b for rectangle or a and h for triangle:");
            double a = double.Parse(Console.ReadLine());
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter 1 for rectangle and 2 for triangle");
            int choice = int.Parse(Console.ReadLine());
            Console.WriteLine("The area of your figure is: " + CalculateArea(a,b,choice));
        }
    }
}
