﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputAndOutputFromConsoleExamples
{
    class Letter
    {
        public static void PrintLetter(string person, string book)
        {
            string from = "Authors Team";
            Console.WriteLine("   Dear {0}",person);
            Console.Write("We are please to inform you that \"{1}\" is the best Bulgarian book.{2}" + 
                "The authors of the book wish you good luck, {0}!{2}",person,book,Environment.NewLine);
            Console.WriteLine("   Yours,");
            Console.WriteLine("   {0}",from);
        }  
    }
}
