﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatStringComponentsForNumbers
{
    class CustomNumericFormats
    {
      public  static void PrintCustomFormats()
        {
            Console.WriteLine("{0:0.00}",123.46); //123,Indicates a number. If a digit is missing on this position, it
            //represent a number.
            Console.WriteLine("{0:#.##}",0.234); // ,23 Indicates a number. Returns nothing if a digit is missing on the 
            //that position or if the number starts with zero.
            Console.WriteLine("{0:#######}",123456.78); //
          //. - decimal separator in the given culture
          //, - separator for the thousands in the given culture

            Console.WriteLine("{0:(0#) ### ## ##}",23456789);
            Console.WriteLine("{0:(+###) ### ### ###}",+35989985797 );
            Console.WriteLine("{0:%##.#}",0.234); //multiplies the number by 100 and returns the symbol %


        }
    }
}
