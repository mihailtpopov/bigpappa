﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatStringComponentsForNumbers
{
    class Program
    {
        
        static void Main(string[] args)
        {
            StandardNumericFormatss.PrintStandardNumericFormats();
            Console.WriteLine();
            CustomNumericFormats.PrintCustomFormats();
        }
    }
}
