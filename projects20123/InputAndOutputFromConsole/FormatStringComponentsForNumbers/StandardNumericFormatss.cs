﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormatStringComponentsForNumbers
{
    class StandardNumericFormatss
    {
        public static void PrintStandardNumericFormats()
        {
            Console.WriteLine("{0:C2}", 123.456); // 123,46 лв. converts into "Cultures" currency and precise 2 numbers after coma
            Console.WriteLine("{0:D6}", -1234);//-001234 Whole number. The precision indicates the minimal number of
            //symbol to represent the string, as needed completes zero infront of the number.
            Console.WriteLine("{0:E2}", 123); //1,23E+002 Exponencial representation. The precision indicates the number of 
            //number after the coma.
            Console.WriteLine("{0:F2}", -123.456);// -123,46 Integer or floating point number. The precision indicates 
            //the number of numbers after the coma.
            Console.WriteLine("{0:N2}", 1234567.8);// 1 234 567,80 Same as "F", but illustrates the number with separa
            //tor for the thousands, millions etc for the given culture.
            Console.WriteLine("{0:P}", 0.456); // 45,60% It will multiply the number by 100 and print the % symbol.
            //The precision indicates the number of numbers after the coma.
            Console.WriteLine("{0:X}", 254); // FE Converts the number into hexadecimal number system. Works only with
            //ints. The precision indicates the minimal number of symbols of representing the string as the missing ones 
            //completes with zero in front of the number.
        }
    }
}
