﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputAndOutputFromConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello World!";
                Console.Write(str);
            Console.Write("{0}",str);

            string name = "Boris";
            int age = 18;
            string city = "Plovdiv";
            Console.Write("{0} is {1} years old from {2}!\n",name,age,city);
        }
    }
}
